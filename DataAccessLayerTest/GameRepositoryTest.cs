﻿using System;
using System.Globalization;
using System.Linq;
using Common;
using Common.Database;
using DataAccessLayer.GameEntry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Protocol;

namespace DataAccessLayerTest
{
    [TestClass]
    public class GameRepositoryTest
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            DatabaseUtils.DropDatabaseIfExists(DatabaseName, SqlServer);
            DatabaseUtils.CreateDatabaseIfNotExists(DatabaseName, SqlServer);

            _connectionFactory = new SqlConnectionFactory(
                sqlServerName: SqlServer,
                databaseName: DatabaseName);

            _gameRepository = new GameRepository(_connectionFactory);
            _gameRepository.CreateIfMissing();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.ExecuteNonQuery(string.Format("delete from {0}", _gameRepository.EntryName));
            }
        }

        [TestMethod]
        public void GameResultsTableShouldExistsTest()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.ExecuteReader("SELECT * FROM " + _gameRepository.EntryName);
            }
        }

        [TestMethod]
        public void SavedGameResultShouldBeFounded()
        {
            var whitePlayerId = 1;

            Assert.AreEqual(0, _gameRepository.GetGamesWithUser(whitePlayerId).Count());

            var blackPlayerId = 2;
            var startTime = DateTime.UtcNow;
            var finishTime = DateTime.UtcNow.AddHours(2);
            var deltaRating = 20;
            var gameResult = new GameEntry(
                whitePlayerId,
                blackPlayerId,
                GameResult.WhiteWin,
                GameMode.SimpleGame,
                deltaRating,
                -deltaRating,
                finishTime,
                startTime);

            _gameRepository.SaveGameResult(gameResult);
            Assert.AreNotEqual(-1, gameResult.Id);

            var loadedGameResult = _gameRepository.GetGamesWithUser(whitePlayerId).First();
            Assert.AreEqual(gameResult.Id, loadedGameResult.Id);
            Assert.AreEqual(gameResult.WhitePlayerId, loadedGameResult.WhitePlayerId);
            Assert.AreEqual(gameResult.WhitePlayerRatingDelta, loadedGameResult.WhitePlayerRatingDelta);
            Assert.AreEqual(GameResult.WhiteWin, loadedGameResult.Result);
            Assert.AreEqual(
                gameResult.FinishTime.ToString(CultureInfo.InvariantCulture), 
                loadedGameResult.FinishTime.ToString(CultureInfo.InvariantCulture));
        }

        const string SqlServer = @".\SQLSERVER";
        const string DatabaseName = @"TestGameResultDb";
        private static IConnectionFactory _connectionFactory;
        private static GameRepository _gameRepository;
    }
}
