﻿using System;
using System.ServiceModel;
using Common;
using GameMechanics;

namespace Protocol
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IGameCallback))]
    public interface IGameService
    {
        [OperationContract]
        MoveResult MakeMove(Guid sessionId, Guid gameId, FlatPosition start, FlatPosition finish);

        [OperationContract]
        MoveResult UpdatePawn(Guid sessionId, Guid gameId, Figure newFigure);

        [OperationContract]
        void FindEnemy(Guid sessionId, GameMode mode);

        [OperationContract]
        void StopToFind(Guid sessionId);

        [OperationContract]
        void UpdateChannel(Guid sessionId);
    }
}
