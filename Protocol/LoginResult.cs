﻿namespace Protocol
{
    public enum LoginResult
    {
        Success = 0,
        WrongNameOrPassword = 1,
        UnknownServerException = 2
    }
}
