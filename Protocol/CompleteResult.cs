﻿using System;

namespace Protocol
{
    [Serializable]
    public class CompleteResult
    {
        public CompleteResult(
            int newRating, 
            int newEnemyRating, 
            GameResult result)
        {
            Result = result;
            NewEnemyRating = newEnemyRating;
            NewRating = newRating;
        }

        public int NewRating { get; private set; }

        public int NewEnemyRating { get; private set; }

        public GameResult Result { get; private set; }
    }
}
