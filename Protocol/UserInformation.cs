﻿using System;

namespace Protocol
{
    [Serializable]
    public sealed class UserInformation
    {
        public UserInformation()
        {
            DisplayName = string.Empty;
            Ratings = new UserRatings();
        }

        public UserInformation(string name, UserRatings ratings)
        {
            DisplayName = name;
            Ratings = ratings;
        }

        public string DisplayName { get; private set; }

        public UserRatings Ratings { get; private set; }
    }
}
