﻿using System;
using System.ServiceModel;

namespace Protocol
{
    [ServiceBehavior(
        InstanceContextMode = InstanceContextMode.PerSession,
        ConcurrencyMode = ConcurrencyMode.Single)]
    public sealed class GameCallback : IGameCallback
    {
        public void GameFound(SearchResult game)
        {
            GameFoundEvent(game);
        }

        public void GameCanceled(bool yourFault)
        {
            GameCanceledEvent(yourFault);
        }

        public void GameTerminated(CompleteResult result)
        {
            GameTerminatedEvent(result);
        }

        public void EnemyMakeMove(MoveResult move)
        {
            EnemyMakeMoveEvent(move);
        }

        public void EnemyUpgradePawn(MoveResult upgradeResult)
        {
            EnemyUpgradePawnEvent(upgradeResult);
        }

        public event Action<SearchResult> GameFoundEvent;
        public event Action<CompleteResult> GameTerminatedEvent;
        public event Action<MoveResult> EnemyMakeMoveEvent;
        public event Action<MoveResult> EnemyUpgradePawnEvent;
        public event Action<bool> GameCanceledEvent;
    }
}
