﻿using System;
using Common;
using GameMechanics;

namespace Protocol
{
    [Serializable]
    public sealed class SearchResult
    {
        public SearchResult(
            Guid gameId, 
            string enemyName, 
            int enemyRating, 
            FigureColor color, 
            GameMode mode)
        {
            Mode = mode;
            Color = color;
            EnemyRating = enemyRating;
            EnemyName = enemyName;
            GameId = gameId;
        }

        public Guid GameId { get; private set; }

        public string EnemyName { get; private set; }

        public int EnemyRating { get; private set; }

        public FigureColor Color { get; private set; }

        public GameMode Mode { get; private set; }
    }
}
