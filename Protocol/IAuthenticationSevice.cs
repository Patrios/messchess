﻿using System;
using System.ServiceModel;

namespace Protocol
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    public interface IAuthenticationSevice
    {
        [OperationContract]
        LoginInformation Login(string userName, string password);

        [OperationContract]
        Guid ExtendSession(Guid sessionId, string userName);
    }
}
