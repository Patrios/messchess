﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace Protocol
{
    [Serializable]
    public class UserRatings
    {
        public void SetRating(GameMode mode, int rating)
        {
            if (_ratings.ContainsKey(mode))
            {
                _ratings[mode] = rating;
            }
            else
            {
                _ratings.Add(mode, rating);
            }
        }

        public int GetRating(GameMode mode)
        {
            return _ratings.ContainsKey(mode) ? _ratings[mode] : MinRatingValue;
        }

        public GameMode[] GetModesWithRatings()
        {
            return _ratings.Keys.ToArray();
        }

        public const int MinRatingValue = 1200;

        private readonly IDictionary<GameMode, int> _ratings = new Dictionary<GameMode, int>();
    }
}
