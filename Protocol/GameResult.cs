﻿namespace Protocol
{
    public enum GameResult
    {
        WhiteWin,
        BlackWin,
        Draw
    }
}
