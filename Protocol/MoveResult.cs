﻿using System;
using GameMechanics;

namespace Protocol
{
    [Serializable]
    public sealed class MoveResult
    {
        public MoveResult(
            MoveInformation information, 
            CompleteResult gameResult)
        {
            GameResult = gameResult;
            Information = information;
        }

        public MoveInformation Information { get; private set; }

        public CompleteResult GameResult { get; private set; }
    }
}
