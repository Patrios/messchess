﻿using System;

namespace Protocol
{
    [Serializable]
    public sealed class LoginInformation
    {
        public LoginInformation(LoginResult result)
            : this(result, Guid.Empty, new UserInformation())
        {
        }

        public LoginInformation(
            LoginResult result, 
            Guid sessionId, 
            UserInformation userInformation)
        {
            UserInformation = userInformation;
            SessionId = sessionId;
            Result = result;
        }

        public LoginResult Result { get; private set; }

        public UserInformation UserInformation { get; private set; }

        public Guid SessionId { get; private set; }
    }
}
