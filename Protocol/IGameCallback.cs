﻿using System;
using System.ServiceModel;

namespace Protocol
{
    public interface IGameCallback
    {
        [OperationContract]
        void GameFound(SearchResult game);

        [OperationContract]
        void GameCanceled(bool yourFault);

        [OperationContract]
        void GameTerminated(CompleteResult result);

        [OperationContract]
        void EnemyMakeMove(MoveResult move);

        [OperationContract]
        void EnemyUpgradePawn(MoveResult upgradeResult);

        event Action<SearchResult> GameFoundEvent;

        event Action<CompleteResult> GameTerminatedEvent;

        event Action<MoveResult> EnemyMakeMoveEvent;

        event Action<MoveResult> EnemyUpgradePawnEvent;

        event Action<bool> GameCanceledEvent;
    }
}
