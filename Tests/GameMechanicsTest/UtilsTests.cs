﻿using System.Linq;
using GameMechanics;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class UtilsTests
    {
        [TestMethod]
        public void TestBoardConstants()
        {
            Assert.IsTrue(BoardUtils.MaskRow.First().HasFlag(Bitboard.A1));
            Assert.IsTrue(BoardUtils.MaskRow.First().HasFlag(Bitboard.A8));
            Assert.IsTrue(BoardUtils.MaskRow.Last().HasFlag(Bitboard.H1));
            Assert.IsTrue(BoardUtils.MaskRow.Last().HasFlag(Bitboard.H8));

            Assert.IsTrue(BoardUtils.MaskColumn.First().HasFlag(Bitboard.A1));
            Assert.IsTrue(BoardUtils.MaskColumn.First().HasFlag(Bitboard.H1));
            Assert.IsTrue(BoardUtils.MaskColumn.Last().HasFlag(Bitboard.A8));
            Assert.IsTrue(BoardUtils.MaskColumn.Last().HasFlag(Bitboard.H8));

            Assert.IsTrue(!BoardUtils.ClearRow.First().HasFlag(Bitboard.A1));
            Assert.IsTrue(!BoardUtils.ClearRow.First().HasFlag(Bitboard.A8));
            Assert.IsTrue(!BoardUtils.ClearRow.Last().HasFlag(Bitboard.H1));
            Assert.IsTrue(!BoardUtils.ClearRow.Last().HasFlag(Bitboard.H8));

            Assert.IsTrue(!BoardUtils.ClearColumn.First().HasFlag(Bitboard.A1));
            Assert.IsTrue(!BoardUtils.ClearColumn.First().HasFlag(Bitboard.H1));
            Assert.IsTrue(!BoardUtils.ClearColumn.Last().HasFlag(Bitboard.A8));
            Assert.IsTrue(!BoardUtils.ClearColumn.Last().HasFlag(Bitboard.H8));
        }

        [TestMethod]
        public void PositionByDescartesTest()
        {
            var position = new FlatPosition(0, 0).ToBitboard();
            Assert.AreEqual(
                position,
                Bitboard.A1);

            position = new FlatPosition(7, 7).ToBitboard();
            Assert.AreEqual(
                position,
                Bitboard.H8);

            position = new FlatPosition(5, 5).ToBitboard();
            Assert.AreEqual(
                position,
                Bitboard.F6);
        }

        [TestMethod]
        public void PositionTest()
        {
            const Bitboard positionA1 = Bitboard.A1;
            Assert.AreEqual((ulong)positionA1, (ulong)1);
        }

        [TestMethod]
        public void PositionNone()
        {
            const Bitboard positionNone = Bitboard.None;
            Assert.AreEqual((ulong)positionNone, (ulong)0);
        }

        [TestMethod]
        public void BitboardsUnionTest()
        {
            var a1AndA2Bitboard = Bitboard.A1.Union(Bitboard.A2);

            Assert.IsTrue(a1AndA2Bitboard.HasFlag(Bitboard.A1));
            Assert.IsTrue(a1AndA2Bitboard.HasFlag(Bitboard.A2));
            Assert.IsTrue(!a1AndA2Bitboard.HasFlag(Bitboard.A3));
        }

        [TestMethod]
        public void BitboardsCommonTest()
        {
            var a1AndA2 = Bitboard.A1.Union(Bitboard.A2);
            var a2AndA3 = Bitboard.A2.Union(Bitboard.A3);
            var common = a1AndA2.Common(a2AndA3);
            Assert.IsTrue(common.HasFlag(Bitboard.A2));
            Assert.IsTrue(!common.HasFlag(Bitboard.A1));
            Assert.IsTrue(!common.HasFlag(Bitboard.A3));
        }

        [TestMethod]
        public void BitboardAddColumnTest()
        {
            Assert.AreEqual(Bitboard.B1, Bitboard.A1.AddColumns(1));
        }

        [TestMethod]
        public void BitboardSubColumnTest()
        {
            Assert.AreEqual(Bitboard.B2, Bitboard.C2.SubColumns(1));
        }

        [TestMethod]
        public void BitboardAddRowTest()
        {
            Assert.AreEqual(Bitboard.A2, Bitboard.A1.AddRows(1));
        }

        [TestMethod]
        public void BitboardSubRowTest()
        {
            Assert.AreEqual(Bitboard.B1, Bitboard.B2.SubRows(1));
        }

        [TestMethod]
        public void BitboardAddOverflow()
        {
            Assert.AreEqual(Bitboard.None, Bitboard.H4.AddColumns(1));
            Assert.AreEqual(Bitboard.None, Bitboard.A8.AddRows(1));
            Assert.AreEqual(Bitboard.None, Bitboard.A4.SubColumns(1));
            Assert.AreEqual(Bitboard.None, Bitboard.B1.SubRows(1));
            Assert.AreEqual(Bitboard.None, Bitboard.None.AddRows(1));

            Assert.AreEqual(Bitboard.None, Bitboard.G4.AddColumns(2));
            Assert.AreEqual(Bitboard.None, Bitboard.A7.AddRows(2));
            Assert.AreEqual(Bitboard.None, Bitboard.B4.SubColumns(2));
            Assert.AreEqual(Bitboard.None, Bitboard.B2.SubRows(2));
        }

        [TestMethod]
        public void BitboardNegativeTest()
        {
            var a1Negative = Bitboard.A1.Negative();

            Assert.IsTrue(!a1Negative.HasFlag(Bitboard.A1));
            Assert.IsTrue(a1Negative.HasFlag(Bitboard.A2));
        }

        [TestMethod]
        public void GetFigureNameTest()
        {
            Assert.AreEqual("Knight", Figure.BlackKnight.GetFigureName());
            Assert.AreEqual("Knight", Figure.WhiteKnight.GetFigureName());
            Assert.AreEqual("", Figure.None.GetFigureName());
        }

        [TestMethod]
        public void CellNumberTest()
        {
            Assert.AreEqual(0, Bitboard.A1.Cell());
            Assert.AreEqual(63, Bitboard.H8.Cell());
        }

        [TestMethod]
        public void SplitPositionsTest()
        {
            var unionPositions = Bitboard.A1.Union(Bitboard.A2);
            Assert.IsTrue(
                unionPositions
                    .SplitPositions().SequenceEqual(
                        new[] { Bitboard.A1, Bitboard.A2 }));
        }
    }
}
