﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class BoardTests
    {
        [TestMethod]
        public void MakeMoveIncorrectFigureTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            var moveResult = board.MakeMove(Bitboard.A1, Bitboard.A2, FigureColor.Black);
            Assert.AreEqual(MoveStatus.NoFigure, moveResult.Status);

            board.SetFigure(Figure.WhiteKnight, Bitboard.A1);
            moveResult = board.MakeMove(Bitboard.A1, Bitboard.A2, FigureColor.Black);
            Assert.AreEqual(MoveStatus.NoFigure, moveResult.Status);
        }

        [TestMethod]
        public void IncorrectMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKnight, Bitboard.A2);

            var moveResult = board.MakeMove(Bitboard.A2, Bitboard.A5, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void NoCorrectFigureTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKnight, Bitboard.D4);
            var moveInformation = board.MakeMove(Bitboard.A1, Bitboard.C3, FigureColor.White);

            Assert.AreEqual(MoveStatus.NoFigure, moveInformation.Status);

            board.SetFigure(Figure.BlackBishop, Bitboard.A1);
            moveInformation = board.MakeMove(Bitboard.A1, Bitboard.C3, FigureColor.White);

            Assert.AreEqual(MoveStatus.NoFigure, moveInformation.Status);
        }

        [TestMethod]
        public void SelfCheckTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteBishop, Bitboard.E2);
            board.SetFigure(Figure.BlackQueen, Bitboard.E3);

            var moveInformation = board.MakeMove(Bitboard.E2, Bitboard.F1, FigureColor.White);

            Assert.AreEqual(MoveStatus.SelfCheck, moveInformation.Status);
            Assert.AreEqual(Figure.WhiteBishop, board.GetFigure(Bitboard.E2));
        }

        [TestMethod]
        public void SelfCheckWithEatTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteBishop, Bitboard.E2);

            board.SetFigure(Figure.BlackPawn, Bitboard.F3);
            board.SetFigure(Figure.BlackQueen, Bitboard.E3);

            var moveInformation = board.MakeMove(Bitboard.E2, Bitboard.F3, FigureColor.White);

            Assert.AreEqual(MoveStatus.SelfCheck, moveInformation.Status);
            Assert.AreEqual(Figure.WhiteBishop, board.GetFigure(Bitboard.E2));
            Assert.AreEqual(Figure.BlackPawn, board.GetFigure(Bitboard.F3));
        }

        [TestMethod]
        public void IsDoubleMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.A2);

            Assert.AreEqual(false, board.IsDoublePawnMove(Bitboard.A1, FigureColor.White));

            var moveResult = board.MakeMove(Bitboard.A2, Bitboard.A4, FigureColor.White);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);
            Assert.AreEqual(true, board.IsDoublePawnMove(Bitboard.A1, FigureColor.White));

            moveResult = board.MakeMove(Bitboard.A4, Bitboard.A5, FigureColor.White);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);
            Assert.AreEqual(false, board.IsDoublePawnMove(Bitboard.A1, FigureColor.White));
        }

        [TestMethod]
        public void SuccessMoveInformationTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKnight, Bitboard.B2);

            var moveResult = board.MakeMove(Bitboard.B2, Bitboard.C4, FigureColor.White);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);
            Assert.AreEqual(Figure.WhiteKnight, moveResult.MovedFigure);
            Assert.AreEqual(Bitboard.B2, moveResult.Start);
            Assert.AreEqual(Bitboard.C4, moveResult.Finish);
        }

        [TestMethod]
        public void PawnTwoDoubleStepMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.A2);

            var moveResult = board.MakeMove(Bitboard.A2, Bitboard.A4, FigureColor.White);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);

            moveResult = board.MakeMove(Bitboard.A4, Bitboard.A6, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void PawnsCrossEatingTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.A2);
            board.SetFigure(Figure.WhitePawn, Bitboard.B2);
            board.SetFigure(Figure.BlackPawn, Bitboard.A3);
            board.SetFigure(Figure.BlackPawn, Bitboard.B3);

            var moveResult = board.MakeMove(Bitboard.A2, Bitboard.B3, FigureColor.White);
            Assert.AreEqual(MoveStatus.EatEnemy, moveResult.Status);
            Assert.AreEqual(Bitboard.B3, moveResult.Finish);

            moveResult = board.MakeMove(Bitboard.B2, Bitboard.A3, FigureColor.White);
            Assert.AreEqual(MoveStatus.EatEnemy, moveResult.Status);
            Assert.AreEqual(Bitboard.A3, moveResult.Finish);

            moveResult = board.MakeMove(Bitboard.A3, Bitboard.A5, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void PawnsUpgradeMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.A7);
            board.SetFigure(Figure.BlackPawn, Bitboard.A2);

            var moveResult = board.MakeMove(Bitboard.A7, Bitboard.A8, FigureColor.White);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);
            Assert.AreEqual(true, moveResult.PawnUpgrade);

            moveResult = board.MakeMove(Bitboard.A2, Bitboard.A1, FigureColor.Black);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveResult.Status);
            Assert.AreEqual(true, moveResult.PawnUpgrade);
        }

        [TestMethod]
        public void CheckTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);

            var moveResult = board.MakeMove(Bitboard.A1, Bitboard.E1, FigureColor.White);

            Assert.AreEqual(EnemyStatus.Check, moveResult.EnemyStatus);
        }

        [TestMethod]
        public void StalemateMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A7);
            board.SetFigure(Figure.WhiteRook, Bitboard.F1);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);
            board.SetFigure(Figure.BlackPawn, Bitboard.A8);

            var moveResult = board.MakeMove(Bitboard.A1, Bitboard.D1, FigureColor.White);

            Assert.AreEqual(EnemyStatus.Stalemate, moveResult.EnemyStatus);
        }

        [TestMethod]
        public void MateTestMoves()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A7);
            board.SetFigure(Figure.WhiteRook, Bitboard.F1);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteRook, Bitboard.D2);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);
            board.SetFigure(Figure.BlackPawn, Bitboard.A8);

            var moveResult = board.MakeMove(Bitboard.A1, Bitboard.E1, FigureColor.White);

            Assert.AreEqual(EnemyStatus.Mate, moveResult.EnemyStatus);
        }

        [TestMethod]
        public void MoveShouldNotSucceededIfThereIfPawnForUpgradeTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A7);
            board.SetFigure(Figure.WhitePawn, Bitboard.A8);

            var moveResult = board.MakeMove(Bitboard.A7, Bitboard.B7, FigureColor.White);

            Assert.AreEqual(MoveStatus.NeedPawnUpgrade, moveResult.Status);
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
