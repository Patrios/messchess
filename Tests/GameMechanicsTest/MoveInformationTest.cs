﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class MoveInformationTest
    {
        [TestMethod]
        public void SimpleMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            var bishopMovesProvider = new BishopMovesProvider(board);
            var moveInformation = bishopMovesProvider.GetMoveInformation(Bitboard.A1, Bitboard.C3);
            Assert.AreEqual(MoveStatus.AcceptedMove, moveInformation.Status);
        }

        [TestMethod]
        public void AttackMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteBishop, Bitboard.B3);

            var knightMovesProvider = new KnightMovesProvider(board);
            var moveInformation = knightMovesProvider.GetMoveInformation(Bitboard.A1, Bitboard.B3);
            Assert.AreEqual(MoveStatus.EatEnemy, moveInformation.Status);
            Assert.AreEqual(Bitboard.B3, moveInformation.EatedPosition);
            Assert.AreEqual(Figure.WhiteBishop, moveInformation.EatedFigure);
        }

        [TestMethod]
        public void CastlingMoveTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            var kingMovesProvider = new KingMovesProvider(board);
            var moveInformation = kingMovesProvider.GetMoveInformation(Bitboard.E1, Bitboard.C1);

            Assert.AreEqual(MoveStatus.Castling, moveInformation.Status);
            Assert.AreEqual(Bitboard.D1, moveInformation.CastlingFinishRook);

            moveInformation = kingMovesProvider.GetMoveInformation(Bitboard.E1, Bitboard.G1);

            Assert.AreEqual(MoveStatus.Castling, moveInformation.Status);
            Assert.AreEqual(Bitboard.F1, moveInformation.CastlingFinishRook);
        }

        [TestMethod]
        public void PawnMovesInformationTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            var pawnMoverProvider = new PawnMovesProvider(board);

            var moveInformation = pawnMoverProvider.GetMoveInformation(Bitboard.D2, Bitboard.D3);

            Assert.AreEqual(MoveStatus.AcceptedMove, moveInformation.Status);
            Assert.AreEqual(false, moveInformation.IsPawnDoubleMove);

            moveInformation = pawnMoverProvider.GetMoveInformation(Bitboard.D2, Bitboard.D4);

            Assert.AreEqual(MoveStatus.AcceptedMove, moveInformation.Status);
            Assert.AreEqual(true, moveInformation.IsPawnDoubleMove);

            moveInformation = pawnMoverProvider.GetMoveInformation(Bitboard.D2, Bitboard.C3);

            Assert.AreEqual(MoveStatus.EatEnemy, moveInformation.Status);
            Assert.AreEqual(Bitboard.C2, moveInformation.EatedPosition);

            board.SetFigure(Figure.BlackRook, Bitboard.C3);
            moveInformation = pawnMoverProvider.GetMoveInformation(Bitboard.D2, Bitboard.C3);

            Assert.AreEqual(MoveStatus.EatEnemy, moveInformation.Status);
            Assert.AreEqual(Bitboard.C3, moveInformation.EatedPosition);
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
