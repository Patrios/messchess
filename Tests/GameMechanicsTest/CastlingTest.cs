﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class CastlingTest
    {
        [TestMethod]
        public void CastlingWhiteLeftTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteRook, Bitboard.H1);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.Castling, moveResult.Status);
            Assert.AreEqual(Bitboard.D1, moveResult.CastlingFinishRook);
            Assert.AreEqual(Bitboard.A1, moveResult.CastlingStartRook);

            moveResult = board.MakeMove(Bitboard.C1, Bitboard.G1, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void CastlingWhiteRightTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteRook, Bitboard.H1);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.G1, FigureColor.White);
            Assert.AreEqual(MoveStatus.Castling, moveResult.Status);
            Assert.AreEqual(Bitboard.F1, moveResult.CastlingFinishRook);
            Assert.AreEqual(Bitboard.H1, moveResult.CastlingStartRook);

            moveResult = board.MakeMove(Bitboard.G1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void CastlingBlackLeftTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.BlackRook, Bitboard.A8);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);
            board.SetFigure(Figure.BlackRook, Bitboard.H8);

            var moveResult = board.MakeMove(Bitboard.E8, Bitboard.C8, FigureColor.Black);
            Assert.AreEqual(MoveStatus.Castling, moveResult.Status);
            Assert.AreEqual(Bitboard.D8, moveResult.CastlingFinishRook);
            Assert.AreEqual(Bitboard.A8, moveResult.CastlingStartRook);

            moveResult = board.MakeMove(Bitboard.C8, Bitboard.G8, FigureColor.Black);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void CastlingOverBeatedPosition()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.BlackRook, Bitboard.C3);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void CastlingOverFigureTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteKnight, Bitboard.B1);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.IncorrectMove, moveResult.Status);
        }

        [TestMethod]
        public void SuccessCastlingTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.Castling, moveResult.Status);
            Assert.AreEqual(Bitboard.C1, moveResult.Finish);
            Assert.AreEqual(Bitboard.A1, moveResult.CastlingStartRook);
            Assert.AreEqual(Bitboard.D1, moveResult.CastlingFinishRook);
        }

        [TestMethod]
        public void MateAfterCastling()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.BlackKing, Bitboard.D8);
            board.SetFigure(Figure.WhiteRook, Bitboard.A7);
            board.SetFigure(Figure.WhiteRook, Bitboard.E2);
            board.SetFigure(Figure.WhiteRook, Bitboard.C2);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);

            var moveResult = board.MakeMove(Bitboard.E1, Bitboard.C1, FigureColor.White);
            Assert.AreEqual(MoveStatus.Castling, moveResult.Status);
            Assert.AreEqual(EnemyStatus.Mate, moveResult.EnemyStatus);
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
