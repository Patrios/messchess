﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class BoardMetadataTest
    {
        [TestMethod]
        public void SetWhiteKingTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKing, Bitboard.A1);

            Assert.AreEqual(Figure.WhiteKing, board.GetFigure(Bitboard.A1));
        }

        [TestMethod]
        public void SetPawnsTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhitePawn, Bitboard.B1);
            board.SetFigure(Figure.WhitePawn, Bitboard.B2);
            board.SetFigure(Figure.WhitePawn, Bitboard.B3);

            Assert.AreEqual(Figure.WhitePawn, board.GetFigure(Bitboard.B1));
            Assert.AreEqual(Figure.WhitePawn, board.GetFigure(Bitboard.B2));
            Assert.AreEqual(Figure.WhitePawn, board.GetFigure(Bitboard.B3));
        }

        [TestMethod]
        public void WhitePositionsTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKing, Bitboard.A1);
            board.SetFigure(Figure.WhitePawn, Bitboard.A2);
            board.SetFigure(Figure.WhitePawn, Bitboard.A3);

            Assert.AreEqual(
                board.GetPositionsWithColor(FigureColor.White),
                Bitboard.A1
                    .Union(Bitboard.A2)
                    .Union(Bitboard.A3));
        }

        [TestMethod]
        public void BlackPositionsTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.BlackBishop, Bitboard.A1);
            board.SetFigure(Figure.BlackKing, Bitboard.A2);
            board.SetFigure(Figure.BlackPawn, Bitboard.A3);

            Assert.AreEqual(
                board.GetPositionsWithColor(FigureColor.Black),
                Bitboard.A1
                    .Union(Bitboard.A2)
                    .Union(Bitboard.A3));
        }

        [TestMethod]
        public void NoFigureOnPositionTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            Assert.AreEqual(
                Figure.None,
                board.GetFigure(Bitboard.B2));
        }

        [TestMethod]
        public void BlackFigureOnPositionTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.BlackKing, Bitboard.B2);
            Assert.AreEqual(
                Figure.BlackKing,
                board.GetFigure(Bitboard.B2));
        }

        [TestMethod]
        public void WhiteFigureOnPositionTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhiteKnight, Bitboard.B2);
            Assert.AreEqual(
                Figure.WhiteKnight,
                board.GetFigure(Bitboard.B2));
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
