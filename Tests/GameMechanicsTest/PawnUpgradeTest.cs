﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class PawnUpgradeTest
    {
        [TestMethod]
        public void NoPawnTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            var upgradeResult = board.UpgradePawn(FigureColor.White, Figure.WhiteQueen);

            Assert.AreEqual(MoveStatus.IncorrectMove, upgradeResult.Status);
        }

        [TestMethod]
        public void WrongNewFigureTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            var upgradeResult = board.UpgradePawn(FigureColor.White, Figure.WhiteKing);

            Assert.AreEqual(MoveStatus.IncorrectMove, upgradeResult.Status);
        }

        [TestMethod]
        public void OtherColorPawnTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.BlackPawn, Bitboard.B1);
            var upgradeResult = board.UpgradePawn(FigureColor.White, Figure.WhiteQueen);

            Assert.AreEqual(MoveStatus.IncorrectMove, upgradeResult.Status);    
        }

        [TestMethod]
        public void SuccessUpgradeTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.B8);
            board.SetFigure(Figure.BlackKing, Bitboard.A1);
            var upgradeResult = board.UpgradePawn(FigureColor.White, Figure.WhiteQueen);

            Assert.AreEqual(MoveStatus.AcceptedMove, upgradeResult.Status);
            Assert.AreEqual(EnemyStatus.None, upgradeResult.EnemyStatus);        
        }

        [TestMethod]
        public void MateAfterUpgradeTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            board.SetFigure(Figure.WhitePawn, Bitboard.B8);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);
            board.SetFigure(Figure.WhiteRook, Bitboard.A7);

            var upgradeResult = board.UpgradePawn(FigureColor.White, Figure.WhiteQueen);

            Assert.AreEqual(MoveStatus.AcceptedMove, upgradeResult.Status);
            Assert.AreEqual(EnemyStatus.Mate, upgradeResult.EnemyStatus);    
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
