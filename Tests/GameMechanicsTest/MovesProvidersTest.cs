﻿using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameMechanicsTest
{
    [TestClass]
    public class MovesProvidersTest
    {
        [TestMethod]
        public void OwnPositionsFilterTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKnight, Bitboard.D4);
            board.SetFigure(Figure.WhiteKnight, Bitboard.C2);

            var knightMovesProvider = new KnightMovesProvider(board);

            var acceptedMoves =
                knightMovesProvider
                    .GetAcceptedMoves(Bitboard.D4, FigureColor.White);

            Assert.IsTrue(!acceptedMoves.HasFlag(Bitboard.C2));
        }

        [TestMethod]
        public void KnightMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKnight, Bitboard.D4);

            var knightMovesProvider = new KnightMovesProvider(board);

            var accepterPositions= 
                knightMovesProvider.GetAcceptedMoves(Bitboard.D4, FigureColor.White);

            var expectedPosition =
                Bitboard.C2
                    .Union(Bitboard.C6)
                    .Union(Bitboard.E2)
                    .Union(Bitboard.E6)
                    .Union(Bitboard.F3)
                    .Union(Bitboard.F5)
                    .Union(Bitboard.B3)
                    .Union(Bitboard.B5);

            Assert.AreEqual(expectedPosition, accepterPositions);
        }

        [TestMethod]
        public void KnightBorderTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKnight, Bitboard.A8);

            var knightMovesProvider = new KnightMovesProvider(board);

            var acceptedMoves =
                knightMovesProvider.GetAcceptedMoves(Bitboard.A8, FigureColor.White);

            Assert.AreEqual(
                Bitboard.B6.Union(Bitboard.C7),
                acceptedMoves);
        }

        [TestMethod]
        public void BishopMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteBishop, Bitboard.D4);
            board.SetFigure(Figure.WhitePawn, Bitboard.B2);
            board.SetFigure(Figure.WhitePawn, Bitboard.F2);
            board.SetFigure(Figure.BlackPawn, Bitboard.B6);

            var bishopMovesProvider = new BishopMovesProvider(board);

            var acceptedMoves =
                bishopMovesProvider.GetAcceptedMoves(Bitboard.D4, FigureColor.White);

            var expectedPosition =
                Bitboard.E3
                    .Union(Bitboard.C3)
                    .Union(Bitboard.E5)
                    .Union(Bitboard.F6)
                    .Union(Bitboard.C5)
                    .Union(Bitboard.B6)
                    .Union(Bitboard.G7)
                    .Union(Bitboard.H8);

            Assert.AreEqual(expectedPosition, acceptedMoves);
        }

        [TestMethod]
        public void RookMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteRook, Bitboard.G2);
            board.SetFigure(Figure.WhitePawn, Bitboard.F2);
            board.SetFigure(Figure.BlackPawn, Bitboard.H2);
            board.SetFigure(Figure.BlackPawn, Bitboard.G5);

            var rookMovesProvider = new RookMovesProvider(board);

            var acceptedMoves =
                rookMovesProvider.GetAcceptedMoves(Bitboard.G2, FigureColor.White);

            var expectedPosition =
                Bitboard.H2
                    .Union(Bitboard.G1)
                    .Union(Bitboard.G3)
                    .Union(Bitboard.G4)
                    .Union(Bitboard.G5);

            Assert.AreEqual(expectedPosition, acceptedMoves);
        }


        [TestMethod]
        public void QueenMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteQueen, Bitboard.G2);
            board.SetFigure(Figure.WhitePawn, Bitboard.F2);
            board.SetFigure(Figure.WhitePawn, Bitboard.E4);
            board.SetFigure(Figure.BlackPawn, Bitboard.H2);
            board.SetFigure(Figure.BlackPawn, Bitboard.G5);

            var queenMovesProvider = new QueenMovesProvider(board);

            var acceptedMoves = queenMovesProvider.GetAcceptedMoves(Bitboard.G2, FigureColor.White);

            var expectedPosition =
                Bitboard.H2
                    .Union(Bitboard.G1)
                    .Union(Bitboard.G3)
                    .Union(Bitboard.G4)
                    .Union(Bitboard.G5)
                    .Union(Bitboard.H1)
                    .Union(Bitboard.F1)
                    .Union(Bitboard.F3)
                    .Union(Bitboard.H3);

            Assert.AreEqual(expectedPosition, acceptedMoves);
        }

        [TestMethod]
        public void PawnMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhitePawn, Bitboard.A2);
            board.SetFigure(Figure.BlackPawn, Bitboard.B3);

            var pawnMovesProvider = new PawnMovesProvider(board);

            var acceptedMoves = pawnMovesProvider.GetAcceptedMoves(Bitboard.A2, FigureColor.White);
            var expectedPosition =
                Bitboard.A3
                    .Union(Bitboard.A4)
                    .Union(Bitboard.B3);
            Assert.AreEqual(expectedPosition, acceptedMoves);

            board.SetFigure(Figure.BlackKnight, Bitboard.A3);
            acceptedMoves = pawnMovesProvider.GetAcceptedMoves(Bitboard.A2, FigureColor.White);
            Assert.AreEqual(Bitboard.B3, acceptedMoves);

            board.SetFigure(Figure.WhitePawn, Bitboard.B6);
            acceptedMoves = pawnMovesProvider.GetAcceptedMoves(Bitboard.B6, FigureColor.White);
            Assert.AreEqual(Bitboard.B7, acceptedMoves);

            board.SetFigure(Figure.WhitePawn, Bitboard.B7);
            acceptedMoves = pawnMovesProvider.GetAcceptedMoves(Bitboard.B7, FigureColor.White);
            Assert.AreEqual(Bitboard.B8, acceptedMoves);
        }

        [TestMethod]
        public void KingMovesTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);

            board.SetFigure(Figure.WhiteKing, Bitboard.E1);
            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteRook, Bitboard.H1);

            var kingMovesProvider = new KingMovesProvider(board);

            var acceptedMoves = kingMovesProvider.GetAcceptedMoves(Bitboard.E1, FigureColor.White);

            var cellsAround =
                Bitboard.D1
                    .Union(Bitboard.F1)
                    .Union(Bitboard.D2)
                    .Union(Bitboard.F2)
                    .Union(Bitboard.E2);
            Assert.AreEqual(cellsAround.Union(Bitboard.C1).Union(Bitboard.G1), acceptedMoves);

            board.SetFigure(Figure.BlackQueen, Bitboard.E3);
            acceptedMoves = kingMovesProvider.GetAcceptedMoves(Bitboard.E1, FigureColor.White);
            Assert.AreEqual(cellsAround, acceptedMoves);
        }

        [TestMethod]
        public void EmptyFractionTest()
        {
            var board = new ChessBoard(_movesProviderFacadeFactory);
            Assert.AreEqual(false, board.IsUnderAttack(Bitboard.None.Negative(), FigureColor.Black));

            board.SetFigure(Figure.BlackRook, Bitboard.A1);
            Assert.AreEqual(true, board.IsUnderAttack(Bitboard.None.Negative(), FigureColor.Black));
        }

        private readonly IMovesProviderFacadeFactory _movesProviderFacadeFactory = new MovesProviderFacadeFactory();
    }
}
