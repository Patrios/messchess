﻿using System;
using System.Threading;
using Common.Crypto;
using Protocol;

namespace ClientCore
{
    public sealed class LoginProvider : IDisposable
    {
        public LoginProvider(
            IAuthenticationSevice authenticationSevice,
            TimeSpan extendSessionTimeout)
        {
            _authenticationSevice = authenticationSevice;

            _extendSessionTimer = new Timer(
                _ => ExtendSession(),
                null,
                TimeSpan.FromSeconds(0),
                extendSessionTimeout);

            /*
             * CurrentLogin = new LoginInformation(
                LoginResult.Success, 
                Guid.Empty, 
                new UserInformation("Default", new UserRatings()));
             */
        }

        public LoginResult Login(string userName, string password)
        {
            var passwordHash = CryptoUtils.GetHashedPassword(password);
            var loginInformation = _authenticationSevice.Login(userName, passwordHash);
            CurrentLogin = loginInformation;
            if (loginInformation.Result == LoginResult.Success)
            {
                _userName = userName;
                _password = passwordHash;
            }

            return loginInformation.Result;
        }

        public void Dispose()
        {
            _extendSessionTimer.Dispose();
        }

        public LoginInformation CurrentLogin { get; private set; }

        private void ExtendSession()
        {
            lock (_lockRoot)
            {
                if (CurrentLogin != null && 
                    CurrentLogin.Result == LoginResult.Success)
                {
                    _authenticationSevice.ExtendSession(
                        CurrentLogin.SessionId, 
                        _userName);
                }
            }
        }

        private string _userName;
        private string _password;

        private readonly Timer _extendSessionTimer;
        private readonly IAuthenticationSevice _authenticationSevice;
        private readonly object _lockRoot = new object();
    }
}
