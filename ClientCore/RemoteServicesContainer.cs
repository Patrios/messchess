﻿using System.ServiceModel;
using Common.WCF;
using Protocol;

namespace ClientCore
{
    public sealed class RemoteServicesContainer
    {
        public RemoteServicesContainer(
            IEndpointDescriber authenticationDescriber,
            IEndpointDescriber gameDescriber)
        {
            GameCallback = new GameCallback();

            AuthenticationSevice = 
                new ChannelFactory<IAuthenticationSevice>(
                    authenticationDescriber.GetServiceEndpoint<IAuthenticationSevice>())
                .CreateChannel();

            GameService =
                new DuplexChannelFactory<IGameService>(
                    GameCallback,
                    gameDescriber.GetServiceEndpoint<IGameService>())
                .CreateChannel();
        }

        public IAuthenticationSevice AuthenticationSevice { get; private set; }

        public IGameService GameService { get; private set; }

        public IGameCallback GameCallback { get; private set; }
    }
}
