﻿using System;
using Common.Logging;
using Common.WCF;
using Protocol;

namespace ClientCore
{
    public sealed class ClientContainer : IDisposable
    {
        public ClientContainer(
            ILogger logger,
            string serverAddress = "localhost",
            int authenticationPort = 8801,
            int gamePort = 8800)
        {
            _logger = logger;
            _remoteServicesContainer = new RemoteServicesContainer(
                new NetTcpEndpointDescriber(serverAddress, authenticationPort),
                new NetTcpEndpointDescriber(serverAddress, gamePort));

            _logger.WriteMessage("Remote services initialized.", LogLevel.Message);

            LoginProvider = new LoginProvider(
                _remoteServicesContainer.AuthenticationSevice,
                SessionExpandTimeout);

            _logger.WriteMessage("Login provider start work.", LogLevel.Message);

            GameProvider = new GameProvider(
                LoginProvider,
                _remoteServicesContainer);

            _logger.WriteMessage("Game provider start work.", LogLevel.Message);
            _logger.WriteMessage("Client initialization completed.\n\n", LogLevel.Message);
        }

        public void Dispose()
        {
            LoginProvider.Dispose();
        }

        public LoginProvider LoginProvider { get; private set; }

        public IGameProvider GameProvider { get; private set; }

        public IGameCallback GameCallback
        {
            get { return _remoteServicesContainer.GameCallback; }
        }

        private static TimeSpan SessionExpandTimeout
        {
            get { return TimeSpan.FromSeconds(30); }
        }

        private readonly RemoteServicesContainer _remoteServicesContainer;

        private readonly ILogger _logger;
    }
}
