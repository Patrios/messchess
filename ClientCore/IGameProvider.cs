﻿using Common;
using GameMechanics;
using GameMechanics.Board;
using Protocol;

namespace ClientCore
{
    public interface IGameProvider
    {
        void FindEnemy(GameMode mode);

        MoveResult MakeMove(FlatPosition start, FlatPosition finish);

        MoveResult UpgradePawn(Figure newFigure);

        CompleteResult LastGameResult { get;  }

        ChessBoard CurrentBoard { get; }

        SearchResult CurrentGame { get; }
    }
}
