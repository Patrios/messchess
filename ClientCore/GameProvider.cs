﻿using Common;
using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Protocol;

namespace ClientCore
{
    public sealed class GameProvider : IGameProvider
    {
        public GameProvider(
            LoginProvider loginProvider,
            RemoteServicesContainer remoteServicesContainer)
        {
            _loginProvider = loginProvider;
            _remoteServicesContainer = remoteServicesContainer;

            remoteServicesContainer.GameCallback.GameFoundEvent += OnEnemyFound;
            remoteServicesContainer.GameCallback.EnemyMakeMoveEvent += OnEnemyMakeMove;
            remoteServicesContainer.GameCallback.EnemyUpgradePawnEvent += OnEnemyUpgradePawn;

            remoteServicesContainer.GameCallback.GameTerminatedEvent += OnGameTerminated;
        }

        public void FindEnemy(GameMode mode)
        {
            _remoteServicesContainer.GameService.UpdateChannel(_loginProvider.CurrentLogin.SessionId);
            _remoteServicesContainer.GameService.FindEnemy(_loginProvider.CurrentLogin.SessionId, mode);
        }

        public MoveResult MakeMove(FlatPosition start, FlatPosition finish)
        {
            var localMoveResult = CurrentBoard.MakeMove(
                start.ToBitboard(), 
                finish.ToBitboard(), 
                CurrentGame.Color);

            if (localMoveResult.IsIncorrectMove())
            {
                return new MoveResult(localMoveResult, null);
            }

            var moveResult = _remoteServicesContainer.GameService.MakeMove(
                _loginProvider.CurrentLogin.SessionId,
                CurrentGame.GameId,
                start,
                finish);

            return moveResult;
        }

        public MoveResult UpgradePawn(Figure newFigure)
        {
            var localMoveResult = CurrentBoard.UpgradePawn(CurrentGame.Color, newFigure);

            if (localMoveResult.IsIncorrectMove())
            {
                return new MoveResult(localMoveResult, null);
            }

            var moveResult = _remoteServicesContainer.GameService.UpdatePawn(
                _loginProvider.CurrentLogin.SessionId,
                CurrentGame.GameId,
                newFigure);

            return moveResult;
        }

        private void OnEnemyFound(SearchResult foundGame)
        {
            CurrentBoard = new ChessBoard(
                new MovesProviderFacadeFactory());
            _boardInitialization.Initialize(CurrentBoard);

            CurrentGame = foundGame;
        }

        private void OnEnemyMakeMove(MoveResult move)
        {
            CurrentBoard.MakeMove(
                move.Information.Start,
                move.Information.Finish,
                CurrentGame.Color.EnemyColor());
        }

        private void OnEnemyUpgradePawn(MoveResult move)
        {
            CurrentBoard.UpgradePawn(
                CurrentGame.Color.EnemyColor(),
                move.Information.PawnNewSharp);
        }

        private void OnGameTerminated(CompleteResult gameResult)
        {
            LastGameResult = gameResult;
        }

        public CompleteResult LastGameResult { get; private set; }

        public ChessBoard CurrentBoard { get; private set; }

        public SearchResult CurrentGame { get; private set; }

        private readonly IBoardInitialization _boardInitialization = new StandardBoardInitialization();
        private readonly LoginProvider _loginProvider;
        private readonly RemoteServicesContainer _remoteServicesContainer;
    }
}
