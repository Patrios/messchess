﻿namespace ClientCore
{
    public enum ClientState
    {
        MainMenu,
        Search,
        Game,
        WaitEnemy,
        Results,
        NeedPawnUpgrade,
        Exit
    }
}
