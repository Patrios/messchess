﻿using System.Security.Cryptography;
using System.Text;

namespace Common.Crypto
{
    public static class CryptoUtils
    {
        public static string GetHashedPassword(string password)
        {
            var hashBytes = Hasher.ComputeHash(Encoding.GetBytes(password));
            return Encoding.GetString(hashBytes);
        }

        private static readonly Encoding Encoding = Encoding.UTF8;
        private static readonly SHA256 Hasher = SHA256.Create();
    }
}
