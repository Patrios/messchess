﻿using System.Security.Cryptography;
using System.Text;

namespace Common.Crypto
{
// ReSharper disable InconsistentNaming
    public sealed class PBKDF2HashProvider : ISaltedHashProvider
// ReSharper restore InconsistentNaming
    {
        public string Hash(string password, string salt)
        {
            var saltBytes = Encoding.UTF8.GetBytes(salt);
            var deriveBytes = new Rfc2898DeriveBytes(password, saltBytes, IterationCount);
            return Encoding.Default.GetString(deriveBytes.GetBytes(PasswordHasCount));
        }

        public string GenerateSalt()
        {
            var salt = new byte[SaltSize];
            new RNGCryptoServiceProvider().GetBytes(salt);
            return Encoding.Default.GetString(salt);
        }

        private const int SaltSize = 128;
        private const int PasswordHasCount = 64;
        private const int IterationCount = 1000;
    }
}
