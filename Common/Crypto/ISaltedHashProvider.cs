﻿namespace Common.Crypto
{
    public interface ISaltedHashProvider
    {
       string Hash(string password, string salt);

        string GenerateSalt();
    }
}
