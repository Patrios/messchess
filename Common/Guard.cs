﻿using System;

namespace Common
{
    public static class Guard
    {
        public static void CheckRange(int value, int min, int max)
        {
            if (value < min || value > max)
            {
                throw new ArgumentException(
                    string.Format("Value {0} have to be less then {1} and great then {2}", 
                    value, 
                    max, 
                    min));
            }
        }

        public static void CheckTrue(bool statement, string message)
        {
            if (!statement)
            {
                throw new ArgumentException(message);
            }
        }
    }
}
