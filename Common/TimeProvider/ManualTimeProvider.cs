﻿using System;

namespace Common.TimeProvider
{
    public sealed class ManualTimeProvider : ITimeProvider, IDrivedTimeProvider
    {
        public DateTime CurrentUtcTime()
        {
            return _isTimeStopped ? _stoppedTime.Add(_shift) : DateTime.UtcNow;
        }

        public void StopTime()
        {
            _stoppedTime = CurrentUtcTime();
            _isTimeStopped = true;
        }

        public void ShiftTime(TimeSpan addedTime)
        {
            _shift = _shift.Add(addedTime);
        }

        private DateTime _stoppedTime;
        private bool _isTimeStopped;
        private TimeSpan _shift = TimeSpan.Zero;
    }
}
