﻿using System;

namespace Common.TimeProvider
{
    public sealed class CurrentTimeProvider : ITimeProvider
    {
        public DateTime CurrentUtcTime()
        {
            return DateTime.UtcNow;
        }
    }
}
