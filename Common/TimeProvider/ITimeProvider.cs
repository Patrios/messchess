﻿using System;

namespace Common.TimeProvider
{
    public interface ITimeProvider
    {
        DateTime CurrentUtcTime();
    }
}
