﻿using System;

namespace Common.TimeProvider
{
    interface IDrivedTimeProvider
    {
        void StopTime();

        void ShiftTime(TimeSpan addedTime);
    }
}
