﻿using System.ServiceModel.Description;

namespace Common.WCF
{
    public interface IEndpointDescriber
    {
        ServiceEndpoint GetMetadataEndpoint(object service);

        ServiceEndpoint GetServiceEndpoint<TContract>();
    }
}
