﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace Common.WCF
{
    public sealed class HttpsEndpointDescriber : IEndpointDescriber
    {
        public HttpsEndpointDescriber(string address, int port, bool isDuplex)
        {
            _address = address;
            _port = port;
            _isDuplex = isDuplex;
        }

        public ServiceEndpoint GetMetadataEndpoint(object service)
        {
            var mexBinding = MetadataExchangeBindings.CreateMexHttpBinding();

            var metadataUrl = string.Format(
                "http://{0}:{1}/{2}", _address, _port, service.GetType().Name);

            return new ServiceEndpoint(
                ContractDescription.GetContract(typeof(IMetadataExchange)),
                mexBinding,
                new EndpointAddress(metadataUrl));
        }

        public ServiceEndpoint GetServiceEndpoint<TContract>()
        {
            Binding binding = null;
            if (_isDuplex)
            {
                binding = new WSDualHttpBinding();
            }
            else
            {
                binding = new WSHttpBinding(SecurityMode.None);
            }

            var serviceUrl = string.Format(
                "http://{0}:{1}/{2}", _address, _port, typeof(TContract).Name);

            return new ServiceEndpoint(
                ContractDescription.GetContract(typeof(TContract)),
                binding,
                new EndpointAddress(serviceUrl));
        }


        private readonly string _address;
        private readonly int _port;
        private readonly bool _isDuplex;
    }
}
