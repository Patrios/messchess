﻿using System.ServiceModel;
using System.ServiceModel.Description;

namespace Common.WCF
{
    public sealed class NetTcpEndpointDescriber : IEndpointDescriber
    {
        public NetTcpEndpointDescriber(string address, int port)
        {
            _address = address;
            _port = port;
        }

        public ServiceEndpoint GetMetadataEndpoint(object service)
        {
            var mexBinding = MetadataExchangeBindings.CreateMexTcpBinding();
            var metadataUrl = string.Format(
                "net.tcp://{0}:{1}/{2}Metadata", _address, _port, service.GetType().Name);
            
            return new ServiceEndpoint(
                ContractDescription.GetContract(typeof(IMetadataExchange)), 
                mexBinding, 
                new EndpointAddress(metadataUrl));
        }

        public ServiceEndpoint GetServiceEndpoint<TContract>()
        {
            var binding = new NetTcpBinding(SecurityMode.None);

            var serviceUrl = string.Format(
                "net.tcp://{0}:{1}/{2}", _address, _port, typeof(TContract).Name);

            return new ServiceEndpoint(
                ContractDescription.GetContract(typeof(TContract)),
                binding,
                new EndpointAddress(serviceUrl));
        }


        private readonly string _address;
        private readonly int _port;
    }
}
