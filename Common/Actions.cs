﻿using System;

namespace Common
{
    public static class Actions
    {
        public static Action DoNothing = () => { };

        public static Action<T> DoNothingT<T>()
        {
            return _ => { };
        }

        public static Action<T, TT> DoNothingT<T, TT>()
        {
            return (_, __) => { };
        }
    }
}
