﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public static class CollectionsExtension
    {
        public static IEnumerable<T1> GetKeysByValue<T1, T2>(
            this IEnumerable<KeyValuePair<T1, T2>> dictionary, 
            Func<T2, bool> acceptor)
        {
            return
                from keyValuePair in dictionary
                where acceptor(keyValuePair.Value)
                select keyValuePair.Key;
        }
    }
}
