﻿namespace Common.SettingsProvider
{
    public interface ISettingsProvider
    {
        int GetIntSetting(string name);

        string GetSetting(string name);
    }
}
