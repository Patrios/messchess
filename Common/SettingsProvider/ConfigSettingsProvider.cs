﻿using System.Configuration;

namespace Common.SettingsProvider
{
    public sealed class ConfigSettingsProvider : ISettingsProvider
    {
        public int GetIntSetting(string name)
        {
            return int.Parse(GetSetting(name));
        }

        public string GetSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
    }
}
