﻿namespace Common.SettingsProvider
{
    public static class SettingsProviderExtension
    {
        public static string SqlServerNameSetting(this ISettingsProvider provider)
        {
            return provider.GetSetting("SqlServerName");
        }

        public static string DatabaseNameSetting(this ISettingsProvider provider)
        {
            return provider.GetSetting("DatabaseName");
        }

        public static string ServerUriSetting(this ISettingsProvider provider)
        {
            return provider.GetSetting("ServerUri");
        }

        public static int GameServicePortSetting(this ISettingsProvider provider)
        {
            return provider.GetIntSetting("GameServicePort");
        }

        public static int AuthorizationServicePortSetting(this ISettingsProvider provider)
        {
            return provider.GetIntSetting("AuthorizationServicePort");
        }
    }
}
