﻿namespace Common
{
    public enum GameMode
    {
        ShortGame,
        SimpleGame,
        LongGame
    }
}
