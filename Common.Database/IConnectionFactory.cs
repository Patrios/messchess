﻿using System.Data;

namespace Common.Database
{
    public interface IConnectionFactory
    {
        IDbConnection CreateOpenedConnection();
    }
}
