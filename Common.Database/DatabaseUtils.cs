﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Common.Database
{
    public static class DatabaseUtils
    {
        public static void CreateDatabaseIfNotExists(string databaseName, string serverName)
        {
            using (var connection = new SqlConnection(string.Format("server={0};Trusted_Connection=True;", serverName)))
            {
                connection.Open();
                if (IsDatabaseExists(connection, databaseName))
                {
                    return;
                }

                connection.ExecuteNonQuery(string.Format("Create Database {0};", databaseName));
            }
        }

        public static void DropDatabaseIfExists(string databaseName, string serverName)
        {
            using (var connection = new SqlConnection(string.Format("server={0};Trusted_Connection=True;", serverName)))
            {
                connection.Open();
                if (!IsDatabaseExists(connection, databaseName))
                {
                    return;
                }

                connection.ExecuteNonQuery(string.Format("Drop Database {0};", databaseName));
            }
        }

        public static void CreateTableIfNotExists(
            this IDbConnection connection, 
            string tableName,
            IEnumerable<KeyValuePair<string, string>> entryFields)
        {
            var isExistsTable = connection.ExecuteScalar<int>(string.Format(
                "SELECT COUNT(*) FROM information_schema.tables WHERE table_name = '{0}'",
                tableName)) > 0;

            if (isExistsTable)
            {
                return;
            }

            connection.ExecuteNonQuery(GetEntryCreationCommand(tableName, entryFields));
        }

        public static string GetParameterName(string field)
        {
            return "@p_" + field;
        }

        public static TResult ExecuteScalar<TResult>(this IDbConnection connection, string commandText)
        {
            var command = connection.CreateCommand();
            command.CommandText = commandText;
            return (TResult)command.ExecuteScalar();
        }

        public static void ExecuteNonQuery(this IDbConnection connection, string commandText)
        {
            var command = connection.CreateCommand();
            command.CommandText = commandText;
            command.ExecuteNonQuery();
        }

        public static IDataReader ExecuteReader(this IDbConnection connection, string commandText)
        {
            var command = connection.CreateCommand();
            command.CommandText = commandText;
            return command.ExecuteReader();
        }

        private static bool IsDatabaseExists(IDbConnection connection, string databaseName)
        {
            var sqlCreateDbQuery = string.Format("SELECT COUNT(*) FROM sys.databases WHERE Name = '{0}'", databaseName);
            return connection.ExecuteScalar<int>(sqlCreateDbQuery) != 0;
        }

        private static string GetEntryCreationCommand(
            string tableName, 
            IEnumerable<KeyValuePair<string, string>> entryFields)
        {
            var creationCommandBuilder = new StringBuilder();
            creationCommandBuilder.AppendFormat("CREATE TABLE {0} (", tableName);
            foreach (var entryField in entryFields)
            {
                creationCommandBuilder.AppendFormat("{0} {1},", entryField.Key, entryField.Value);
            }

            creationCommandBuilder.Append(");");
            return creationCommandBuilder.ToString();
        }
    }
}
