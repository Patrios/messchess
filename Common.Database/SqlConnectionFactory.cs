﻿using System.Data;
using System.Data.SqlClient;

namespace Common.Database
{
    public class SqlConnectionFactory : IConnectionFactory
    {
        public SqlConnectionFactory(string sqlServerName, string databaseName)
        {
            _connectionString = string.Format(
                "Server={0};Database={1};Trusted_Connection=True;", 
                sqlServerName, 
                databaseName);
        }

        public IDbConnection CreateOpenedConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }

        private readonly string _connectionString;
    }
}
