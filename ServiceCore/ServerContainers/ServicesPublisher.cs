﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using Common.WCF;

namespace ServiceCore.ServerContainers
{
    internal sealed class ServicesPublisher : IDisposable
    {
        public ServicesPublisher(
            ServerContainer container, 
            IEndpointDescriber authorizationDescriber,
            IEndpointDescriber gameDescriber)
        {
            _authorizationServiceHost = new ServiceHost(container.AuthenticationSevice);
            _gameServiceHost = new ServiceHost(container.GameService);

            PublishService(
                container.AuthenticationSevice,
                authorizationDescriber,
                _authorizationServiceHost);

            PublishService(
                container.GameService, 
                gameDescriber,
                _gameServiceHost);
        }

        private static void PublishService<TContract>(
            TContract service, 
            IEndpointDescriber describer,
            ServiceHostBase host)
        {
            var serviceMetadataBehavior = new ServiceMetadataBehavior();
            host.Description.Behaviors.Add(serviceMetadataBehavior);

            AddServiceEndpoint(host, describer.GetServiceEndpoint<TContract>());
            AddServiceEndpoint(host, describer.GetMetadataEndpoint(service), isMex: true);

            host.Open();
        }

        public void Dispose()
        {
            _gameServiceHost.Close();
            _authorizationServiceHost.Close();
        }

        private static void AddServiceEndpoint(
            ServiceHostBase host, 
            ServiceEndpoint endpoint,
            bool isMex = false)
        {
            host.AddServiceEndpoint(
                isMex ? endpoint.Contract.ContractType.Name : endpoint.Contract.ContractType.ToString(), 
                endpoint.Binding, 
                endpoint.Address.Uri);
        }

        private readonly ServiceHost _gameServiceHost;
        private readonly ServiceHost _authorizationServiceHost;
    }
}
