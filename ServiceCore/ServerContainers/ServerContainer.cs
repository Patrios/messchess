﻿using System;
using Common.Crypto;
using Common.Database;
using Common.Logging;
using Common.TimeProvider;
using Common.WCF;
using DataAccessLayer.GameEntry;
using DataAccessLayer.Users;
using GameMechanics.Board;
using Protocol;
using ServiceCore.Authentication;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.GameSettings;
using ServiceCore.Game.MovementManager;
using ServiceCore.Game.RatingCalculator;
using ServiceCore.Game.Search;

namespace ServiceCore.ServerContainers
{
    public class ServerContainer : IDisposable
    {
        public ServerContainer(
            string sqlServer,
            string databaseName,
            IEndpointDescriber authorizationDescriber,
            IEndpointDescriber gameDescriber,
            ILogger logger)
        {
            _logger = logger;

            DatabaseUtils.CreateDatabaseIfNotExists(databaseName, sqlServer);
            _logger.WriteMessage("Database created.", LogLevel.Message);

            _connectionFactory = new SqlConnectionFactory(sqlServer, databaseName);

            _timeProvider = new CurrentTimeProvider();
            _gameRepository = new GameRepository(_connectionFactory);
            _gameRepository.CreateIfMissing();

            _logger.WriteMessage("Game repository created.", LogLevel.Message);

            _userRepository = new UserDatabaseRepository(
                _connectionFactory,
                new PBKDF2HashProvider());
            _userRepository.CreateIfMissing();

            _logger.WriteMessage("User repository created.", LogLevel.Message);

            _ratingCalculator =
                new SaveResultRatingCalculatorWrapper(
                    new EloRatingCalculator(),
                    _userRepository);

            _gameManager = new GameManager(
                _timeProvider, 
                _ratingCalculator,
                _gameRepository,
                new ChessBoardFactory(
                    new StandardBoardInitialization()));

            _logger.WriteMessage("Game manager started.", LogLevel.Message);

            _movementManager = new MovementManager(_gameManager);

            _sessionManager = new SessionManager(
                _timeProvider,
                new FallbackGameCallbackFactory(_logger),
                SessionExpireTime);

            _logger.WriteMessage("Session manager started.", LogLevel.Message);

            _gameSettingsFactory = new ChessGameSettingsFactory();

            _gameSearcher = new GameSearcher(
                _sessionManager,
                _gameManager,
                _gameSettingsFactory,
                MaxRatingDelta);

            _logger.WriteMessage("Game searcher started.", LogLevel.Message);

            AuthenticationSevice = new AuthorizationService(
                _userRepository,
                _sessionManager,
                _logger);

            GameService = new GameService(
                _sessionManager,
                _movementManager,
                _logger);

            _servicesPublisher = new ServicesPublisher(
                this, 
                authorizationDescriber,
                gameDescriber);

            _logger.WriteMessage("Services published.", LogLevel.Message);
            _logger.WriteMessage("\nServer initialization completed.\n", LogLevel.Message);
        }

        public void Dispose()
        {
            _servicesPublisher.Dispose();

            _gameSearcher.Dispose();
            _gameManager.Dispose();
            _sessionManager.Dispose();
        }

        public IAuthenticationSevice AuthenticationSevice { get; private set; }

        public IGameService GameService { get; private set; }

        private static TimeSpan SessionExpireTime
        {
            get { return TimeSpan.FromMinutes(2); }
        }

        private readonly ServicesPublisher _servicesPublisher;

        private readonly IGameManager _gameManager;
        private readonly GameSearcher _gameSearcher;

        private readonly ISessionManager _sessionManager;

        // ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
        private readonly IMovementManager _movementManager;
        private readonly IGameRepository _gameRepository;
        private readonly IUserRepository _userRepository;

        private readonly IRatingCalculator _ratingCalculator;
        private readonly ITimeProvider _timeProvider;
        private readonly ChessGameSettingsFactory _gameSettingsFactory;
        private readonly ILogger _logger;
        private readonly IConnectionFactory _connectionFactory;
        // ReSharper restore PrivateFieldCanBeConvertedToLocalVariable

        private const int MaxRatingDelta = 200;
    }
}
