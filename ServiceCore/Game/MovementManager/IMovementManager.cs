﻿using System;
using GameMechanics;
using Protocol;
using ServiceCore.Authentication.SessionControl;

namespace ServiceCore.Game.MovementManager
{
    public interface IMovementManager
    {
        MoveResult MakeMove(Session player, Guid gameId, Bitboard start, Bitboard finish);

        MoveResult UpgradePawn(Session player, Guid gameId, Figure newFigure);
    }
}
