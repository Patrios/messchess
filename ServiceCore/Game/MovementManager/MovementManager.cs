﻿using System;
using GameMechanics;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameManager;

namespace ServiceCore.Game.MovementManager
{
    public sealed class MovementManager : IMovementManager
    {
        public MovementManager(IGameManager gameManager)
        {
            _gameManager = gameManager;
        }

        public MoveResult MakeMove(Session player, Guid gameId, Bitboard start, Bitboard finish)
        {
            return ProcessMove(
                (channel, result) => channel.EnemyMakeMove(result),
                (game, color) => game.Board.MakeMove(start, finish, color),
                gameId,
                player);
        }

        public MoveResult UpgradePawn(Session player, Guid gameId, Figure newFigure)
        {
            return ProcessMove(
                (channel, result) => channel.EnemyUpgradePawn(result),
                (game, color) => game.Board.UpgradePawn(color, newFigure),
                gameId,
                player);
        }

        private MoveResult ProcessMove(
            Action<IGameCallback, MoveResult> sendMessage,
            Func<ChessGame, FigureColor, MoveInformation> getInformation,
            Guid gameId,
            Session player)
        {
            var game = _gameManager.GetGame(gameId);
            if (game == null)
            {
                return null;
            }

            var playerColor = GetPlayerColor(player, game);
            var move = getInformation(game, playerColor);
            
            var playersResult = new PlayersResult(null, null);

            if (move.IsGameEnd())
            {
                playersResult =
                    _gameManager.CompleteGame(game, GetGameResult(playerColor, move.EnemyStatus));
            }

            if (!move.IsIncorrectMove())
            {
                SendEnemyMakeMoveMessage(
                    playerColor == FigureColor.White ? game.BlackPlayer : game.WhitePlayer, 
                    new MoveResult(move, playersResult.GetResultForPlayerEnemy(playerColor)),
                    sendMessage);
            }

            if (!move.PawnUpgrade)
            {
                game.Watches.SwitchPlayers();
            }

            return new MoveResult(move, playersResult.GetResultForPlayer(playerColor));
        }

        private static void SendEnemyMakeMoveMessage(
            Session player, 
            MoveResult result,
            Action<IGameCallback, MoveResult> sendMessage)
        {
            player.ClientChannel.SendMessage(
                channel => sendMessage(channel, result),
                () => player.ClientChannel.SubscribeToChannelStateChanged(
                    (channel, state) =>
                    {
                        if (state == ChannelState.Active)
                        {
                            sendMessage(channel, result);
                        }
                    }));
        }

        private static GameResult GetGameResult(FigureColor playerColor, EnemyStatus enemyStatus)
        {
            if (enemyStatus == EnemyStatus.Stalemate)
            {
                return GameResult.Draw;
            }

            if (playerColor == FigureColor.Black)
            {
                return GameResult.BlackWin;
            }

            return GameResult.WhiteWin;
        }

        private static FigureColor GetPlayerColor(Session player, ChessGame game)
        {
            if (game.WhitePlayer == player)
            {
                return FigureColor.White;
            }

            if (game.BlackPlayer == player)
            {
                return FigureColor.Black;
            }

            return FigureColor.None;
        }

        private readonly IGameManager _gameManager;
    }
}
