﻿using System;
using Common;
using Common.TimeProvider;
using GameMechanics.Board;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameSettings;

namespace ServiceCore.Game.GameManager
{
    public sealed class ChessGame : IEquatable<ChessGame>
    {
        public ChessGame(
            Guid id,
            Session whitePlayer,
            Session blackPlayer, 
            ChessGameSettings settings, 
            GameMode mode,
            ITimeProvider timeProvider, 
            IBoard board)
        {
            Board = board;
            Watches = new GameWatches(timeProvider);
            GameStartTime = timeProvider.CurrentUtcTime();
            Mode = mode;
            Settings = settings;
            BlackPlayer = blackPlayer;
            WhitePlayer = whitePlayer;
            Id = id;
        }

        public bool Equals(ChessGame other)
        {
            if (other == null)
            {
                return false;
            }

            return Id == other.Id;
        }

        public Guid Id { get; private set; }

        public Session WhitePlayer { get; private set; }

        public GameWatches Watches { get; private set; }

        public Session BlackPlayer { get; private set; }

        public ChessGameSettings Settings { get; private set; }

        public IBoard Board { get; private set; }

        public GameMode Mode { get; private set; }

        public DateTime GameStartTime { get; private set; }
    }
}
