﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Common.TimeProvider;
using DataAccessLayer.GameEntry;
using GameMechanics;
using GameMechanics.Board;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameSettings;
using ServiceCore.Game.RatingCalculator;

namespace ServiceCore.Game.GameManager
{
    public sealed class GameManager : IGameManager, IFlusher
    {
        public GameManager(
            ITimeProvider timeProvider, 
            IRatingCalculator calculator, 
            IGameRepository gameRepository,
            IBoardFactory boardFactory)
        {
            _timeProvider = timeProvider;
            _games = new List<ChessGame>();
            _calculator = calculator;
            _gameRepository = gameRepository;
            _boardFactory = boardFactory;

            _timer = new Timer(
                _ => CheckGames(),
                null,
                TimeSpan.FromSeconds(0),
                TimeSpan.FromSeconds(5));
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public ChessGame CreateGame(Session whitePlayer, Session blackPlayer, ChessGameSettings settings)
        {
            var board = _boardFactory.Create();

            var newGame = new ChessGame(
                Guid.NewGuid(), 
                whitePlayer, 
                blackPlayer,
                settings,
                whitePlayer.SearchedMode,
                _timeProvider,
                board);

            _games.Add(newGame);
            return newGame;
        }

        public PlayersResult CompleteGame(ChessGame game, GameResult result)
        {
            _games.Remove(game);
            var changes = _calculator.RecalculateRatings(game, result);
            var gameEntry = CreateGameEntry(game, result, changes);

            _gameRepository.SaveGameResult(gameEntry);

            SetSessionState(game, SessionState.Online);

            var whitePlayerRating = game.WhitePlayer.User.Ratings.GetRating(game.Mode);
            var blackPlayerRating = game.BlackPlayer.User.Ratings.GetRating(game.Mode);

            return new PlayersResult(
                whitePlayerResult: new CompleteResult(whitePlayerRating, blackPlayerRating, result),
                blackPlayerResult: new CompleteResult(blackPlayerRating, whitePlayerRating, result));
        }

        public ChessGame GetGame(Guid gameId)
        {
            return _games.FirstOrDefault(g => g.Id == gameId);
        }

        public void CancelGame(ChessGame game, FigureColor faultedUser)
        {
            _games.Remove(game);
            SetSessionState(game, SessionState.InSearch);

            SendGameCanceledMessage(game.WhitePlayer, faultedUser == FigureColor.White);
            SendGameCanceledMessage(game.BlackPlayer, faultedUser == FigureColor.Black);
        }

        public void Flush()
        {
            CheckGames();
        }

        private static void SendGameCanceledMessage(Session session, bool yourFault)
        {
            var channel = session.ClientChannel;
            channel.SendMessage(
                onChannelActive:
                    clientCallback =>
                        clientCallback.GameCanceled(yourFault),
                onChannelUnActive:
                    () =>
                       channel.SubscribeToChannelStateChanged(
                            (callback, state) =>
                            {
                                if (state == ChannelState.Active)
                                {
                                    callback.GameCanceled(yourFault);
                                }
                            }));
        }

        private void TerminateGame(ChessGame game, GameResult result)
        {
            var gameResults = CompleteGame(game, result);
            SendTerminateMessage(game.WhitePlayer, gameResults.WhitePlayerResult);
            SendTerminateMessage(game.BlackPlayer, gameResults.BlackPlayerResult);
        }

        private static void SetSessionState(ChessGame chessGame, SessionState state)
        {
            lock (chessGame.BlackPlayer.SessionStateRoot)
            {
                chessGame.BlackPlayer.State = state;
            }

            lock (chessGame.WhitePlayer.SessionStateRoot)
            {
                chessGame.WhitePlayer.State = state;
            }
        }

        private static void SendTerminateMessage(Session player, CompleteResult result)
        {
            player.ClientChannel.SendMessage(
                channel => channel.GameTerminated(result),
                () => player.ClientChannel.SubscribeToChannelStateChanged(
                    (channel, state) =>
                    {
                        if (state == ChannelState.Active)
                        {
                            channel.GameTerminated(result);
                        }
                    }));
        }

        private static GameResult GetResultByWinner(FigureColor winner)
        {
            if (winner == FigureColor.None)
            {
                return GameResult.Draw;
            }

            return winner == FigureColor.Black ? GameResult.BlackWin : GameResult.WhiteWin;
        }

        private GameEntry CreateGameEntry(
            ChessGame game, 
            GameResult result,
            RatingChanges changes)
        {
            return 
                new GameEntry(
                    whitePlayerId: game.WhitePlayer.User.Id,
                    blackPlayerId: game.BlackPlayer.User.Id,
                    gameResult: result,
                    mode: game.Mode,
                    whitePlayerRatingDelta: changes.WhitePlayerDelta,
                    blackPlayerRatingDelta: changes.BlackPlayerDelta,
                    startTime: game.GameStartTime,
                    finishTime: _timeProvider.CurrentUtcTime());
        }

        private void CheckGames()
        {
            lock (_lockRoot)
            {
                var removedGames = new List<ChessGame>();
                var winners = new List<FigureColor>();
                foreach (var game in _games)
                {
                    var winner = GetWinner(game);
                    if (winner == FigureColor.None)
                    {
                        continue;
                    }

                    removedGames.Add(game);
                    winners.Add(winner);
                }

                for (var i = 0; i < removedGames.Count; i++)
                {
                    TerminateGame(removedGames[i],  GetResultByWinner(winners[i]));
                }
            }
        }

        private static FigureColor GetWinner(ChessGame chessGame)
        {
            if (chessGame.Watches.CurrentMovePassed > chessGame.Settings.OneMoveTime)
            {
                return chessGame.Watches.CurrentMovedPlayer.EnemyColor();
            }

            return 
                chessGame.Watches.CurrentTotal > chessGame.Settings.TotalMovesTime ? 
                    chessGame.Watches.CurrentMovedPlayer.EnemyColor() : 
                    FigureColor.None;
        }

        private readonly IBoardFactory _boardFactory;
        private readonly object _lockRoot = new object();
        private readonly IList<ChessGame> _games;
        private readonly Timer _timer;
        private readonly ITimeProvider _timeProvider;
        private readonly IRatingCalculator _calculator;
        private readonly IGameRepository _gameRepository;
    }
}
