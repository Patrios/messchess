﻿using GameMechanics;
using Protocol;

namespace ServiceCore.Game.GameManager
{
    public sealed class PlayersResult
    {
        public PlayersResult(
            CompleteResult whitePlayerResult, 
            CompleteResult blackPlayerResult)
        {
            BlackPlayerResult = blackPlayerResult;
            WhitePlayerResult = whitePlayerResult;
        }

        public CompleteResult GetResultForPlayer(FigureColor color)
        {
            return color == FigureColor.White ? WhitePlayerResult : BlackPlayerResult;
        }

        public CompleteResult GetResultForPlayerEnemy(FigureColor color)
        {
            return color == FigureColor.White ? BlackPlayerResult : WhitePlayerResult;
        }

        public CompleteResult WhitePlayerResult { get; private set; }

        public CompleteResult BlackPlayerResult { get; private set; }
    }
}
