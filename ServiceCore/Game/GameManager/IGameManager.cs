﻿using System;
using GameMechanics;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameSettings;

namespace ServiceCore.Game.GameManager
{
    public interface IGameManager : IDisposable
    {
        ChessGame CreateGame(Session whitePlayer, Session blackPlayer, ChessGameSettings settings);

        PlayersResult CompleteGame(ChessGame game, GameResult result);

        ChessGame GetGame(Guid gameId);

        void CancelGame(ChessGame game, FigureColor faultedUser);
    }
}
