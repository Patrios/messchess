﻿using System;
using Common.TimeProvider;
using GameMechanics;

namespace ServiceCore.Game.GameManager
{
    public sealed class GameWatches
    {
        public GameWatches(ITimeProvider timeProvider, FigureColor startFigure = FigureColor.White)
        {
            _timeProvider = timeProvider;
            CurrentMovedPlayer = startFigure;
            _currentMoveStart = timeProvider.CurrentUtcTime();
            _whitePlayerTotalTime = new TimeSpan();
            _blackPlayerTotalTime = new TimeSpan();
        }

        public void SwitchPlayers()
        {
            if (CurrentMovedPlayer == FigureColor.White)
            {
                _whitePlayerTotalTime += CurrentMovePassed;
            }
            else
            {
                _blackPlayerTotalTime += CurrentMovePassed;
            }


            CurrentMovedPlayer = CurrentMovedPlayer.EnemyColor();
            _currentMoveStart = _timeProvider.CurrentUtcTime();
        }

        public TimeSpan CurrentMovePassed
        {
            get { return _timeProvider.CurrentUtcTime() - _currentMoveStart; }
        }

        public TimeSpan CurrentTotal
        {
            get 
            { 
                var total = 
                    CurrentMovedPlayer == FigureColor.White ? 
                    _whitePlayerTotalTime : 
                    _blackPlayerTotalTime;
                return total + CurrentMovePassed;
            }
        }

        public FigureColor CurrentMovedPlayer { get; private set; }

        private DateTime _currentMoveStart;
        private TimeSpan _whitePlayerTotalTime;
        private TimeSpan _blackPlayerTotalTime;
        private readonly ITimeProvider _timeProvider;
    }
}
