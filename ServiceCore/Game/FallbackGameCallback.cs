﻿using System;
using System.ServiceModel;
using Common;
using Common.Logging;
using Protocol;

namespace ServiceCore.Game
{
    public class FallbackGameCallback : IFallbackClientChannel<IGameCallback>
    {
        public FallbackGameCallback(
            ChannelState defaultState,
            ILogger logger)
        {
            _logger = logger;
            State = defaultState;
            if (State == ChannelState.Active)
            {
                _remoteCallback = new GameCallback();
            }
        }

        public void SendMessage(
            Action<IGameCallback> onChannelActive, 
            Action onChannelUnActive,
            Action onException = null)
        {
            if (onException == null)
            {
                onException = Actions.DoNothing;
            }

            try
            {
                if (State == ChannelState.Active)
                {
                    onChannelActive(_remoteCallback);
                }

                if (State == ChannelState.UnActive)
                {
                    onChannelUnActive();
                }
            }
            catch (Exception e)
            {
                _logger.WriteMessage(string.Format("Client call throw exception {0}.", e), LogLevel.Error);
                onException();
            }
        }

        public void SubscribeToChannelStateChanged(Action<IGameCallback, ChannelState> onChanged)
        {
            OnChannelStateChanged += onChanged;
        }

        public void Initialize(OperationContext context)
        {
            if (State != ChannelState.UnActive)
            {
                return;
            }

            if (_channel != null)
            {
                _channel.Closed -= OnChannelClosed;
            }

            _channel = context.Channel;
            _channel.Closed += OnChannelClosed;

            _remoteCallback = context.GetCallbackChannel<IGameCallback>();
            State = ChannelState.Active;
        }
        public void Dispose()
        {
            State = ChannelState.Closed;
        }

        private ChannelState State
        {
            get { return _state; }
            set
            {
                _state = value;
                OnChannelStateChanged(_remoteCallback, State);
                UnSubscribeAllFromChannelChanged();
            }
        }

        private void OnChannelClosed(object sender, EventArgs e)
        {
            State = ChannelState.UnActive;
        }

        private void UnSubscribeAllFromChannelChanged()
        {
            OnChannelStateChanged = Actions.DoNothingT<IGameCallback, ChannelState>();
        }

        private ChannelState _state;
        private IGameCallback _remoteCallback;
        private IContextChannel _channel;

        private event Action<IGameCallback, ChannelState> OnChannelStateChanged =
            Actions.DoNothingT<IGameCallback, ChannelState>();

        private readonly ILogger _logger;

    }
}
