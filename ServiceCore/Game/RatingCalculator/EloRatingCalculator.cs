﻿using System;
using Common;
using DataAccessLayer.Users;
using GameMechanics;
using Protocol;
using ServiceCore.Game.GameManager;

namespace ServiceCore.Game.RatingCalculator
{
    public sealed class EloRatingCalculator : IRatingCalculator
    {
        public RatingChanges RecalculateRatings(ChessGame game, GameResult result)
        {
            var oldWhiteRating = game.WhitePlayer.User.Ratings.GetRating(game.Mode);
            var oldBlackRating = game.BlackPlayer.User.Ratings.GetRating(game.Mode);

            var newWhiteRating = RecalculateForPlayer(
                game.WhitePlayer.User,
                oldBlackRating,
                GetScore(result, FigureColor.White),
                game.Mode);

            var newBlackRating = RecalculateForPlayer(
                game.BlackPlayer.User,
                oldWhiteRating,
                GetScore(result, FigureColor.Black),
                game.Mode);

            return 
                new RatingChanges(newWhiteRating - oldWhiteRating, newBlackRating - oldBlackRating);
        }

        private static float GetScore(GameResult result, FigureColor playerColor)
        {
            if (result == GameResult.Draw)
            {
                return 0.5f;
            }

            if ((playerColor == FigureColor.White && result == GameResult.WhiteWin) ||
                (playerColor == FigureColor.Black && result == GameResult.BlackWin))
            {
                return 1;
            }

            return 0;
        }

        private static int RecalculateForPlayer(
            UserEntry player, 
            int enemyRating, 
            float score,
            GameMode mode)
        {
            var playerRating = player.Ratings.GetRating(mode); 
            var expectedRating = 1/(1 + Math.Pow(10, (enemyRating - playerRating) / 400.0));
            var newRating = 
                playerRating +
                (int) (GetPlayerBasedFactor(playerRating)*(score - expectedRating));
            
            player.Ratings.SetRating(mode, newRating);

            return newRating;
        }

        private static int GetPlayerBasedFactor(int rating)
        {
            if (rating > 2400)
            {
                return 10;
            }

            if (rating < UserRatings.MinRatingValue)
            {
                return 35;
            }

            return 25;
        }
    }
}
