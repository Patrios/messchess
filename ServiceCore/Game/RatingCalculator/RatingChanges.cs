﻿namespace ServiceCore.Game.RatingCalculator
{
    public sealed class RatingChanges
    {
        public RatingChanges(int whitePlayerDelta, int blackPlayerDelta)
        {
            BlackPlayerDelta = blackPlayerDelta;
            WhitePlayerDelta = whitePlayerDelta;
        }

        public int WhitePlayerDelta { get; private set; }

        public int BlackPlayerDelta { get; private set; }
    }
}
