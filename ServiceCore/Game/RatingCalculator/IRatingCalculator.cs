﻿using Protocol;
using ServiceCore.Game.GameManager;

namespace ServiceCore.Game.RatingCalculator
{
    public interface IRatingCalculator
    {
        RatingChanges RecalculateRatings(ChessGame game, GameResult result);
    }
}
