﻿using DataAccessLayer.Users;
using Protocol;
using ServiceCore.Game.GameManager;

namespace ServiceCore.Game.RatingCalculator
{
    public sealed class SaveResultRatingCalculatorWrapper : IRatingCalculator
    {
        public SaveResultRatingCalculatorWrapper(
            IRatingCalculator wrappedCalculator,
            IUserRepository userRepository)
        {
            _wrappedCalculator = wrappedCalculator;
            _userRepository = userRepository;
        }

        public RatingChanges RecalculateRatings(ChessGame game, GameResult result)
        {
            var changes = _wrappedCalculator.RecalculateRatings(game, result);

            _userRepository.SaveRatings(game.WhitePlayer.User);
            _userRepository.SaveRatings(game.BlackPlayer.User);

            return changes;
        }

        private readonly IRatingCalculator _wrappedCalculator;
        private readonly IUserRepository _userRepository;
    }
}
