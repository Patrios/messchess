﻿using Common.Logging;
using Protocol;

namespace ServiceCore.Game
{
    public class FallbackGameCallbackFactory : IFallbackGameServiceCallbackFactory
    {
        public FallbackGameCallbackFactory(ILogger logger)
        {
            _logger = logger;
        }

        public IFallbackClientChannel<IGameCallback> Create()
        {
            return new FallbackGameCallback(ChannelState.UnActive, _logger);
        }

        private readonly ILogger _logger;
    }
}
