﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Common;
using GameMechanics;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.GameSettings;

namespace ServiceCore.Game.Search
{
    public sealed class GameSearcher : IFlusher, IDisposable
    {
        public GameSearcher(
            ISessionManager sessionManager, 
            IGameManager gameManager,
            ChessGameSettingsFactory settingsFactory,
            int maxRatingDelta)
        {
            _sessionManager = sessionManager;
            _gameManager = gameManager;
            _settingsFactory = settingsFactory;
            _maxRatingDelta = maxRatingDelta;

            _timer = new Timer(
                _ => FormGames(),
                null,
                TimeSpan.FromSeconds(0),
                TimeSpan.FromSeconds(5));
        }

        public void Flush()
        {
            FormGames();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        private void FormGames()
        {
            lock (_lockRoot)
            {
                var sessionsInSearch = _sessionManager.GetInSearchSessions();

                foreach (var session in sessionsInSearch)
                {
                    lock (session.SessionStateRoot)
                    {
                        if (session.State != SessionState.InSearch)
                        {
                            continue;
                        }

                        var enemySession = GetEnemy(session, sessionsInSearch);

                        if (enemySession == null || enemySession.IsEmpty())
                        {
                            continue;
                        }

                        lock (enemySession.SessionStateRoot)
                        {
                            if (enemySession.State == SessionState.InSearch)
                            {
                                CreateGame(session, enemySession);
                            }
                        }
                    }
                }
            }
        }

        private Session GetEnemy(Session player, IEnumerable<Session> sessionsInSearch)
        {
            var gameMode = player.SearchedMode;

            var allOthers =
                sessionsInSearch.Where(
                    enemy => enemy.SessionId != player.SessionId &&
                        enemy.State == SessionState.InSearch &&
                        enemy.SearchedMode == gameMode)
                    .Where(enemy => GetRatingDelta(enemy, player, gameMode) < _maxRatingDelta)
                    .ToArray();

            if (!allOthers.Any())
            {
                return Session.EmptySession;
            }

            var minRatingDelta = allOthers.Min(enemy => GetRatingDelta(enemy, player, gameMode));
            return allOthers.FirstOrDefault(enemy => GetRatingDelta(enemy, player, gameMode) == minRatingDelta);
        }

        private static int GetRatingDelta(Session player1, Session player2, GameMode mode)
        {
            return Math.Abs(player1.User.Ratings.GetRating(mode) - player2.User.Ratings.GetRating(mode));
        }

        private void CreateGame(Session whitePlayer, Session blackSession)
        {
            whitePlayer.State = SessionState.InGame;
            blackSession.State = SessionState.InGame;

            var game = _gameManager.CreateGame(
                whitePlayer, 
                blackSession, 
                _settingsFactory.Create(whitePlayer.SearchedMode));

            SendGameFoundSignal(whitePlayer, FigureColor.White, game);
            SendGameFoundSignal(blackSession, FigureColor.Black, game);
        }

        private void SendGameFoundSignal(Session player, FigureColor color, ChessGame game)
        {
            if (player.State == SessionState.InGame)
            {
                 player.ClientChannel.SendMessage(
                        onChannelActive:
                            clientCallback =>
                                clientCallback.GameFound(GetGameSearcResult(game, color)),
                        onChannelUnActive:
                            () => _gameManager.CancelGame(game, color),
                        onException:
                            () => _gameManager.CancelGame(game, color));
            }
        }

        private static SearchResult GetGameSearcResult(ChessGame game, FigureColor color)
        {
            var enemy = color == FigureColor.White ? game.BlackPlayer.User : game.WhitePlayer.User;
            return new SearchResult(
                gameId: game.Id,
                enemyName: enemy.DisplayName,
                enemyRating: enemy.Ratings.GetRating(game.Mode),
                color: color,
                mode: game.Mode);
        }

        private readonly object _lockRoot = new object();
        private readonly ISessionManager _sessionManager;
        private readonly IGameManager _gameManager;
        private readonly ChessGameSettingsFactory _settingsFactory;
        private readonly int _maxRatingDelta;
        private readonly Timer _timer;
    }
}
