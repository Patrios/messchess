﻿namespace ServiceCore.Game
{
    public enum ChannelState
    {
        Active,
        UnActive,
        Closed
    }
}
