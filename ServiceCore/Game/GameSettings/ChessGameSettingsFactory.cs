﻿using System;
using System.Collections.Generic;
using Common;

namespace ServiceCore.Game.GameSettings
{
    public class ChessGameSettingsFactory
    {
        public ChessGameSettingsFactory()
        {
            _gameSettings = new Dictionary<GameMode, ChessGameSettings>
            {
                {
                    GameMode.ShortGame, 
                    new ChessGameSettings(
                        totalMovesTime: TimeSpan.FromMinutes(5).Add(TimeReserve),
                        oneMoveTime: TimeSpan.FromMinutes(1).Add(TimeReserve),
                        name: "Short game.")
                },
                {
                    GameMode.SimpleGame,
                    new ChessGameSettings(
                        totalMovesTime: TimeSpan.FromHours(2).Add(TimeReserve),
                        oneMoveTime: TimeSpan.FromMinutes(5).Add(TimeReserve),
                        name: "Normal game.")
                },
                {
                    GameMode.LongGame,
                    new ChessGameSettings(
                        totalMovesTime: TimeSpan.FromDays(5).Add(TimeReserve),
                        oneMoveTime: TimeSpan.FromDays(1).Add(TimeReserve),
                        name: "Long game.")
                }
            };
        }

        public ChessGameSettings Create(GameMode mode)
        {
            if (!_gameSettings.ContainsKey(mode))
            {
                throw new ArgumentException(string.Format("Wrong mode {0}.", mode));
            }

            return _gameSettings[mode];
        }

        private readonly static TimeSpan TimeReserve = TimeSpan.FromSeconds(2);
        private readonly IDictionary<GameMode, ChessGameSettings> _gameSettings;
    }
}
