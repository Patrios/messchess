﻿using System;

namespace ServiceCore.Game.GameSettings
{
    public sealed class ChessGameSettings
    {
        public ChessGameSettings(TimeSpan totalMovesTime, TimeSpan oneMoveTime, string name)
        {
            Name = name;
            OneMoveTime = oneMoveTime;
            TotalMovesTime = totalMovesTime;
        }

        public TimeSpan TotalMovesTime { get; private set; }

        public TimeSpan OneMoveTime { get; private set; }

        public string Name { get; private set; }
    }
}
