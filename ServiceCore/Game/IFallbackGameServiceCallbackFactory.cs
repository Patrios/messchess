﻿using Protocol;

namespace ServiceCore.Game
{
    public interface IFallbackGameServiceCallbackFactory
    {
        IFallbackClientChannel<IGameCallback> Create();
    }
}
