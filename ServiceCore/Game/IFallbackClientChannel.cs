﻿using System;
using System.ServiceModel;

namespace ServiceCore.Game
{
    public interface IFallbackClientChannel<T> : IDisposable
    {
        void SendMessage(
            Action<T> onChannelActive,
            Action onChannelUnActive,
            Action onException = null);

        void SubscribeToChannelStateChanged(Action<T, ChannelState> onChanged);

        void Initialize(OperationContext context);
    }
}
