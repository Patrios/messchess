﻿using System;
using System.ServiceModel;
using Common;
using Common.Logging;
using GameMechanics;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.MovementManager;

namespace ServiceCore.Game
{
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple, 
        InstanceContextMode = InstanceContextMode.Single)]
    public sealed class GameService : IGameService
    {
        public GameService(
            ISessionManager sessionManager,
            IMovementManager movementManager,
            ILogger logger)
        {
            _sessionManager = sessionManager;
            _movementManager = movementManager;
            _logger = logger;
        }

        public MoveResult MakeMove(Guid sessionId, Guid gameId, FlatPosition start, FlatPosition finish)
        {
            var session =_sessionManager.GetSession(sessionId);
            if (session.IsEmpty())
            {
                return null;
            }

            return _movementManager.MakeMove(session, gameId, start.ToBitboard(), finish.ToBitboard());
        }

        public MoveResult UpdatePawn(Guid sessionId, Guid gameId, Figure newFigure)
        {
            var session = _sessionManager.GetSession(sessionId);
            if (session.IsEmpty())
            {
                return null;
            }

            return _movementManager.UpgradePawn(session, gameId, newFigure);
        }

        public void FindEnemy(Guid sessionId, GameMode mode)
        {
            _logger.WriteMessage(
                string.Format("User with Id {0} try to find mode {1}", sessionId, mode), LogLevel.Message);
            var session = _sessionManager.GetSession(sessionId);
            if (session.IsEmpty())
            {
                return;    
            }

            lock (session.SessionStateRoot)
            {
                session.SearchedMode = mode;
                session.State = SessionState.InSearch;
            }
        }

        public void StopToFind(Guid sessionId)
        {
            var session = _sessionManager.GetSession(sessionId); 
            if (session.IsEmpty())
            {
                return;
            }

            lock (session.SessionStateRoot)
            {
                if (session.State == SessionState.InSearch)
                {
                    session.State = SessionState.Online;
                }
            }
        }

        public void UpdateChannel(Guid sessionId)
        {
            var session = _sessionManager.GetSession(sessionId);

            if (session.IsEmpty())
            {
                return;
            }

            if (session.State != SessionState.Expire)
            {
                session.ClientChannel.Initialize(OperationContext.Current);
            }
        }

        private readonly ISessionManager _sessionManager;
        private readonly IMovementManager _movementManager;
        private readonly ILogger _logger;
    }
}
