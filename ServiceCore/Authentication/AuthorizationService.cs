﻿using System;
using System.ServiceModel;
using Common.Logging;
using DataAccessLayer.Users;
using Protocol;
using ServiceCore.Authentication.SessionControl;

namespace ServiceCore.Authentication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public sealed class AuthorizationService : IAuthenticationSevice
    {
        public AuthorizationService(
            IUserRepository userRepository,
            ISessionManager sessionManager,
            ILogger logger)
        {
            _userRepository = userRepository;
            _sessionManager = sessionManager;
            _logger = logger;
        }

        public LoginInformation Login(string userName, string password)
        {
            try
            {
                var user = _userRepository.GetUser(userName, password);

                if (user.IsEmptyUser())
                {
                    return new LoginInformation(LoginResult.WrongNameOrPassword);
                }

                // ToDo: Сделать проверку на существование сессии для пользователя
                var sessionId = _sessionManager.CreateSession(user).SessionId;
                _logger.WriteMessage(
                    string.Format("User with id {0} logined.", sessionId),
                    LogLevel.Message);

                return new LoginInformation(
                    LoginResult.Success,
                    sessionId, 
                    new UserInformation(user.DisplayName, user.Ratings));
            }
            catch(Exception exception)
            {
                _logger.WriteObject(exception, LogLevel.Error);
                return new LoginInformation(LoginResult.UnknownServerException);
            }
        }

        public Guid ExtendSession(Guid sessionId, string userName)
        {
            var updatedSession = _sessionManager.UpdateSession(sessionId, userName);

            if (updatedSession.IsEmpty())
            {
                _logger.WriteMessage(
                    string.Format("User with name {0} try to extend bad session", userName),
                    LogLevel.Warning);
            }

            return updatedSession.SessionId;
        }

        private readonly IUserRepository _userRepository;
        private readonly ISessionManager _sessionManager;
        private readonly ILogger _logger;
    }
}
