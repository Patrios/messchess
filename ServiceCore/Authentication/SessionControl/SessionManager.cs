﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Common.TimeProvider;
using DataAccessLayer.Users;
using ServiceCore.Game;

namespace ServiceCore.Authentication.SessionControl
{
    public sealed class SessionManager : ISessionManager, IFlusher
    {
        public SessionManager(
            ITimeProvider timeProvider,
            IFallbackGameServiceCallbackFactory gameServiceCallbackFactory,
            TimeSpan sessionExpireSpan)
        {
            _timeProvider = timeProvider;
            _gameServiceCallbackFactory = gameServiceCallbackFactory;
            _sessionExpireSpan = sessionExpireSpan;

            _timer = new Timer(
                _ => CheckSessions(),
                null,
                TimeSpan.FromSeconds(0),
                TimeSpan.FromSeconds(10));
        }

        public Session CreateSession(UserEntry user)
        {
            var existsSession = _sessions.FirstOrDefault(session => session.User.Id == user.Id);
            if (existsSession != null)
            {
                existsSession.ClientChannel.Dispose();
                existsSession.State = SessionState.Expire;
                _sessions.Remove(existsSession);
            }

            var newSession = new Session(
                Guid.NewGuid(), 
                user, 
                _gameServiceCallbackFactory.Create())
            {
                SessionLastUpdate = _timeProvider.CurrentUtcTime(),
                State = SessionState.Online
            };

            _sessions.Add(newSession);

            return newSession;
        }

        public Session UpdateSession(Guid sessionId, string userName)
        {
            var session = _sessions.FirstOrDefault(
                s => s.SessionId == sessionId && s.User.UserName == userName);
            if (session == null)
            {
                return Session.EmptySession;
            }

            session.SessionLastUpdate = _timeProvider.CurrentUtcTime();
            return session;
        }

        public Session GetSession(Guid sessionId)
        {
            var session = _sessions.FirstOrDefault(s => s.SessionId == sessionId);
            if (session == null)
            {
                return Session.EmptySession;
            }

            return session;
        }

        public Session[] GetInSearchSessions()
        {
            return _sessions.Where(session => session.State == SessionState.InSearch).ToArray();
        }

        public void Flush()
        {
            CheckSessions();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        private void CheckSessions()
        {
            var removedSessions = new List<Session>();
            foreach (var session in _sessions)
            {
                if (session.State != SessionState.InGame &&
                    (_timeProvider.CurrentUtcTime() - session.SessionLastUpdate) > _sessionExpireSpan)
                {
                    session.State = SessionState.Expire;
                    removedSessions.Add(session);
                }
            }

            foreach (var sessionInformation in removedSessions)
            {
                _sessions.Remove(sessionInformation);
            }
        }

        private readonly IList<Session> _sessions =
            new List<Session>();

        private readonly Timer _timer;
        private readonly ITimeProvider _timeProvider;
        private readonly IFallbackGameServiceCallbackFactory _gameServiceCallbackFactory;
        private readonly TimeSpan _sessionExpireSpan;
    }
}
