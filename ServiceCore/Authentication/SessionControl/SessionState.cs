﻿namespace ServiceCore.Authentication.SessionControl
{
    public enum SessionState
    {
        Online,
        InSearch,
        InGame,
        Expire
    }
}
