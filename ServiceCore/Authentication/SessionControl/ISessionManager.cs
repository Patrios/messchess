﻿using System;
using DataAccessLayer.Users;

namespace ServiceCore.Authentication.SessionControl
{
    public interface ISessionManager : IDisposable
    {
        Session CreateSession(UserEntry user);

        Session UpdateSession(Guid sessionId, string userName);

        Session GetSession(Guid sessionId);

        Session[] GetInSearchSessions();
    }
}
