﻿using System;
using Common;
using Common.Logging;
using DataAccessLayer.Users;
using Protocol;
using ServiceCore.Game;

namespace ServiceCore.Authentication.SessionControl
{
    [Serializable]
    public sealed class Session : IEquatable<Session>
    {
        public Session(
            Guid sessionId, 
            UserEntry user, 
            IFallbackClientChannel<IGameCallback> clientChannel)
        {
            SessionId = sessionId;
            _user = user;
            _clientChannel = clientChannel;
        }

        bool IEquatable<Session>.Equals(Session other)
        {
            if (other == null)
            {
                return false;
            }

            return SessionId == other.SessionId;
        }

        public bool IsEmpty()
        {
            return SessionId == Guid.Empty;
        }

        public Guid SessionId { get; private set; }

        public DateTime SessionLastUpdate
        {
            get { return _sessionLastUpdate; }
            set { _sessionLastUpdate = value; }
        }

        public SessionState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public GameMode SearchedMode { get; set; }

        public IFallbackClientChannel<IGameCallback> ClientChannel
        {
            get { return _clientChannel; }
        }

        public UserEntry User
        {
            get
            {
                return _user;
            }
        }

        public Object SessionStateRoot = new object();

        public static readonly Session EmptySession = 
            new Session(
                Guid.Empty, 
                UserEntry.EmptyEntry, 
                new FallbackGameCallback(ChannelState.UnActive, new EmptyLogger()));

        private SessionState _state;

        [NonSerialized] private readonly IFallbackClientChannel<IGameCallback> _clientChannel;
        [NonSerialized] private DateTime _sessionLastUpdate;
        [NonSerialized] private readonly UserEntry _user;
    }
}
