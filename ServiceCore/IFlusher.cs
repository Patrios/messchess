﻿namespace ServiceCore
{
    public interface IFlusher
    {
        void Flush();
    }
}
