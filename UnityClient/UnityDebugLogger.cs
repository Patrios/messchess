﻿using System;
using Common.Logging;
using UnityEngine;

namespace Utils
{
    public sealed class UnityDebugLogger : ILogger
    {
        public void WriteMessage(string message, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Warning:
                    Debug.LogWarning(message);
                    break;
                case LogLevel.Message:
                    Debug.Log(message);
                    break;
                case LogLevel.Error:
                    Debug.LogError(message);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("level");
            }
        }

        public void WriteObject(object loggedObject, LogLevel level)
        {
            WriteMessage(loggedObject.ToString(), level);
        }
    }
}
