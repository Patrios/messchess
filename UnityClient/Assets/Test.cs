﻿using System.ServiceModel;
using System.Text;
using ClientCore;
using Common.Crypto;
using Common.Logging;
using Common.WCF;
using Protocol;
using UnityEngine;

namespace Assets
{
    public class Test : MonoBehaviour
    {
        public Test()
        {
            var logger = new UnityDebugLogger();
            /*
            var describer = //new NetTcpEndpointDescriber("localhost", 8801);
                new HttpsEndpointDescriber("localhost", 8801, isDuplex: false); 
            var gameCallback = new GameCallback();

            var channelFactory =
                new ChannelFactory<IAuthenticationSevice>(
                    describer.GetServiceEndpoint<IAuthenticationSevice>());
           
           /* var authenticationSevice = channelFactory.CreateChannel();
            var password = CryptoUtils.GetHashedPassword("Test");

            var encodedPassword = Encoding.UTF8.GetBytes(password);
            var result = authenticationSevice.Login("Test", encodedPassword);
            */
             _container = new ClientContainer(logger);

            _container.LoginProvider.Login("Test", "Test");
        }

        // Use this for initialization
        private void Start()
        {

        }

        // Update is called once per frame
        private void Update()
        {

        }

        private ClientContainer _container;
    }
}
