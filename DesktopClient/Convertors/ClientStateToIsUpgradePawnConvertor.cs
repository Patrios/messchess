﻿using System;
using System.Globalization;
using System.Windows.Data;
using ClientCore;

namespace DesktopClient.Convertors
{
    public sealed class ClientStateToIsUpgradePawnConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (ClientState) value == ClientState.NeedPawnUpgrade;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
