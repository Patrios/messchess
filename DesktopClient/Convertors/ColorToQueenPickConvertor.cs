﻿using GameMechanics;

namespace DesktopClient.Convertors
{
    public sealed class ColorToQueenPickConvertor : BaseFigurePickConvertor
    {
        protected override Figure WhiteFigure
        {
            get { return Figure.WhiteQueen; }
        }

        protected override Figure BlackFigure
        {
            get { return Figure.BlackQueen; }
        }
    }
}
