﻿using System;
using System.Globalization;
using System.Windows.Data;
using ClientCore;

namespace DesktopClient.Convertors
{
    public sealed class ClientStateToIsResultConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (ClientState) value == ClientState.Results;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
