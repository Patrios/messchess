﻿using GameMechanics;

namespace DesktopClient.Convertors
{
    public sealed class ColorToRookPickConvertor : BaseFigurePickConvertor
    {
        protected override Figure WhiteFigure
        {
            get { return Figure.WhiteRook; }
        }

        protected override Figure BlackFigure
        {
            get { return Figure.BlackRook; }
        }
    }
}
