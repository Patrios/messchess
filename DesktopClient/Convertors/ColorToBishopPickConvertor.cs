﻿using GameMechanics;

namespace DesktopClient.Convertors
{
    public sealed class ColorToBishopPickConvertor : BaseFigurePickConvertor
    {
        protected override Figure WhiteFigure
        {
            get { return Figure.WhiteBishop; }
        }

        protected override Figure BlackFigure
        {
            get { return Figure.BlackBishop; }
        }
    }
}
