﻿using GameMechanics;

namespace DesktopClient.Convertors
{
    public sealed class ColorToKnightPickConvertor : BaseFigurePickConvertor
    {
        protected override Figure WhiteFigure
        {
            get { return Figure.WhiteKnight; }
        }

        protected override Figure BlackFigure
        {
            get { return Figure.BlackKnight; }
        }
    }
}
