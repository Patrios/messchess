﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DesktopClient.Convertors
{
    internal sealed class IsSelectedToOpacityConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? 0.5 : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !((double) value == 0);
        }
    }
}
