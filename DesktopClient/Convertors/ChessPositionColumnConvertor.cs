﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DesktopClient.Convertors
{
    internal sealed class ChessPositionColumnConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var boardPieceSize = (double)(parameter);
            return boardPieceSize * (int)(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
