﻿using System;
using System.Globalization;
using System.Windows.Data;
using GameMechanics;

namespace DesktopClient.Convertors
{
    public abstract class BaseFigurePickConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = (FigureColor) value;

            if (color == FigureColor.None)
            {
                throw new ArgumentException("Can not convert none color.");
            }

            return color == FigureColor.White ? WhiteFigure : BlackFigure;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        protected abstract Figure WhiteFigure { get; }

        protected abstract Figure BlackFigure { get; }
    }
}
