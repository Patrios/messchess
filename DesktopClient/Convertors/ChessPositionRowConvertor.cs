﻿using System;
using System.Globalization;
using System.Windows.Data;
using GameMechanics.Board;

namespace DesktopClient.Convertors
{
    internal sealed class ChessPositionRowConvertor : IValueConverter 
    {
        public object Convert(
            object value, 
            Type targetType, 
            object parameter, 
            CultureInfo culture)
        {
            var boardPieceSize = (double) (parameter);
            return boardPieceSize * BoardUtils.BoardRank - boardPieceSize * ((int)(value) + 1);
        }

        public object ConvertBack(
            object value, 
            Type targetType, 
            object parameter, 
            CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
