﻿using System;
using System.Globalization;
using System.Windows.Data;
using ClientCore;

namespace DesktopClient.Convertors
{
    internal sealed class ClientStateToBoolConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((ClientState) value) == ClientState.WaitEnemy;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
