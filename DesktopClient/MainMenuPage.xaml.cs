﻿using System.Windows.Controls;
using DesktopClient.ViewModel;
using Protocol;

namespace DesktopClient
{
    public partial class MainMenuPage
    {
        public MainMenuPage(bool isInSearch = false)
        {
            InitializeComponent();
            _mainFrame = this.GetMainFrame();
            DataContext = new MainMenuViewModel(
                ContainerManager.Container().LoginProvider.CurrentLogin.UserInformation,
                isInSearch);

            ContainerManager.Container().GameCallback.GameFoundEvent += OnGameFound;
        }

        private void OnGameFound(SearchResult searchResult)
        {
            _mainFrame.NavigateToPage(() => new GamePage(
                new GameViewModel(ContainerManager.Container().LoginProvider.CurrentLogin.UserInformation)));
        }

        private readonly Frame _mainFrame;
    }
}
