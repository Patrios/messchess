﻿using System.Windows;

namespace DesktopClient
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
           // MainFrame.Navigate(new GamePage(new GameViewModel()));
            MainFrame.Navigate(new AuthorizationPage());
        }
    }
}
