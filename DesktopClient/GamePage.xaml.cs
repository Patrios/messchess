﻿using System;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DesktopClient.ViewModel;
using GameMechanics;
using GameMechanics.Board;

namespace DesktopClient
{
    public partial class GamePage
    {
        public GamePage(GameViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
            _mainFrame = this.GetMainFrame();

            viewModel.GoToMainMenuEvent += ViewModelOnGoToMainMenuEvent;
        }

        private void GoToMainMenu(object sender, RoutedEventArgs e)
        {
            ViewModelOnGoToMainMenuEvent(false);
        }

        private void ViewModelOnGoToMainMenuEvent(bool isInSearch)
        {
            _mainFrame.NavigateToPage(() => new MainMenuPage(isInSearch));
        }

        private void BoardClick(object sender, MouseButtonEventArgs e)
        {
            var dataContext = (DataContext as GameViewModel);
            if (dataContext == null)
            {
                throw new ApplicationException("Data Context could be only GameViewModel object.");
            }

            var point = Mouse.GetPosition(BoardUnits);

            var pieceSize = (double)Resources["BoardPieceSize"];

            var row = (BoardUtils.BoardRank - (int) (point.Y/pieceSize)) - 1;
            var column = (int) (point.X / pieceSize);

            dataContext.TrySelectFigureOrMakeMove(new FlatPosition(row, column));
        }

        private void VkShare(object sender, RoutedEventArgs routedEventArgs)
        {
            if (string.IsNullOrEmpty(_vkAccessToken))
            {
                SharePopup.IsOpen = true;
                GetCode();
            }
            else
            {
                VkSendPost();
            }
        }

        private void VkSendPost()
        {
            var dataContext = (DataContext as GameViewModel);
            var webClient = new WebClient();
            if (dataContext != null)
            {
                webClient.DownloadString(
                    string.Format(VkPostUrl, _vkAccessToken, dataContext.ShareMessage()));
            }

            ViewModelOnGoToMainMenuEvent(false);
        }

        private void GetCode()
        {
            ShareBrowser.Navigated += (sender, args) =>
            {
                if (args.Uri.Fragment.StartsWith(VkCodeFragment))
                {

                    SharePopup.IsOpen = false;
                    var code = args.Uri.Fragment.Substring(VkCodeFragment.Length);
                    _vkAccessToken = GetToken(code);
                    VkSendPost();
                }
            };

            ShareBrowser.Navigate(string.Format(VkAuthorizeCodeUrl, VkApplicationId));
        }

        private string GetToken(string code)
        {
            if (string.IsNullOrEmpty(_vkAccessToken))
            {
                var webClient = new WebClient();
                var response = webClient.DownloadString(
                    string.Format(VkAuthorizeTokenUrl, VkApplicationId, VkSecurityKey, code));

                var jsonResponse = new JavaScriptSerializer().Deserialize<VkJsonTokenResponse>(response);
                _vkAccessToken = jsonResponse.access_token;
            }

            return _vkAccessToken;
        }


        private string _vkAccessToken = string.Empty;
        private const string VkApplicationId = "4748401";
        private const string VkSecurityKey = "RjoBsKNEIk5wb4vD5j62";

        private const string VkPostUrl =
            "https://api.vkontakte.ru/method/wall.post?access_token={0}&message={1}";

        private const string VkAuthorizeCodeUrl =
            "http://api.vkontakte.ru/oauth/authorize?client_id={0}&scope=offline,wall";

        private const string VkAuthorizeTokenUrl =
            "https://api.vkontakte.ru/oauth/access_token?client_id={0}&client_secret={1}&code={2}";

        private const string VkCodeFragment = "#code=";

        private readonly Frame _mainFrame;

        private sealed class VkJsonTokenResponse
        {
            // ReSharper disable InconsistentNaming
            public string access_token { get; set; }

            public string expires_in { get; set; }

            public string user_id { get; set; }
            // ReSharper restore InconsistentNaming
        }
    }
}
