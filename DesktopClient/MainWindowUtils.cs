﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace DesktopClient
{
    internal static class MainWindowUtils
    {
        public static void NavigateToPage(this Frame mainFrame, Func<Page> page)
        {
            mainFrame
                .Dispatcher
                .Invoke(
                    DispatcherPriority.Background,
                    (Action)(() => mainFrame.Navigate(page())));
        }

        public static Frame GetMainFrame(this Page currentPage)
        {
            return (Application.Current.MainWindow as MainWindow).MainFrame;
        }
    }
}
