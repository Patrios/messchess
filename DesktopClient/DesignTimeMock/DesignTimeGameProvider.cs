﻿using System;
using ClientCore;
using Common;
using GameMechanics;
using GameMechanics.AcceptedMovement;
using GameMechanics.Board;
using Protocol;

namespace DesktopClient.DesignTimeMock
{
    public sealed class DesignTimeGameProvider : IGameProvider
    {
        public DesignTimeGameProvider()
        {
            CurrentGame = new SearchResult(
                Guid.Empty,
                "Enemy",
                1000,
                FigureColor.White,
                GameMode.SimpleGame);

            var boardInitialization = new WhitePawnsInitialization();
            CurrentBoard = new ChessBoard(new MovesProviderFacadeFactory());
            boardInitialization.Initialize(CurrentBoard);
        }

        public void FindEnemy(GameMode mode)
        {
            throw new System.NotImplementedException();
        }

        public MoveResult MakeMove(FlatPosition start, FlatPosition finish)
        {
            return new MoveResult(
                CurrentBoard.MakeMove(
                    start.ToBitboard(),
                    finish.ToBitboard(),
                    CurrentGame.Color), 
                null);
        }

        public MoveResult UpgradePawn(Figure newFigure)
        {
            return new MoveResult(CurrentBoard.UpgradePawn(CurrentGame.Color, newFigure), null);
        }

        public CompleteResult LastGameResult { get; private set; }
        public ChessBoard CurrentBoard { get; private set; }
        public SearchResult CurrentGame { get; private set; }
    }
}
