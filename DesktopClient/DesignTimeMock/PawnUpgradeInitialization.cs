﻿using GameMechanics;
using GameMechanics.Board;

namespace DesktopClient.DesignTimeMock
{
    internal sealed class PawnUpgradeInitialization : IBoardInitialization
    {
        public void Initialize(IBoard board)
        {
            board.SetFigure(Figure.WhitePawn, Bitboard.A7);
        }
    }
}
