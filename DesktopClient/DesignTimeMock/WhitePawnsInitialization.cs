﻿using GameMechanics;
using GameMechanics.Board;

namespace DesktopClient.DesignTimeMock
{
    internal sealed class WhitePawnsInitialization : IBoardInitialization
    {
        public void Initialize(IBoard board)
        {
            board.SetFigure(Figure.WhitePawn, Bitboard.A2);
            board.SetFigure(Figure.WhitePawn, Bitboard.B2);
            board.SetFigure(Figure.WhitePawn, Bitboard.C4);
            board.SetFigure(Figure.WhitePawn, Bitboard.D2);
            board.SetFigure(Figure.WhitePawn, Bitboard.E2);
            board.SetFigure(Figure.WhitePawn, Bitboard.F2);
            board.SetFigure(Figure.WhitePawn, Bitboard.G2);
            board.SetFigure(Figure.WhitePawn, Bitboard.H2);

            board.SetFigure(Figure.WhiteKnight, Bitboard.B1);
            board.SetFigure(Figure.WhiteKnight, Bitboard.F3);
        }
    }
}
