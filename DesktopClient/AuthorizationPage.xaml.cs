﻿using System;
using System.Windows.Controls;
using DesktopClient.ViewModel;

namespace DesktopClient
{
    public partial class AuthorizationPage
    {
        public AuthorizationPage()
        {
            InitializeComponent();
            _mainFrame = this.GetMainFrame();

            var viewModel = (DataContext as AuthorizationViewModel);

            if (viewModel == null)
            {
                throw new ArgumentException("View model can be only AuthorizationViewModel");
            }

            viewModel.UserLoginSucceeded += () => 
                _mainFrame.NavigateToPage(() => new MainMenuPage());
        }

        private readonly Frame _mainFrame;
    }
}
