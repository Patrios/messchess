﻿using GameMechanics;
using Protocol;

namespace DesktopClient.ViewModel
{
    public sealed class GameResultViewModel : ViewModelBase
    {
        public GameResultViewModel()
        {
            OwnResult = GetOwnResult(GameResult.BlackWin, FigureColor.Black);
            GameResultText = GetResultText();
        }

        public void Initialize(
            CompleteResult completeResult,
            FigureColor playerColor)
        {
            OwnNewRating = completeResult.NewRating;
            EnemyResult = completeResult.NewEnemyRating;
            OwnResult = GetOwnResult(completeResult.Result, playerColor);
            GameResultText = GetResultText();
        }

        public OwnGameResult OwnResult { get; private set; }

        public int OwnNewRating
        {
            get { return _ownNewRating; }
            private set
            {
                _ownNewRating = value;
                OnPropertyChanged();
            }
        }

        public int EnemyResult
        {
            get { return _enemyResult; }
            private set
            {
                _enemyResult = value;
                OnPropertyChanged();
            }
        }

        public string GameResultText
        {
            get { return _gameResultText; }
            private set
            {
                _gameResultText = value;
                OnPropertyChanged();
            }
        }

        private static OwnGameResult GetOwnResult(GameResult result, FigureColor playerColor)
        {
            if (result == GameResult.Draw)
            {
                return OwnGameResult.Dawn;
            }

            if ((playerColor == FigureColor.White && result == GameResult.WhiteWin) ||
                (playerColor == FigureColor.Black && result == GameResult.BlackWin))
            {
                return OwnGameResult.Win;
            }

            return OwnGameResult.Loose;
        }

        private string GetResultText()
        {
            switch (OwnResult)
            {
                 case OwnGameResult.Dawn:
                    return "Ничья.";
                 case OwnGameResult.Loose:
                    return "Вы проиграли.";
                 case OwnGameResult.Win:
                    return "Вы выиграли!";
            }

            return string.Empty;
        }

        private int _ownNewRating;
        private int _enemyResult;
        private string _gameResultText;
    }
}
