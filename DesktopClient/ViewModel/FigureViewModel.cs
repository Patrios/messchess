﻿using GameMechanics;

namespace DesktopClient.ViewModel
{
    public sealed class FigureViewModel : ViewModelBase
    {
        public FigureViewModel(Figure figure, FlatPosition position)
        {
            Position = position;
            Figure = figure;
        }

        public Figure Figure { get; private set; }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public FlatPosition Position
        {
            get { return _position; }
            set
            {
                _position = value;
                OnPropertyChanged("Position");
            }
        }

        private FlatPosition _position;
        private bool _isSelected;
    }
}
