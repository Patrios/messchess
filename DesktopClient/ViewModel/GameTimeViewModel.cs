﻿using System;

namespace DesktopClient.ViewModel
{
    public sealed class GameTimeViewModel : ViewModelBase
    {
        public void FlushSelfTime()
        {
            TotalSelfTime += CurrentSelfTime;
            CurrentSelfTime = new TimeSpan();
        }

        public void FlushEnemyTime()
        {
            TotalEnemyTime += CurrentEnemyTime;
            CurrentEnemyTime = new TimeSpan();
        }

        public TimeSpan TotalSelfTime
        {
            get { return _totalSelfTime; }
            set
            {
                _totalSelfTime = value;
                OnPropertyChanged();
            }
        }
        public TimeSpan TotalEnemyTime
        {
            get { return _totalEnemyTime; }
            set
            {
                _totalEnemyTime = value;
                OnPropertyChanged();
            }
        }
        public TimeSpan CurrentSelfTime
        {
            get { return _currentSelfTime; }
            set
            {
                _currentSelfTime = value;
                OnPropertyChanged();
            }
        }
        public TimeSpan CurrentEnemyTime
        {
            get { return _currentEnemyTime; }
            set
            {
                _currentEnemyTime = value;
                OnPropertyChanged();
            }
        }

        private TimeSpan _totalSelfTime;
        private TimeSpan _totalEnemyTime;
        private TimeSpan _currentSelfTime;
        private TimeSpan _currentEnemyTime;
    }
}
