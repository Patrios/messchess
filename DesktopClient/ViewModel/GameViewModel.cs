﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using ClientCore;
using DesktopClient.DesignTimeMock;
using GameMechanics;
using GameMechanics.Board;
using Protocol;

namespace DesktopClient.ViewModel
{
    public sealed class GameViewModel : ViewModelBase
    {
        public GameViewModel()
        {
            _user = new UserInformation("Default", new UserRatings());
            _gameProvider = new DesignTimeGameProvider();
            UpdateBoard();
            State = ClientState.Results;
            
            Time = new GameTimeViewModel();

            _timer = new Timer(
                _ => RecalculateTime(),
                null,
                TimeSpan.FromSeconds(0),
                TimeSpan.FromSeconds(1));

            UpgradePawnCommand = new LambdaCommand(
                parameter => UpgradePawn((Figure)parameter));

            Result = new GameResultViewModel();
        }

        public GameViewModel(UserInformation user) : this()
        {
            _user = user;
            _gameProvider = ContainerManager.Container().GameProvider;

            State = _gameProvider.CurrentGame.Color == FigureColor.Black ? ClientState.WaitEnemy : ClientState.Game;
            SubscribeToServerEvents();
            UpdateBoard();
        }

        public void TrySelectFigureOrMakeMove(FlatPosition position)
        {
            if (State != ClientState.Game)
            {
                return;
            }

            var selectedFigure = BoardViewModel.FirstOrDefault(figure => figure.IsSelected);
            if (selectedFigure != null)
            {
                if (position.Equals(selectedFigure.Position))
                {
                    selectedFigure.IsSelected = false;
                }
                else
                {
                    TryMakeMove(position, selectedFigure);
                }
            }
            else
            {
                TrySelectFigure(position);
            }
        }

        public event Action<bool> GoToMainMenuEvent;

        public FigureColor UserColor
        {
            get { return _gameProvider.CurrentGame.Color; }
        }

        public string DisplayName
        {
            get { return _user.DisplayName; }
        }

        public string EnemyDisplayName
        {
            get { return _gameProvider.CurrentGame.EnemyName; }
        }

        public int Rating
        {
            get { return _user.Ratings.GetRating(_gameProvider.CurrentGame.Mode); }
        }

        public int EnemyRating
        {
            get { return _gameProvider.CurrentGame.EnemyRating; }
        }

        public ObservableCollection<FigureViewModel> BoardViewModel
        {
            get { return _boardViewModel; }
            private set
            {
                _boardViewModel = value;
                OnPropertyChanged("BoardViewModel");
            }
        }

        public ClientState State
        {
            get { return _clientState; }
            private set
            {
                _clientState = value;
                OnPropertyChanged("State");
            }
        }

        public GameTimeViewModel Time { get; private set; }

        public GameResultViewModel Result { get; private set; }

        public ICommand UpgradePawnCommand { get; private set; }

        public ICommand TwittResultsCommand { get; private set; }

        public string ShareMessage()
        {
            var message = string.Empty;
            switch (Result.OwnResult)
            {
                case OwnGameResult.Win:
                    message += "Я выиграл игру с ";
                    break;
                case OwnGameResult.Loose:
                    message += "Я проиграл игру с ";
                    break;
                case OwnGameResult.Dawn:
                    message += "Я сыграл вничью с ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            message += string.Format("{0} ({1})", EnemyDisplayName, EnemyRating);
            return message;
        }

        private void UpgradePawn(Figure newFigure)
        {
            var result = _gameProvider.UpgradePawn(newFigure);
            ProcessMoveResult(result);
        }

        private void RecalculateTime()
        {
            var oneSecond = TimeSpan.FromSeconds(1);
            if (State == ClientState.Game || State == ClientState.NeedPawnUpgrade)
            {
                Time.CurrentSelfTime += oneSecond;
                return;
            }

            if (State == ClientState.WaitEnemy)
            {
                Time.CurrentEnemyTime += oneSecond;
                return;
            }

            _timer.Dispose();
        }

        private void TrySelectFigure(FlatPosition position)
        {
            var figureToSelect = BoardViewModel.FirstOrDefault(figure => figure.Position.Equals(position));

            if (figureToSelect != null && figureToSelect.Figure.GetColor() == _gameProvider.CurrentGame.Color)
            {
                figureToSelect.IsSelected = true;
            }
        }

        private void TryMakeMove(FlatPosition newPosition, FigureViewModel selectedFigure)
        {
            if (selectedFigure.Figure.GetColor() == _gameProvider.CurrentGame.Color && !newPosition.Equals(selectedFigure.Position))
            {
                var moveResult = _gameProvider.MakeMove(selectedFigure.Position, newPosition);
                ProcessMoveResult(moveResult);
            }
        }

        private void ProcessMoveResult(MoveResult moveResult)
        {
            if (moveResult.Information.IsIncorrectMove())
            {
                MessageBox.Show(IncorrectMoveDisplay(moveResult.Information.Status));
                return;
            }

            UpdateBoard();

            if (moveResult.GameResult != null)
            {
                FinishGame(moveResult.GameResult);
            }
            else if (moveResult.Information.PawnUpgrade)
            {
                State = ClientState.NeedPawnUpgrade;
            }
            else
            {
                Time.FlushSelfTime();
                State = ClientState.WaitEnemy;
            }
        }

        private static string IncorrectMoveDisplay(MoveStatus status)
        {
            switch (status)
            {
                case MoveStatus.IncorrectMove:
                    return "Некорректный ход.";
                case MoveStatus.NeedPawnUpgrade:
                    return "Вы должны выбрать пешку.";
                case MoveStatus.NoFigure:
                    return "Нет такой фигуры.";
                case MoveStatus.SelfCheck:
                    return "У вас шах.";
                default:
                    throw new ArgumentOutOfRangeException("status");
            }
        }

        private void UpdateBoard()
        {
            var newBoard = new List<FigureViewModel>();
            for (var i = 0; i < BoardUtils.BoardRank; i++)
            {
                for (var j = 0; j < BoardUtils.BoardRank; j++)
                {
                    var position = new FlatPosition(i, j);
                    var figure = _gameProvider.CurrentBoard.GetFigure(position.ToBitboard());

                    if (figure != Figure.None)
                    {
                        newBoard.Add(new FigureViewModel(figure, position));
                    }
                }
            }

            BoardViewModel = new ObservableCollection<FigureViewModel>(newBoard);
        }

        #region Server event handlers

        private void SubscribeToServerEvents()
        {
            var gameCallback = ContainerManager.Container().GameCallback;
            gameCallback.GameTerminatedEvent += FinishGame;
            gameCallback.EnemyMakeMoveEvent += EnemyMakeMove;
            gameCallback.EnemyUpgradePawnEvent += EnemyMakeMove;
            gameCallback.GameCanceledEvent += GameCanceled;
        }

        private void GameCanceled(bool isYourFault)
        {
            GoToMainMenuEvent(!isYourFault);
        }

        private void EnemyMakeMove(MoveResult result)
        {
            UpdateBoard();
            if (!result.Information.PawnUpgrade)
            {
                Time.FlushEnemyTime();
                State = ClientState.Game;
                if (result.GameResult != null)
                {
                    FinishGame(result.GameResult);
                }
            }
        }

        private void FinishGame(CompleteResult result)
        {
            Result.Initialize(result, _gameProvider.CurrentGame.Color);
            _user.Ratings.SetRating(_gameProvider.CurrentGame.Mode, result.NewRating);
            State = ClientState.Results;
        }

        #endregion

        private readonly IGameProvider _gameProvider;

        private readonly Timer _timer;

        private ObservableCollection<FigureViewModel> _boardViewModel;
        private readonly UserInformation _user;
        private ClientState _clientState;
    }
}
