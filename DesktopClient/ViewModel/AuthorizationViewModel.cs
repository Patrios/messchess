﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Common;
using Common.Logging;
using Protocol;

namespace DesktopClient.ViewModel
{
    public sealed class AuthorizationViewModel : ViewModelBase
    {
        public AuthorizationViewModel()
        {
            UserName = "Default";
            Password = "";
            _loginCommand = new LambdaCommand(
                parameter =>
                {
                    WaitServer = true;
                    Task.Run(() =>
                    {
                        var loginResult = 
                            ContainerManager.Container().LoginProvider.Login(UserName, Password);

                        WaitServer = false;

                        if (loginResult != LoginResult.Success)
                        {
                            InitializeError(loginResult);
                        }

                        if (loginResult == LoginResult.Success)
                        {
                            ContainerManager.Logger.WriteMessage("User login succeeded.", LogLevel.Message);
                            UserLoginSucceeded();
                        }
                    });
                });

        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value ?? string.Empty;
                OnPropertyChanged("Password");
            }
        }

        public bool WaitServer
        {
            get { return _waitServer; }
            set
            {
                _waitServer = value;
                OnPropertyChanged("WaitServer");
            }
        }

        public string LoginError
        {
            get { return _loginError; }
            set
            {
                _loginError = value;
                OnPropertyChanged("LoginError");
            }
        }

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }

        public event Action UserLoginSucceeded = Actions.DoNothing; 

        private void InitializeError(LoginResult result)
        {
            switch (result)
            {
                case LoginResult.WrongNameOrPassword:
                    LoginError = "Неверное имя пользователя или пароль.";
                    break;
                case LoginResult.UnknownServerException:
                    LoginError = "Неизвестная ошибка сервера.";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("result");
            }
        }

        private string _userName;
        private string _password;
        private bool _waitServer;
        private string _loginError;
        private readonly ICommand _loginCommand;
    }
}
