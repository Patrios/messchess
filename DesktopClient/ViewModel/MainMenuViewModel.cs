﻿using System.Windows;
using System.Windows.Input;
using Common;
using Protocol;

namespace DesktopClient.ViewModel
{
    public sealed class MainMenuViewModel : ViewModelBase
    {
        public MainMenuViewModel()
        {
            _user = new UserInformation("Default", new UserRatings());
            _exitCommand = new LambdaCommand(
                parameter => Application.Current.Shutdown());

            _findShortGameCommand = new LambdaCommand(
                parameter => FindGame(GameMode.ShortGame));

            _findSimpleGameCommand = new LambdaCommand(
                parameter => FindGame(GameMode.SimpleGame));
            
            _findLongGameCommand = new LambdaCommand(
                parameter => FindGame(GameMode.LongGame));

            ContainerManager.Container().GameCallback.GameFoundEvent +=
                result => WaitServer = false;
        }

        public MainMenuViewModel(
            UserInformation user,
            bool isInSearch) : this()
        {
            _user = user;
            WaitServer = isInSearch;
        }

        public int ShortGameRating
        {
            get { return _user.Ratings.GetRating(GameMode.ShortGame); }
        }

        public int SimpleGameRating
        {
            get { return _user.Ratings.GetRating(GameMode.SimpleGame); }
        }

        public int LongGameRating
        {
            get { return _user.Ratings.GetRating(GameMode.LongGame); }
        }

        public bool WaitServer
        {
            get { return _waitServer; }
            private set
            {
                _waitServer = value;
                OnPropertyChanged("WaitServer");
            }
        }

        public string DisplayName
        {
            get { return _user.DisplayName; }
        }

        public ICommand ExitCommand
        {
            get { return _exitCommand; }
        }

        public ICommand FindShortGameCommand
        {
            get { return _findShortGameCommand; }
        }

        public ICommand FindSimpleGameCommand
        {
            get { return _findSimpleGameCommand; }
        }

        public ICommand FindLongGameCommand
        {
            get { return _findLongGameCommand; }
        }

        private void FindGame(GameMode mode)
        {
            WaitServer = true;
            ContainerManager.Container().GameProvider.FindEnemy(mode);    
        }

        private bool _waitServer;
        private readonly UserInformation _user;
        private readonly ICommand _exitCommand;
        private readonly ICommand _findShortGameCommand;
        private readonly ICommand _findSimpleGameCommand;
        private readonly ICommand _findLongGameCommand;
    }
}
