﻿using System;
using ClientCore;
using Common.Logging;
using Common.SettingsProvider;
using Common.TimeProvider;

namespace DesktopClient
{
    public static class ContainerManager
    {
        static ContainerManager()
        {
            TimeProvider = new CurrentTimeProvider();
            var settingsProvider = new ConfigSettingsProvider();
            var servicesUri = settingsProvider.ServerUriSetting();

            Logger = new FileLogger(GetRandomLogName(), TimeProvider);
            Container =
                () => _containerObject ?? 
                    (_containerObject =
                        new ClientContainer(
                            Logger,
                            servicesUri,
                            settingsProvider.AuthorizationServicePortSetting(),
                            settingsProvider.GameServicePortSetting()));
        }

        public static ITimeProvider TimeProvider { get; private set; }

        public static ILogger Logger { get; private set; }

        public static Func<ClientContainer> Container { get; private set; }

        private static string GetRandomLogName()
        {
            return string.Format("ClientLog{0}.txt", new Random().Next(0, 1000));
        }

        private static ClientContainer _containerObject;
    }
}
