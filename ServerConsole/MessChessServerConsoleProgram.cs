﻿using System;
using Common.Logging;
using Common.SettingsProvider;
using Common.WCF;
using ServiceCore.ServerContainers;

namespace ServerConsole
{
    public static class MessChessServerConsoleProgram
    {
        static void Main()
        {
            var settingsProvider = new ConfigSettingsProvider();
            var logger = new ConsoleLogger();
            var servicesUri = settingsProvider.ServerUriSetting();
            using (new ServerContainer(
                sqlServer: settingsProvider.SqlServerNameSetting(),
                databaseName: settingsProvider.DatabaseNameSetting(),
                authorizationDescriber: 
                    new NetTcpEndpointDescriber(
                        servicesUri, 
                        settingsProvider.AuthorizationServicePortSetting()),
                gameDescriber: 
                    new NetTcpEndpointDescriber(
                        servicesUri, 
                        settingsProvider.GameServicePortSetting()),
                logger: logger))
            {
                Console.WriteLine("Write any key to stop server.");
                Console.ReadKey();
            }
        }
    }
}
