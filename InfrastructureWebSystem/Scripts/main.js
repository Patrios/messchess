﻿$(document).ready(function () {
    $("#shortGame-label").hide();
    $("#simpleGame-label").hide();
    $("#longGame-label").hide();
    $.template('games', $('#tmpl_game').html());
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            if (!GetGames.NoMore) {
                GetGames();
            }
        }
    });

    $(".find-user-button").click(function () {
        $(".games").empty();

        GetGames.NoMore = false;
        searchedUser = $(".searched-user").val();
        loadedPage = 0;
        GetGames();
    });
});

function AppendGames(games) {
    $.tmpl('games', games).appendTo('.games');
}


function GetGames() {
    $.getJSON("/UserStatistics/GetUserStatistics?displayName=" + searchedUser + "&page=" + loadedPage,
        function (data) {
            if (data.games.length == 0) {
                if (loadedPage == 0) {
                    $('.games').text("Нет игр!");
                }

                GetGames.NoMore = true;
            } else {
                if (loadedPage == 0) {
                    $.getJSON("/UserStatistics/GetGraphs?displayName=" + searchedUser,
                        function(data) {
                            DisplayFunction("#shortGame", data.shortGames);
                            DisplayFunction("#simpleGame", data.simpleGames);
                            DisplayFunction("#longGame", data.longGames);
                        });
                }

                loadedPage++;
                AppendGames(data.games);
            }
        });
}

function PointsToLine(points) {
    var result = new Array();
    for (var i = 0; i < points.length; i++) {
        result.push([i, points[i]]);
    }
    return result;
}

function DisplayFunction(mode, results) {
    if (results.length > 1) {
        $(mode + "-label").show();
        var points = PointsToLine(results);
        $.plot($(mode), [points], {
            xaxis: {
                tickSize: 1,
                tickFormatter: function(v, o) {
                    return parseInt(v);
                },
                reserveSpace: true,
                axisLabelUseCanvas: true
            },
            yaxis: {
                reserveSpace: true,
                axisLabelUseCanvas: true
            },
            lines: {
                show: true
            },
            points: {
                show: true
            }
        });
    }
}

var loadedPage;
var searchedUser = "";