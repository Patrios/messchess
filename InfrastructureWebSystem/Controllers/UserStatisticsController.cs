﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Common;
using Common.Crypto;
using Common.Database;
using DataAccessLayer.GameEntry;
using DataAccessLayer.Users;
using Protocol;

namespace InfrastructureWebSystem.Controllers
{
    public class UserStatisticsController : Controller
    {
        public UserStatisticsController()
        {
            var connectionFactory = new SqlConnectionFactory(@".\SQLSERVER", "MessChess");
            _userRepository = new UserDatabaseRepository(connectionFactory, new PBKDF2HashProvider());
            _gameRepository = new GameRepository(connectionFactory);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadGame()
        {
            return File("~/Installers/MessChess.zip", "application/zip", "MessChess.zip");
        }

        public JsonResult GetUserStatistics(string displayName, int page)
        {
            const int singlePageCount = 5;
            if (!string.IsNullOrEmpty(displayName))
            {
                var userId = _userRepository.GetUserIdByDisplayName(displayName);
                var games = _gameRepository
                    .GetGamesWithUser(userId)
                    .Skip(singlePageCount*page)
                    .Take(singlePageCount);

                var userGames = games.Select(game =>
                {
                    int enemyId;
                    int ownDelta;
                    int enemyDelta;

                    if (game.WhitePlayerId == userId)
                    {
                        enemyId = game.BlackPlayerId;
                        ownDelta = game.WhitePlayerRatingDelta;
                        enemyDelta = game.BlackPlayerRatingDelta;
                    }
                    else
                    {
                        enemyId = game.WhitePlayerId;
                        ownDelta = game.BlackPlayerRatingDelta;
                        enemyDelta = game.WhitePlayerRatingDelta;
                    }
                    return new
                    {
                        ownDelta = string.Format(ownDelta > 0 ? "+{0}" : "{0}", ownDelta),
                        enemyDelta,
                        enemyName = _userRepository.GetUserDisplayById(enemyId),
                        enemyRating = _userRepository.GetRating(game.Mode, enemyId),
                        mode = GetModeDescription(game.Mode),
                        result = GetResultDescription(game, userId),
                        startTimeDate = game.StartTime.ToShortDateString(),
                        startTime = game.StartTime.ToShortTimeString(),
                        finishTimeDate = game.FinishTime.ToShortDateString(),
                        finishTime = game.FinishTime.ToShortTimeString()
                    };
                });

                return Json(new { games = userGames }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { games = new int[0] }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGraphs(string displayName)
        {
            if (!string.IsNullOrEmpty(displayName))
            {
                var userId = _userRepository.GetUserIdByDisplayName(displayName);
                var games = _gameRepository.GetGamesWithUser(userId).ToArray();

                var userShortResults = CollectRatings(GameMode.ShortGame, games, userId);
                var userSimpleResults = CollectRatings(GameMode.SimpleGame, games, userId);
                var userLongResults = CollectRatings(GameMode.LongGame, games, userId);

                return Json(new
                {
                    shortGames = userShortResults,
                    simpleGames = userSimpleResults,
                    longGames = userLongResults
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                shortGames = new int[0],
                simpleGames = new int[0],
                longGames = new int[0]
            }, JsonRequestBehavior.AllowGet);
        }

        private static int[] CollectRatings(GameMode mode, IEnumerable<GameEntry> games, int userId)
        {
            var sortedGames = games
                .Where(game => game.Mode == mode)
                .OrderBy(game => game.StartTime);
            var userRating = new UserRatings();
            var ratings = new List<int> { userRating.GetRating(mode) };

            var currentRating = ratings.First();
            foreach (var game in sortedGames)
            {
                currentRating += game.BlackPlayerId == userId
                    ? game.BlackPlayerRatingDelta
                    : game.WhitePlayerRatingDelta;

                ratings.Add(currentRating);
            }

            return ratings.ToArray();
        }

        private static string GetResultDescription(GameEntry game, int userId)
        {
            if (game.Result == GameResult.Draw)
            {
                return "Ничья.";
            }

            if (game.Result == GameResult.WhiteWin && game.WhitePlayerId == userId)
            {
                return "Победа.";
            }

            if (game.Result == GameResult.BlackWin && game.BlackPlayerId == userId)
            {
                return "Победа.";
            }

            return "Поражение.";
        }

        private static string GetModeDescription(GameMode mode)
        {
            switch (mode)
            {
                case GameMode.ShortGame:
                    return "Блиц игра.";
                case GameMode.SimpleGame:
                    return "Стандартная игра.";
                case GameMode.LongGame:
                    return "Долгая игра.";
                default:
                    throw new ArgumentOutOfRangeException("mode");
            }
        }

        private readonly IUserRepository _userRepository;
        private readonly IGameRepository _gameRepository;
    }
}
