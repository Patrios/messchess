﻿using System;

namespace GameMechanics
{
    [Flags]
    public enum Bitboard : ulong
    {
        A1 = (ulong)1 << 0,
        B1 = (ulong)1 << 1,
        C1 = (ulong)1 << 2,
        D1 = (ulong)1 << 3,
        E1 = (ulong)1 << 4,
        F1 = (ulong)1 << 5,
        G1 = (ulong)1 << 6,
        H1 = (ulong)1 << 7,

        A2 = (ulong)1 << 8,
        B2 = (ulong)1 << 9,
        C2 = (ulong)1 << 10,
        D2 = (ulong)1 << 11,
        E2 = (ulong)1 << 12,
        F2 = (ulong)1 << 13,
        G2 = (ulong)1 << 14,
        H2 = (ulong)1 << 15,

        A3 = (ulong)1 << 16,
        B3 = (ulong)1 << 17,
        C3 = (ulong)1 << 18,
        D3 = (ulong)1 << 19,
        E3 = (ulong)1 << 20,
        F3 = (ulong)1 << 21,
        G3 = (ulong)1 << 22,
        H3 = (ulong)1 << 23,

        A4 = (ulong)1 << 24,
        B4 = (ulong)1 << 25,
        C4 = (ulong)1 << 26,
        D4 = (ulong)1 << 27,
        E4 = (ulong)1 << 28,
        F4 = (ulong)1 << 29,
        G4 = (ulong)1 << 30,
        H4 = (ulong)1 << 31,

        A5 = (ulong)1 << 32,
        B5 = (ulong)1 << 33,
        C5 = (ulong)1 << 34,
        D5 = (ulong)1 << 35,
        E5 = (ulong)1 << 36,
        F5 = (ulong)1 << 37,
        G5 = (ulong)1 << 38,
        H5 = (ulong)1 << 39,

        A6 = (ulong)1 << 40,
        B6 = (ulong)1 << 41,
        C6 = (ulong)1 << 42,
        D6 = (ulong)1 << 43,
        E6 = (ulong)1 << 44,
        F6 = (ulong)1 << 45,
        G6 = (ulong)1 << 46,
        H6 = (ulong)1 << 47,

        A7 = (ulong)1 << 48,
        B7 = (ulong)1 << 49,
        C7 = (ulong)1 << 50,
        D7 = (ulong)1 << 51,
        E7 = (ulong)1 << 52,
        F7 = (ulong)1 << 53,
        G7 = (ulong)1 << 54,
        H7 = (ulong)1 << 55,
        
        A8 = (ulong)1 << 56,
        B8 = (ulong)1 << 57,
        C8 = (ulong)1 << 58,
        D8 = (ulong)1 << 59,
        E8 = (ulong)1 << 60,
        F8 = (ulong)1 << 61,
        G8 = (ulong)1 << 62,
        H8 = (ulong)1 << 63,

        None = 0
    }
}
