﻿namespace GameMechanics
{
    public enum MoveStatus
    {
        NoFigure,
        IncorrectMove,
        SelfCheck,
        AcceptedMove,
        Castling,
        EatEnemy,
        NeedPawnUpgrade
    }
}
