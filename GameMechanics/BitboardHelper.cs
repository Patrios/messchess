﻿using System;
using System.Collections.Generic;
using GameMechanics.Board;

namespace GameMechanics
{
    public static class BitboardHelper
    {
        public static Bitboard Union(
            this Bitboard source, 
            Bitboard target)
        {
            return source | target;
        }

        public static Bitboard Common(
            this Bitboard source,
            Bitboard target)
        {
            return source & target;
        }

        public static Bitboard Negative(
            this Bitboard source)
        {
            return ~source;
        }

        public static int Cell(this Bitboard bitboard)
        {
            for (var i = 0; i < BoardUtils.CellsCount; i++)
            {
                if (((ulong)bitboard & (ulong)1 << i) != 0)
                {
                    return i;
                }
            }

            throw new ArgumentException(string.Format("Bitboard is none {0}.", bitboard));
        }

        public static IEnumerable<Bitboard> SplitPositions(this Bitboard bitboard)
        {
            var cells = new List<Bitboard>();
            for (var i = 0; i < BoardUtils.CellsCount; i++)
            {
                if (((ulong)bitboard & (ulong)1 << i) != 0)
                {
                    cells.Add((Bitboard)((ulong) 1 << i));
                }
            }

            return cells.ToArray();
        }

        public static FlatPosition ToFlat(this Bitboard bitboard)
        {
            return new FlatPosition(bitboard.Row(), bitboard.Column());
        }

        public static int Column(this Bitboard bitboard)
        {
            return bitboard.Cell() % BoardUtils.BoardRank;
        }

        public static int Row(this Bitboard bitboard)
        {
            return bitboard.Cell() / BoardUtils.BoardRank;
        }

        public static Bitboard AddColumns(this Bitboard source, int rowCount)
        {
            if (source == Bitboard.None)
            {
                return Bitboard.None;
            }

            if (source.Column() + rowCount > BoardUtils.BoardRank - 1)
            {
                return Bitboard.None;
            }

            return (Bitboard)((ulong)source << rowCount);
        }

        public static Bitboard AddRows(this Bitboard source, int columnCount)
        {
            if (source == Bitboard.None)
            {
                return Bitboard.None;
            }

            if (source.Row() + columnCount > BoardUtils.BoardRank - 1)
            {
                return Bitboard.None;
            }

            return (Bitboard)((ulong)source << columnCount * BoardUtils.BoardRank);
        }

        public static Bitboard SubColumns(this Bitboard source, int rowCount)
        {
            if (source == Bitboard.None)
            {
                return Bitboard.None;
            }

            if (source.Cell() % BoardUtils.BoardRank - rowCount < 0)
            {
                return Bitboard.None;
            }

            return (Bitboard)((ulong)source >> rowCount);
        }

        public static Bitboard SubRows(this Bitboard source, int columnCount)
        {
            if (source == Bitboard.None)
            {
                return Bitboard.None;
            }

            if (source.Cell() / BoardUtils.BoardRank - columnCount < 0)
            {
                return Bitboard.None;
            }

            return (Bitboard)((ulong)source >> columnCount * BoardUtils.BoardRank);
        }
    }
}
