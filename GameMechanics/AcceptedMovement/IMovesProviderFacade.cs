﻿using System.Collections.Generic;

namespace GameMechanics.AcceptedMovement
{
    public interface IMovesProviderFacade
    {
        Bitboard GetColoredAttacks(FigureColor color);

        IEnumerable<MoveInformation> GetColoredMoves(FigureColor color);

        MoveInformation GetMoveInformation(Bitboard start, Bitboard finish, FigureColor color);
    }
}
