﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public interface IMovesProviderFacadeFactory
    {
        IMovesProviderFacade Create(IBoardMetadata boardMetadata);
    }
}
