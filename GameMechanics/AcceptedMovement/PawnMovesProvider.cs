﻿using System;
using Common;
using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class PawnMovesProvider : BaseMovesProvider
    {
        public PawnMovesProvider(IBoardMetadata boardMetadata) : base(boardMetadata)
        {
        }

        public static bool IsUpgradePosition(FlatPosition position)
        {
            return position.Row == BoardUtils.BoardRank - 1 || position.Row == 0;
        }

        public override MoveInformation GetMoveInformation(Bitboard start, Bitboard finish)
        {
            var flatFinish = finish.ToFlat();
            var flatStart = start.ToFlat();

            if (flatFinish.Column == flatStart.Column)
            {
                return new MoveInformation
                {
                    Status = MoveStatus.AcceptedMove,
                    IsPawnDoubleMove = (Math.Abs(flatFinish.Row - flatStart.Row) == 2),
                    PawnUpgrade = IsUpgradePosition(flatFinish)
                };
            }

            Bitboard eatedPosition;
            if (BoardMetadata.GetFigure(finish) != Figure.None)
            {
                eatedPosition = finish;
            }
            else
            {
                eatedPosition = 
                    flatFinish.Column < flatStart.Column ? 
                        start.SubColumns(1) : 
                        start.AddColumns(1);
            }

            return new MoveInformation
            {
                Status = MoveStatus.EatEnemy, 
                EatedPosition = eatedPosition,
                EatedFigure = BoardMetadata.GetFigure(eatedPosition),
                PawnUpgrade = IsUpgradePosition(flatFinish)
            };
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            var pawnAttacks = CollectPawnAttacks(position, color);
            return pawnAttacks.LeftAttack.Union(pawnAttacks.RightAttack);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var moves = Bitboard.None;

            switch (color)
            {
                case FigureColor.White:
                    moves = CollectCellsOnDirection(position, AddOneRow);
                    break;
                case FigureColor.Black:
                    moves = CollectCellsOnDirection(position, SubOneRow);
                    break;
            }

            var pawnAttack = CollectPawnAttacks(position, color);

            if (pawnAttack.CanAttackLeft)
            {
                moves = moves.Union(pawnAttack.LeftAttack);
            }

            if (pawnAttack.CanAttackRight)
            {
                moves = moves.Union(pawnAttack.RightAttack);
            }

            return moves;
        }

        private PawnAttacks CollectPawnAttacks(Bitboard position, FigureColor color)
        {
            var leftAttack = Bitboard.None;
            var rightAttack = Bitboard.None;

            if (color == FigureColor.White)
            {
                leftAttack = position.AddRows(1).SubColumns(1);
                rightAttack = position.AddRows(1).AddColumns(1);
            }

            if (color == FigureColor.Black)
            {
                leftAttack = position.SubRows(1).SubColumns(1);
                rightAttack = position.SubRows(1).AddColumns(1);
            }

            return new PawnAttacks
            {
                LeftAttack = leftAttack,
                RightAttack = rightAttack,
                CanAttackLeft = CanAttack(leftAttack, color),
                CanAttackRight = CanAttack(rightAttack, color)
            };
        }

        private Bitboard CollectCellsOnDirection(Bitboard start, Func<Bitboard, Bitboard> makeStep)
        {
            var nextCell = makeStep(start);
            
            if (BoardMetadata.GetFigure(nextCell) != Figure.None)
            {
                return Bitboard.None;
            }

            var moves = nextCell;
            nextCell = makeStep(nextCell);
            if (IsCorrectOverCell(nextCell, start))
            {
                moves = moves.Union(nextCell);
            }

            return moves;
        }

        private bool IsCorrectOverCell(Bitboard overCell, Bitboard sourceCell)
        {
            return
                overCell != Bitboard.None &&
                   BoardMetadata.IsFirstMove(sourceCell)
                   && BoardMetadata.GetFigure(overCell) == Figure.None;
        }

        private bool CanAttack(Bitboard position, FigureColor color)
        {
            if (position == Bitboard.None)
            {
                return false;
            }

            if (BoardMetadata.GetPositionsWithColor(color.EnemyColor()).Common(position).HasFlag(position))
            {
                return true;
            }

            var inLinePawnAttackCell = Bitboard.None;

            if (color == FigureColor.White)
            {
                inLinePawnAttackCell = position.SubRows(1);
            }

            if (color == FigureColor.Black)
            {
                inLinePawnAttackCell = position.AddRows(1);
            }

            var figureName = BoardMetadata.GetFigure(inLinePawnAttackCell).GetFigureName();
            if (figureName == Figure.WhitePawn.GetFigureName() &&
                BoardMetadata.IsDoublePawnMove(inLinePawnAttackCell, color.EnemyColor()))
            {
                return true;
            }

            return false;
        }

        private static Bitboard AddOneRow(Bitboard source)
        {
            return source.AddRows(1);
        }

        private static Bitboard SubOneRow(Bitboard source)
        {
            return source.SubRows(1);
        }

        private class PawnAttacks
        {
            public Bitboard LeftAttack { get; set; }

            public Bitboard RightAttack { get; set; }

            public bool CanAttackLeft { get; set; }

            public bool CanAttackRight { get; set; }
        }
    }
}
