﻿using System;
using System.Linq;
using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class KingMovesProvider : BaseMovesProvider
    {
        static KingMovesProvider()
        {
            WhiteCastlingLeft =
                Bitboard.E1
                    .Union(Bitboard.C1)
                    .Union(Bitboard.D1)
                    .Union(Bitboard.B1);

            WhiteCastlingRight =
                Bitboard.E1
                    .Union(Bitboard.F1)
                    .Union(Bitboard.G1);

            BlackCastlingLeft =
                Bitboard.E8
                    .Union(Bitboard.C8)
                    .Union(Bitboard.D8)
                    .Union(Bitboard.B8);

            BlackCastlingRight =
                Bitboard.E8
                    .Union(Bitboard.F8)
                    .Union(Bitboard.G8);
        }

        public KingMovesProvider(IBoardMetadata boardMetadata) : base(boardMetadata)
        {
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            return FilterOwnPositions(CollectAround(position), color);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var kingMoves = CollectAround(position);

            if (!BoardMetadata.IsFirstMove(position))
            {
                return kingMoves;
            }

            return kingMoves.Union(CollectCastlingMoves(color));
        }

        private Bitboard CollectCastlingMoves(FigureColor color)
        {
            var castlingMoves = Bitboard.None;

            castlingMoves = 
                castlingMoves.Union(CastlingOnDirection(
                    LeftRook,
                    LeftCastlingCells,
                    LeftCastlingPosition,
                    color));

            castlingMoves =
                castlingMoves.Union(CastlingOnDirection(
                    RightRook,
                    RightCastlingCells,
                    RightCastlingPosition,
                    color));

            return castlingMoves;
        }

        private Bitboard CastlingOnDirection(
            Func<FigureColor, Bitboard> getRook,
            Func<FigureColor, Bitboard> castlingCalls,
            Func<FigureColor, Bitboard> castlingPosition,
            FigureColor color)
        {

            if (BoardMetadata.IsFirstMove(getRook(color)) &&
                !BoardMetadata.IsUnderAttack(castlingCalls(color), color.EnemyColor()) && 
                castlingCalls(color).SplitPositions().ToArray().All(
                    position => 
                    {
                        var figure = BoardMetadata.GetFigure(position);
                        return 
                            figure == Figure.None ||
                            figure.GetFigureName() == Figure.WhiteKing.GetFigureName();
                    }))
            {
                return castlingPosition(color);
            }

            return Bitboard.None;
        }

        private static Bitboard LeftCastlingPosition(FigureColor color)
        {
            return color == FigureColor.Black ? Bitboard.C8 : Bitboard.C1;
        }

        private static Bitboard RightCastlingPosition(FigureColor color)
        {
            return color == FigureColor.Black ? Bitboard.G8 : Bitboard.G1;
        }

        private static Bitboard LeftRook(FigureColor color)
        {
            return color == FigureColor.Black ? Bitboard.A8 : Bitboard.A1;
        }

        private static Bitboard RightRook(FigureColor color)
        {
            return color == FigureColor.Black ? Bitboard.H8 : Bitboard.H1;
        }

        private static Bitboard LeftCastlingCells(FigureColor color)
        {
            return color == FigureColor.Black ? BlackCastlingLeft : WhiteCastlingLeft;
        }

        private static Bitboard RightCastlingCells(FigureColor color)
        {
            return color == FigureColor.Black ? BlackCastlingRight : WhiteCastlingRight;
        }

        private static Bitboard CollectAround(Bitboard position)
        {
            return
                position.AddRows(1)
                    .Union(position.SubRows(1))
                    .Union(position.AddColumns(1))
                    .Union(position.SubColumns(1))
                    .Union(position.AddRows(1).AddColumns(1))
                    .Union(position.AddRows(1).SubColumns(1))
                    .Union(position.SubRows(1).AddColumns(1))
                    .Union(position.SubRows(1).SubColumns(1));
        }

        public override MoveInformation GetMoveInformation(Bitboard start, Bitboard finish)
        {
            if (Math.Abs(finish.Column() - start.Column()) <= 1)
            {
                return base.GetMoveInformation(start, finish);
            }

            Bitboard castlingStartRook;
            Bitboard castlingFinishRook;

            if (finish.Column() > start.Column())
            {
                castlingStartRook = 
                    new FlatPosition(start.Row(), BoardUtils.BoardRank - 1).ToBitboard();
                castlingFinishRook = finish.SubColumns(1);
            }
            else
            {
                castlingStartRook =
                    new FlatPosition(start.Row(), 0).ToBitboard();
                castlingFinishRook = finish.AddColumns(1);
            }

            return new MoveInformation
            {
                Status = MoveStatus.Castling,
                Finish = finish,
                CastlingStartRook = castlingStartRook,
                CastlingFinishRook = castlingFinishRook
            };
        }

        private static readonly Bitboard WhiteCastlingLeft;
        private static readonly Bitboard BlackCastlingLeft;

        private static readonly Bitboard WhiteCastlingRight;
        private static readonly Bitboard BlackCastlingRight;
    }
}
