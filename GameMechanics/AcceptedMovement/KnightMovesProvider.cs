﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class KnightMovesProvider : BaseMovesProvider
    {
        public KnightMovesProvider(IBoardMetadata boardMetadata)
            : base(boardMetadata)
        {
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            return AttackMovesByAccepted(position, color);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var westNorthMove = position.AddRows(1).SubColumns(2);
            var northWestMove = position.AddRows(2).SubColumns(1);
            var northEastMove = position.AddRows(2).AddColumns(1);
            var eastNorthMove = position.AddRows(1).AddColumns(2);
            var southEastMove = position.SubRows(2).AddColumns(1);
            var eastSouthMove = position.SubRows(1).AddColumns(2);
            var southWestMove = position.SubRows(2).SubColumns(1);
            var westSouthMove = position.SubRows(1).SubColumns(2);

            var knightMoves =
                westNorthMove
                    .Union(northWestMove)
                    .Union(northEastMove)
                    .Union(eastNorthMove)
                    .Union(southEastMove)
                    .Union(eastSouthMove)
                    .Union(southWestMove)
                    .Union(westSouthMove);

            return knightMoves;
        }
    }
}
