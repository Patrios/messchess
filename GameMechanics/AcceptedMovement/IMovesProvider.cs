﻿namespace GameMechanics.AcceptedMovement
{
    public interface IMovesProvider
    {
        Bitboard GetAcceptedMoves(Bitboard position, FigureColor color);

        Bitboard GetAttackPositions(Bitboard position, FigureColor color);

        MoveInformation GetMoveInformation(Bitboard start, Bitboard finish);
    }
}
