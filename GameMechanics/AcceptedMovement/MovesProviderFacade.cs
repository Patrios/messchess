﻿using System.Collections.Generic;
using System.Linq;
using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public class MovesProviderFacade : IMovesProviderFacade
    {
        public MovesProviderFacade(IBoardMetadata boardMetadata)
        {
            _figureToProviders = new Dictionary<Figure, IMovesProvider>
            {
                { Figure.BlackBishop, new BishopMovesProvider(boardMetadata) },
                { Figure.WhiteBishop, new BishopMovesProvider(boardMetadata) },

                { Figure.BlackKnight, new KnightMovesProvider(boardMetadata) },
                { Figure.WhiteKnight, new KnightMovesProvider(boardMetadata) },

                { Figure.BlackRook, new RookMovesProvider(boardMetadata) },
                { Figure.WhiteRook, new RookMovesProvider(boardMetadata) },

                { Figure.BlackPawn, new PawnMovesProvider(boardMetadata) },
                { Figure.WhitePawn, new PawnMovesProvider(boardMetadata) },

                { Figure.BlackQueen, new QueenMovesProvider(boardMetadata) },
                { Figure.WhiteQueen, new QueenMovesProvider(boardMetadata) },

                { Figure.BlackKing, new KingMovesProvider(boardMetadata) },
                { Figure.WhiteKing, new KingMovesProvider(boardMetadata) }
            };

            _boardMetadata = boardMetadata;
        }

        public Bitboard GetColoredAttacks(FigureColor color)
        {
            var figurePositions = _boardMetadata.GetPositionsWithColor(color);

            var attackedPositions = Bitboard.None;
            foreach (var figurePosition in figurePositions.SplitPositions())
            {
                var figure = _boardMetadata.GetFigure(figurePosition);
                attackedPositions = attackedPositions.Union(
                    _figureToProviders[figure].GetAttackPositions(figurePosition, color));
            }

            return attackedPositions;
        }

        public IEnumerable<MoveInformation> GetColoredMoves(FigureColor color)
        {
            var figurePositions = _boardMetadata.GetPositionsWithColor(color);

            var moves = new List<MoveInformation>();
            foreach (var figurePosition in figurePositions.SplitPositions())
            {
                var figure = _boardMetadata.GetFigure(figurePosition);
                moves.AddRange(
                    _figureToProviders[figure]
                        .GetAcceptedMoves(figurePosition, color)
                        .SplitPositions()
                        .Select(
                            acceptedMove => 
                                new MoveInformation
                                {
                                    Start = figurePosition, 
                                    Finish = acceptedMove
                                }));
            }

            return moves;
        }

        public MoveInformation GetMoveInformation(Bitboard start, Bitboard finish, FigureColor color)
        {
            var movedFigure = _boardMetadata.GetFigure(start);

            if (movedFigure.GetColor() != color)
            {
                return new MoveInformation { Status = MoveStatus.NoFigure };
            }

            if (_figureToProviders[movedFigure]
                .GetAcceptedMoves(start, color)
                .Common(finish) == Bitboard.None)
            {
                return new MoveInformation { Status = MoveStatus.IncorrectMove };
            }

            var information = _figureToProviders[movedFigure].GetMoveInformation(start, finish);
            information.Finish = finish;
            information.Start = start;
            information.MovedFigure = movedFigure;

            return information;
        }

        private readonly IBoardMetadata _boardMetadata;
        private readonly IDictionary<Figure, IMovesProvider> _figureToProviders;
    }
}
