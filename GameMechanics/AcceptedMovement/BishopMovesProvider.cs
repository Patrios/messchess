﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class BishopMovesProvider : BaseMovesProvider
    {
        public BishopMovesProvider(IBoardMetadata boardMetadata)
            : base(boardMetadata)
        {
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            return AttackMovesByAccepted(position, color);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var bishopMoves = Bitboard.None;

            bishopMoves = bishopMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number).AddColumns(number)));

            bishopMoves = bishopMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number).SubColumns(number)));

            bishopMoves = bishopMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number).AddColumns(number)));

            bishopMoves = bishopMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number).SubColumns(number)));

            return bishopMoves;
        }
    }
}
