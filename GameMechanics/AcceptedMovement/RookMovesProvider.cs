﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class RookMovesProvider : BaseMovesProvider
    {
        public RookMovesProvider(IBoardMetadata boardMetadata)
            : base(boardMetadata)
        {
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            return AttackMovesByAccepted(position, color);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var rookMoves = Bitboard.None;

            rookMoves = rookMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number)));

            rookMoves = rookMoves.Union(
                CollectCellsOnDirection(number => position.AddColumns(number)));

            rookMoves = rookMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number)));

            rookMoves = rookMoves.Union(
                CollectCellsOnDirection(number => position.SubColumns(number)));

            return rookMoves;
        }
    }
}
