﻿using System;
using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public abstract class BaseMovesProvider : IMovesProvider
    {
        protected BaseMovesProvider(IBoardMetadata boardMetadata)
        {
            BoardMetadata = boardMetadata;
        }

        public Bitboard GetAcceptedMoves(Bitboard position, FigureColor color)
        {
            return FilterOwnPositions(GetAllAcceptedMoves(position, color), color);
        }

        public virtual MoveInformation GetMoveInformation(Bitboard start, Bitboard finish)
        {
            var enemyFigure = BoardMetadata.GetFigure(finish);
            if (enemyFigure != Figure.None)
            {
                return new MoveInformation
                    {
                        Status = MoveStatus.EatEnemy, 
                        Finish = finish, 
                        EatedPosition = finish,
                        EatedFigure = enemyFigure
                    };
            }

            return new MoveInformation{ Status = MoveStatus.AcceptedMove };
        }

        public abstract Bitboard GetAttackPositions(Bitboard position, FigureColor color);

        protected Bitboard CollectCellsOnDirection(Func<int, Bitboard> nextPositionFunc)
        {
            var result = Bitboard.None;

            for (var i = 1; i < BoardUtils.BoardRank; i++)
            {
                var nextPosition = nextPositionFunc(i);

                if (nextPosition == Bitboard.None)
                {
                    break;
                }

                var cellFigure = BoardMetadata.GetFigure(nextPosition);
                result = result.Union(nextPosition);

                if (cellFigure != Figure.None)
                {
                    break;
                }
            }

            return result;
        }

        protected Bitboard AttackMovesByAccepted(Bitboard position, FigureColor color)
        {
            return GetAcceptedMoves(position, color);
        }

        protected Bitboard FilterOwnPositions(Bitboard positions, FigureColor color)
        {
            return
                positions
                    .Common(
                        BoardMetadata
                            .GetPositionsWithColor(color)
                            .Negative());
        }

        protected abstract Bitboard  GetAllAcceptedMoves(Bitboard position, FigureColor color);

        protected readonly IBoardMetadata BoardMetadata;
    }
}
