﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class MovesProviderFacadeFactory : IMovesProviderFacadeFactory
    {
        public IMovesProviderFacade Create(IBoardMetadata boardMetadata)
        {
            return new MovesProviderFacade(boardMetadata);
        }
    }
}
