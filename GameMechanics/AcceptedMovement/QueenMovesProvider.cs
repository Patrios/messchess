﻿using GameMechanics.Board;

namespace GameMechanics.AcceptedMovement
{
    public sealed class QueenMovesProvider : BaseMovesProvider
    {
        public QueenMovesProvider(IBoardMetadata boardMetadata) : base(boardMetadata)
        {
        }

        public override Bitboard GetAttackPositions(Bitboard position, FigureColor color)
        {
            return AttackMovesByAccepted(position, color);
        }

        protected override Bitboard GetAllAcceptedMoves(Bitboard position, FigureColor color)
        {
            var queenMoves = Bitboard.None;

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number).AddColumns(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number).SubColumns(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number).AddColumns(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number).SubColumns(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.AddRows(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.AddColumns(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.SubRows(number)));

            queenMoves = queenMoves.Union(
                CollectCellsOnDirection(number => position.SubColumns(number)));

            return queenMoves;
        }
    }
}
