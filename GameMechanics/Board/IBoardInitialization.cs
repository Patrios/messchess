﻿namespace GameMechanics.Board
{
    public interface IBoardInitialization
    {
        void Initialize(IBoard board);
    }
}
