﻿using GameMechanics.AcceptedMovement;

namespace GameMechanics.Board
{
    public sealed class ChessBoardFactory : IBoardFactory
    {
        
        public ChessBoardFactory(IBoardInitialization initializator)
        {
            _initializator = initializator;
        }
        public IBoard Create()
        {
            var board = new ChessBoard(new MovesProviderFacadeFactory());
            _initializator.Initialize(board);
            return board;
        }

        private readonly IBoardInitialization _initializator;
    }
}
