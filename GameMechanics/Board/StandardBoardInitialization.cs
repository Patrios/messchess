﻿namespace GameMechanics.Board
{
    public sealed class StandardBoardInitialization : IBoardInitialization
    {
        public void Initialize(IBoard board)
        {
            for (var i = 0; i < BoardUtils.BoardRank; i++)
            {
                board.SetFigure(Figure.WhitePawn, new FlatPosition(1, i).ToBitboard());
            }

            board.SetFigure(Figure.WhiteRook, Bitboard.A1);
            board.SetFigure(Figure.WhiteRook, Bitboard.H1);

            board.SetFigure(Figure.WhiteKnight, Bitboard.B1);
            board.SetFigure(Figure.WhiteKnight, Bitboard.G1);

            board.SetFigure(Figure.WhiteBishop, Bitboard.C1);
            board.SetFigure(Figure.WhiteBishop, Bitboard.F1);

            board.SetFigure(Figure.WhiteQueen, Bitboard.D1);
            board.SetFigure(Figure.WhiteKing, Bitboard.E1);

            for (var i = 0; i < BoardUtils.BoardRank; i++)
            {
                board.SetFigure(Figure.BlackPawn, new FlatPosition(BoardUtils.BoardRank - 2, i).ToBitboard());
            }

            board.SetFigure(Figure.BlackRook, Bitboard.A8);
            board.SetFigure(Figure.BlackRook, Bitboard.H8);

            board.SetFigure(Figure.BlackKnight, Bitboard.B8);
            board.SetFigure(Figure.BlackKnight, Bitboard.G8);

            board.SetFigure(Figure.BlackBishop, Bitboard.C8);
            board.SetFigure(Figure.BlackBishop, Bitboard.F8);

            board.SetFigure(Figure.BlackQueen, Bitboard.D8);
            board.SetFigure(Figure.BlackKing, Bitboard.E8);
        }
    }
}
