﻿namespace GameMechanics.Board
{
    public interface IBoard
    {
        void SetFigure(Figure figure, Bitboard positions);

        MoveInformation MakeMove(Bitboard start, Bitboard finish, FigureColor color);

        MoveInformation UpgradePawn(FigureColor color, Figure newFigure);
    }
}
