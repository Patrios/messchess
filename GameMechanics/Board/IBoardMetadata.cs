﻿namespace GameMechanics.Board
{
    public interface IBoardMetadata
    {
        Bitboard GetPositionsWithColor(FigureColor color);

        Figure GetFigure(Bitboard position);

        bool IsFirstMove(Bitboard position);

        bool IsUnderAttack(Bitboard positions, FigureColor color);

        bool IsDoublePawnMove(Bitboard position, FigureColor pawnColor); 
    }
}
