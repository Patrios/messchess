﻿namespace GameMechanics.Board
{
    public interface IBoardFactory
    {
        IBoard Create();
    }
}
