﻿using System.Collections.Generic;
using System.Linq;
using Common;
using GameMechanics.AcceptedMovement;

namespace GameMechanics.Board
{
    public sealed class ChessBoard : IBoard, IBoardMetadata
    {
        public ChessBoard(IMovesProviderFacadeFactory movesProviderFacadeFactory)
        {
            _whitePawnsIsDoubleMove = new bool[BoardUtils.BoardRank];
            _blackPawnsIsDoubleMove = new bool[BoardUtils.BoardRank];

            _isFirstMove = new bool[BoardUtils.BoardRank, BoardUtils.BoardRank];
            InitializeFirstMoveArray();

            _board = new Dictionary<Figure, Bitboard>();

            foreach (var figure in EnumUtils.GetEnumIntems<Figure>())
            {
                _board.Add(figure, Bitboard.None);
            }

            _movesProviderFacade = movesProviderFacadeFactory.Create(this);
        }

        #region IBoardMetadata

        public Figure GetFigure(Bitboard position)
        {
            var figureOnPosition =
                _board.GetKeysByValue(
                    figurePositions => figurePositions.HasFlag(position)).ToArray();

            return figureOnPosition.Any() ? figureOnPosition.Single() : Figure.None;
        }

        public bool IsFirstMove(Bitboard position)
        {
            return _isFirstMove[position.Row(), position.Column()];
        }

        public bool IsUnderAttack(Bitboard positions, FigureColor color)
        {
            return positions.Common(_movesProviderFacade.GetColoredAttacks(color)) != Bitboard.None;
        }

        public bool IsDoublePawnMove(Bitboard position, FigureColor pawnColor)
        {
            Guard.CheckTrue(pawnColor != FigureColor.None, "Pawn can not have none color.");
            return GetIsDoubleMove(pawnColor)[position.Column()];
        }

        public Bitboard GetPositionsWithColor(FigureColor color)
        {
            return
                _board
                    .Where(figure => figure.Key.GetColor() == color)
                    .Select(figure => figure.Value)
                    .Aggregate((position1, position2) => position1.Union(position2));
        }

        #endregion

        #region IBoard 

        public void SetFigure(Figure figure, Bitboard positions)
        {
            _board[figure] = _board[figure].Union(positions);
        }

        public MoveInformation MakeMove(Bitboard start, Bitboard finish, FigureColor color)
        {
            if (NeedToUpgradePawn(color))
            {
                return new MoveInformation { Status = MoveStatus.NeedPawnUpgrade };
            }

            var information = MoveSelfFigure(start, finish, color);

            if (information.IsIncorrectMove() || information.PawnUpgrade)
            {
                return information;
            }

            UpdateBoardStatus(information);
            information.EnemyStatus = GetEnemyStatus(color.EnemyColor());
            return information;
        }

        public MoveInformation UpgradePawn(FigureColor color, Figure newFigure)
        {
            var pawnPosition = FindPawnForUpgrade(color);

            if (pawnPosition == Bitboard.None || !newFigure.CanBeUpgradedFromPawn())
            {
                return new MoveInformation { Status = MoveStatus.IncorrectMove };
            }

            var pawn = color == FigureColor.Black ? Figure.BlackPawn : Figure.WhitePawn;
            EatFigure(pawn, pawnPosition);
            SetFigure(newFigure, pawnPosition);

            return new MoveInformation
            {
                Status = MoveStatus.AcceptedMove,
                EnemyStatus = GetEnemyStatus(color.EnemyColor()),
                PawnNewSharp = newFigure
            };
        }

        private Bitboard FindPawnForUpgrade(FigureColor color)
        {
            return (from position in GetPositionsWithColor(color).SplitPositions()
                let figure = GetFigure(position)
                where figure.GetFigureName() == Figure.WhitePawn.GetFigureName() &&
                      PawnMovesProvider.IsUpgradePosition(position.ToFlat())
                select position).SingleOrDefault();
        }

        private bool NeedToUpgradePawn(FigureColor color)
        {
            return FindPawnForUpgrade(color) != Bitboard.None;
        }

        private MoveInformation MoveSelfFigure(Bitboard start, Bitboard finish, FigureColor color)
        {
            var information = _movesProviderFacade.GetMoveInformation(start, finish, color);

            if (information.IsIncorrectMove())
            {
                return information;
            }

            if (information.Status == MoveStatus.Castling)
            {
                MakeCastling(information);
            }
            else if (!TryMakeMove(information))
            {
                return new MoveInformation { Status = MoveStatus.SelfCheck };
            }

            return information;
        }

        private void UndoMove(MoveInformation information)
        {
            if (information.IsIncorrectMove())
            {
                return;
            }

            MoveFigure(information.MovedFigure, information.Finish, information.Start);

            if (information.Status == MoveStatus.EatEnemy)
            {
                SetFigure(information.EatedFigure, information.EatedPosition);
            }

            if (information.Status == MoveStatus.Castling)
            {
                MoveFigure(
                    GetRook(information.MovedFigure.GetColor()), 
                    information.CastlingFinishRook, 
                    information.CastlingStartRook);
            }
        }

        private EnemyStatus GetEnemyStatus(FigureColor enemyColor)
        {
            if (IsCheck(enemyColor))
            {
                return IsStalemate(enemyColor) ? EnemyStatus.Mate : EnemyStatus.Check;
            }

            return IsStalemate(enemyColor) ? EnemyStatus.Stalemate : EnemyStatus.None;
        }

        private bool IsCheck(FigureColor color)
        {
            return
                _movesProviderFacade.GetColoredAttacks(color.EnemyColor())
                    .Common(_board[GetKing(color)]) != Bitboard.None;
        }

        private bool IsStalemate(FigureColor color)
        {
            var moves = _movesProviderFacade.GetColoredMoves(color);
            return moves.All(move =>
                {
                    var moveResult = MoveSelfFigure(move.Start, move.Finish, color);
                    UndoMove(moveResult);
                    return moveResult.IsIncorrectMove();
                });
        }

        private void UpdateBoardStatus(MoveInformation information)
        {
            if (information.MovedFigure.GetFigureName() == Figure.WhiteKing.GetFigureName())
            {
                UpdateByKingMove(information.MovedFigure.GetColor());
            }

            if (information.MovedFigure.GetFigureName() == Figure.BlackPawn.GetFigureName())
            {
                SetPawnDoubleMove(information.Start.Column(), information.MovedFigure.GetColor(), value: false);
            }

            if (information.IsPawnDoubleMove)
            {
                SetPawnDoubleMove(information.Start.Column(), information.MovedFigure.GetColor(), value: true);
            }

            var flatStart = information.Start.ToFlat();
            var flatFinish = information.Finish.ToFlat();

            _isFirstMove[flatStart.Row, flatStart.Column] = false;
            _isFirstMove[flatFinish.Row, flatFinish.Column] = false;
        }

        private void UpdateByKingMove(FigureColor color)
        {
            for (var i = 0; i < BoardUtils.BoardRank; i++)
            {
                if (color == FigureColor.White)
                {
                    _isFirstMove[0, i] = false;
                }

                if (color == FigureColor.Black)
                {
                    _isFirstMove[BoardUtils.BoardRank - 1, i] = false;
                }
            }
        }

        private void SetPawnDoubleMove(int column, FigureColor color, bool value)
        {
            if (color == FigureColor.Black)
            {
                _blackPawnsIsDoubleMove[column] = value;
            }
            else
            {
                _whitePawnsIsDoubleMove[column] = value;
            }
        }

        private void MakeCastling(MoveInformation information)
        {
            var figure = GetFigure(information.Start);
            MoveFigure(figure, information.Start, information.Finish);

            MoveFigure(GetRook(information.MovedFigure.GetColor()), information.CastlingStartRook, information.CastlingFinishRook);
        }

        private bool TryMakeMove(MoveInformation information)
        {
            if (information.Status == MoveStatus.EatEnemy)
            {
                EatFigure(information.EatedFigure, information.EatedPosition);
            }

            MoveFigure(information.MovedFigure, information.Start, information.Finish);

            if (!IsCheck(information.MovedFigure.GetColor()))
            {
                return true;
            }

            UndoMove(information);
            return false;
        }

        private void MoveFigure(Figure figure, Bitboard start, Bitboard finish)
        {
            _board[figure] = _board[figure].Common(start.Negative()).Union(finish);
        }

        private void EatFigure(Figure figure, Bitboard start)
        {
            MoveFigure(figure, start, Bitboard.None);
        }

        private static Figure GetKing(FigureColor color)
        {
            return color == FigureColor.White ? Figure.WhiteKing : Figure.BlackKing;
        }

        private static Figure GetRook(FigureColor color)
        {
            return color == FigureColor.White ? Figure.WhiteRook : Figure.BlackRook;
        }

        private void InitializeFirstMoveArray()
        {
            for (var row = 0; row < 2; row++)
            {
                for (var column = 0; column < BoardUtils.BoardRank; column++)
                {
                    _isFirstMove[row, column] = _isFirstMove[BoardUtils.BoardRank - row - 1, column] = true;
                }
            }
        }

        #endregion

        private bool[] GetIsDoubleMove(FigureColor color)
        {
            return color == FigureColor.Black ? _blackPawnsIsDoubleMove : _whitePawnsIsDoubleMove;
        }

        private readonly IMovesProviderFacade _movesProviderFacade;
        private readonly bool[,] _isFirstMove;
        private readonly bool[] _whitePawnsIsDoubleMove;
        private readonly bool[] _blackPawnsIsDoubleMove;
        private readonly IDictionary<Figure, Bitboard> _board;
    }
}
