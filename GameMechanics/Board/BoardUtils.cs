﻿namespace GameMechanics.Board
{
    public static class BoardUtils
    {
        static BoardUtils()
        {
            MaskColumn = new Bitboard[BoardRank];
            MaskRow = new Bitboard[BoardRank];
            ClearRow = new Bitboard[BoardRank];
            ClearColumn = new Bitboard[BoardRank];

            for (var i = 0; i < BoardRank; i++)
            {
                InitializeMaskColumn(i);
                InitializeMaskRow(i);
            }

            for (var i = 0; i < BoardRank; i++)
            {
                InitializeClearColumn(i);
                InitializeClearRow(i);
            }
        }

        public static Bitboard[] MaskColumn { get; private set; }

        public static Bitboard[] MaskRow { get; private set; }

        public static Bitboard[] ClearColumn { get; private set; }

        public static Bitboard[] ClearRow { get; private set; }

        public static int BoardRank
        {
            get { return 8; }
        }

        public static int CellsCount
        {
            get { return BoardRank*BoardRank; }
        }

        private static void InitializeClearRow(int row)
        {
            for (var i = 0; i < BoardRank; i++)
            {
                if (i != row)
                {
                    ClearRow[row] = ClearRow[row].Union(MaskRow[i]);
                }
            }
        }

        private static void InitializeClearColumn(int column)
        {
            for (var i = 0; i < BoardRank; i++)
            {
                if (i != column)
                {
                    ClearColumn[column] = ClearColumn[column].Union(MaskColumn[i]);
                }
            }
        }

        private static void InitializeMaskColumn(int column)
        {
            for (var row = 0; row < BoardRank; row++)
            {
                MaskColumn[row] =
                    MaskColumn[row]
                        .Union(
                            new FlatPosition(row, column).ToBitboard());
            }
        }

        private static void InitializeMaskRow(int row)
        {
            for (var column = 0; column < BoardRank; column++)
            {
                MaskRow[column] =
                    MaskRow[column]
                        .Union(
                            new FlatPosition(row, column).ToBitboard());
            }
        }
    }
}
