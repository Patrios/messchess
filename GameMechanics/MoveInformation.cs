﻿using System;
using System.Linq;

namespace GameMechanics
{
    [Serializable]
    public class MoveInformation
    {
        public Figure PawnNewSharp { get; set; }

        public MoveStatus Status { get; set; }

        public EnemyStatus EnemyStatus { get; set; }

        public Bitboard Finish { get; set; }

        public Bitboard Start { get; set; }

        public Figure MovedFigure { get; set; }

        public Bitboard CastlingFinishRook { get; set; }

        public Bitboard CastlingStartRook { get; set; }

        public Bitboard EatedPosition { get; set; }

        public Figure EatedFigure { get; set; }

        public bool IsPawnDoubleMove { get; set; }

        public bool PawnUpgrade { get; set; }

        public bool IsIncorrectMove()
        {
            return IncorrectStatus.Contains(Status);
        }

        public bool IsGameEnd()
        {
            return GameEndStatuses.Contains(EnemyStatus);
        }

        private static readonly EnemyStatus[] GameEndStatuses =
        {
            EnemyStatus.Mate, EnemyStatus.Stalemate
        };

        private static readonly MoveStatus[] IncorrectStatus =
        {
            MoveStatus.IncorrectMove, MoveStatus.NoFigure, MoveStatus.SelfCheck, MoveStatus.NeedPawnUpgrade 
        };
    }
}
