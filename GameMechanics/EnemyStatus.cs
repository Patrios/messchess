﻿namespace GameMechanics
{
    public enum EnemyStatus
    {
        None,
        Check,
        Mate,
        Stalemate
    }
}
