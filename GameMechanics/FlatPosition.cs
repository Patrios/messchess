﻿using System;
using System.Linq;
using Common;
using GameMechanics.Board;

namespace GameMechanics
{
    [Serializable]
    public sealed class FlatPosition : IEquatable<FlatPosition>
    {
        public FlatPosition(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public bool Equals(FlatPosition other)
        {
            if (other == null)
            {
                return false;
            }
            
            return other.Row == Row && other.Column == Column;
        }

        public static FlatPosition Create(string name)
        {
            if (name.Count() != 2)
            {
                throw new ArgumentException(string.Format("Wrong cell description {0}.", name));
            }

            var cell = name.ToLower();
            var column = cell[0] - 'a';
            var row = cell[1] - '1';

            return new FlatPosition(row, column);
        }

        public Bitboard ToBitboard()
        {
            Guard.CheckRange(Row, 0, BoardUtils.BoardRank);
            Guard.CheckRange(Column, 0, BoardUtils.BoardRank);

            var cellNumber = Row * BoardUtils.BoardRank + Column;
            return (Bitboard)((ulong)1 << cellNumber);
        }

        public int Row { get; private set; }

        public int Column { get; private set; }
    }
}
