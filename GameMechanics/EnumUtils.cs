﻿
using System;
using System.Collections.Generic;

namespace GameMechanics
{
    public static class EnumUtils
    {
        public static IEnumerable<TEnum> GetEnumIntems<TEnum>()
        {
            CheckEnumType<TEnum>();
            return (TEnum[])Enum.GetValues(typeof (TEnum));
        }

        private static void CheckEnumType<T>()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException(
                    string.Format("Type {0} is not enum.", typeof(T).Name));
            }
        }
    }
}
