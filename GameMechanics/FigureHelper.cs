﻿using System.Linq;

namespace GameMechanics
{
    public static class FigureHelper
    {
        public static FigureColor EnemyColor(this FigureColor color)
        {
            if (color == FigureColor.White)
            {
                return FigureColor.Black;
            }

            if (color == FigureColor.Black)
            {
                return FigureColor.White;
            }

            return FigureColor.None;
        }

        public static bool CanBeUpgradedFromPawn(this Figure figure)
        {
            return FiguresUpgradableFromPawn.Contains(figure);
        }

        public static FigureColor GetColor(this Figure figure)
        {
            if (figure.ToString().StartsWith("White"))
            {
                return FigureColor.White;
            }

            if (figure.ToString().StartsWith("Black"))
            {
                return FigureColor.Black;
            }

            return FigureColor.None;
        }

        public static string GetFigureName(this Figure figure)
        {
            if (figure.ToString().StartsWith("White"))
            {
                return figure.ToString().Replace("White", "");
            }

            if (figure.ToString().StartsWith("Black"))
            {
                return figure.ToString().Replace("Black", "");
            }

            return string.Empty;
        }

        private static readonly Figure[] FiguresUpgradableFromPawn =
        {
            Figure.BlackBishop,
            Figure.BlackKnight,
            Figure.BlackRook,
            Figure.BlackQueen,
            Figure.WhiteBishop,
            Figure.WhiteKnight,
            Figure.WhiteRook,
            Figure.WhiteQueen
        };
    }
}
