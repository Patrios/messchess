﻿namespace Common.Logging
{
    public interface ILogger
    {
        void WriteMessage(string message, LogLevel level);

        void WriteObject(object loggedObject, LogLevel level);
    }
}
