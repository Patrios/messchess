﻿using System;
using System.IO;
using Common.TimeProvider;

namespace Common.Logging
{
    public sealed class FileLogger : ILogger, IDisposable
    {
        public FileLogger(string pathToLog, ITimeProvider timeProvider)
        {
            _timeProvider = timeProvider;
            _logStream = new StreamWriter(new FileStream(
                pathToLog, 
                FileMode.OpenOrCreate,
                FileAccess.Write,
                FileShare.Read));
        }

        public void WriteMessage(string message, LogLevel level)
        {
            _logStream.WriteLine(
                "{0} {1}: {2}", 
                _timeProvider.CurrentUtcTime(), 
                level, 
                message);
            _logStream.Flush();
        }

        public void WriteObject(object loggedObject, LogLevel level)
        {
            WriteMessage(loggedObject.ToString(), level);
        }

        public void Dispose()
        {
            _logStream.Dispose();
        }

        private readonly ITimeProvider _timeProvider;
        private readonly StreamWriter _logStream;
    }
}
