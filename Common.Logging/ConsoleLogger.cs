﻿using System;

namespace Common.Logging
{
    public sealed class ConsoleLogger : ILogger
    {
        public void WriteMessage(string message, LogLevel level)
        {
            Console.ForegroundColor = GetLevelColor(level);
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public void WriteObject(object loggedObject, LogLevel level)
        {
            WriteMessage(loggedObject.ToString(), level);
        }

        private static ConsoleColor GetLevelColor(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Warning:
                    return ConsoleColor.Yellow;
                case LogLevel.Message:
                    return ConsoleColor.White;
                case LogLevel.Error:
                    return ConsoleColor.Red;
                default:
                    throw new ArgumentOutOfRangeException("level");
            }
        }
    }
}
