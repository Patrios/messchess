﻿namespace Common.Logging
{
    public class EmptyLogger : ILogger
    {
        public void WriteMessage(string message, LogLevel level)
        {
        }

        public void WriteObject(object loggedObject, LogLevel level)
        {
        }
    }
}
