﻿namespace Common.Logging
{
    public enum LogLevel
    {
        Warning,
        Message,
        Error
    }
}
