﻿using Common.CryptoUtils;
using Common.Database;
using DataAccessLayer.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AuthenticationTest
{
    [TestClass]
    public class UserRepositoryTests
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            DatabaseUtils.DropDatabaseIfExists(DatabaseName, SqlServer);
            DatabaseUtils.CreateDatabaseIfNotExists(DatabaseName, SqlServer);

            _connectionFactory = new SqlConnectionFactory(
                sqlServerName: SqlServer,
                databaseName: DatabaseName);

            _userRepository = new UserDatabaseRepository(_connectionFactory, new PBKDF2HashProvider());
            _userRepository.CreateIfMissing();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.ExecuteNonQuery(string.Format("delete from {0}", _userRepository.EntryName));
            }
        }

        [TestMethod]
        public void UserTableShouldExistsTest()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.ExecuteReader("SELECT * FROM " + _userRepository.EntryName);
            }
        }

        [TestMethod]
        public void UnExistedUserShouldNotBeFound()
        {
            const string usexistedUserName = "User";
            const string usexistedUserPassword = "Password";
            var userEntity = _userRepository.GetUser(usexistedUserName, usexistedUserPassword);
        
            Assert.AreEqual(true, userEntity.IsEmptyUser());
        }

        [TestMethod]
        public void UserShouldBeSuccessfullCreated()
        {
            const string testUser = "User";
            const string testPassword = "Password";
            const string testDisplayName = "DisplayName";

            var user = new UserEntry(userName: testUser, password: testPassword, displayName: testDisplayName);
            var userEntry = _userRepository.RegisterUser(user);

            Assert.AreEqual(false, userEntry.IsEmptyUser());
            Assert.AreEqual(testDisplayName, userEntry.DisplayName);
        }

        [TestMethod]
        public void SameUserShouldNotBeCreated()
        {
            const string testUser = "SameUser";
            const string testPassword = "SamePassword";

            var user = new UserEntry(userName: testUser, password: testPassword);
            var userEntry = _userRepository.RegisterUser(user);

            Assert.AreEqual(false, userEntry.IsEmptyUser());

            userEntry = _userRepository.RegisterUser(user);
            Assert.AreEqual(true, userEntry.IsEmptyUser());
        }

        [TestMethod]
        public void UsersWithSameRegistrationPasswordShouldHaveDiffrentSalted()
        {
            const string testUser = "firstUser";
            const string testPassword = "password";
            const string anotherUser = "secondUser";
            
            _userRepository.RegisterUser(new UserEntry(userName: testUser, password: testPassword));
            _userRepository.RegisterUser(new UserEntry(userName: anotherUser, password: testPassword));

            Assert.AreNotEqual(
                _userRepository.GetUserByName(testUser).Password, 
                _userRepository.GetUserByName(anotherUser).Password);
        }

        [TestMethod]
        public void RegisterUserShouldBeFound()
        {
            const string testUser = "firstUser";
            const string testPassword = "password";
            _userRepository.RegisterUser(new UserEntry(userName: testUser, password: testPassword));
            var result = _userRepository.GetUser(testUser, testPassword);

            Assert.AreEqual(false, result.IsEmptyUser());
            Assert.AreEqual(testUser, result.UserName);
        }

        const string SqlServer = @".\SQLSERVER";
        const string DatabaseName = @"TestUserDb";
        private static IConnectionFactory _connectionFactory;
        private static UserDatabaseRepository _userRepository;
    }
}
