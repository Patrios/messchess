﻿using System;
using Common.CryptoUtils;
using Common.Database;
using Common.TimeProvider;
using DataAccessLayer.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceCore.Authentication;
using ServiceCore.Authentication.Session;
using ServiceCore.Game;
using TestUtils;

namespace AuthenticationTest
{
    [TestClass]
    public class AuthorizationServiceTests
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            DatabaseUtils.DropDatabaseIfExists(DatabaseName, SqlServer);
            DatabaseUtils.CreateDatabaseIfNotExists(DatabaseName, SqlServer);

            _connectionFactory = new SqlConnectionFactory(
                sqlServerName: SqlServer,
                databaseName: DatabaseName);

            _userRepository = new UserDatabaseRepository(
                _connectionFactory, 
                new PBKDF2HashProvider());

            _userRepository.CreateIfMissing();

            _userRepository.RegisterUser(TestedUser);

            _timeProvider = new ManualTimeProvider();
            _timeProvider.StopTime();

            InitializeSessionManager();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            InitializeSessionManager();
        }


        [TestMethod]
        public void UnCorrectUserNameShouldReturnWrongNameOrPasswordResult()
        {
            var loginResult = _testedService.Login("badName", "badPassword");
            Assert.AreEqual(LoginResult.WrongNameOrPassword, loginResult.Result);
        }

        [TestMethod]
        public void UnCorrectPasswordShouldReturnWrongNameOrPasswordResult()
        {
            var loginResult = _testedService.Login(TestedUser.UserName, "badPassword");
            Assert.AreEqual(LoginResult.WrongNameOrPassword, loginResult.Result);
        }

        [TestMethod]
        public void TestedUserShouldBeSuccessedLoginned()
        {
            var loginResult = _testedService.Login(TestedUser.UserName, UserPassword);
            Assert.AreEqual(LoginResult.Success, loginResult.Result);
            Assert.AreEqual(TestedUser.DisplayName, loginResult.UserInformation.DisplayName);
            Assert.AreEqual(TestedUser.Rating, loginResult.UserInformation.Rating);
        }

        [TestMethod]
        public void NotExistsSessionShouldNotBeUpdated()
        {
            var session = _testedService.ExtendSession(Guid.Empty, TestedUser.UserName);
            Assert.AreEqual(true, session.IsEmpty());
        }

        [TestMethod]
        public void WrongUserNameShouldNotBeExtended()
        {
            var loginResult = _testedService.Login(TestedUser.UserName, UserPassword);
            Assert.AreEqual(LoginResult.Success, loginResult.Result);

            var session = _testedService.ExtendSession(loginResult.Session.SessionId, "wrongName");
            Assert.AreEqual(true, session.IsEmpty());
        }

        [TestMethod]
        public void CorrectSessionShouldBeExtended()
        {
            var loginResult = _testedService.Login(TestedUser.UserName, UserPassword);
            Assert.AreEqual(LoginResult.Success, loginResult.Result);
            Assert.AreEqual(_timeProvider.CurrentUtcTime(), loginResult.Session.SessionLastUpdate);

            _timeProvider.ShiftTime(TimeSpan.FromMinutes(1));

            var session = _testedService.ExtendSession(
                loginResult.Session.SessionId, 
                TestedUser.UserName);
            Assert.AreEqual(false, session.IsEmpty());
            Assert.AreEqual(loginResult.Session.SessionId, session.SessionId);
            Assert.AreEqual(_timeProvider.CurrentUtcTime(), loginResult.Session.SessionLastUpdate);
        }

        [TestMethod]
        public void ExpiredSessionShouldBeRemovedAfterFlush()
        {
            var session = _sessionManager.CreateSession(TestedUser);
            Assert.AreEqual(false, session.IsEmpty());
            _timeProvider.ShiftTime(TimeSpan.FromMinutes(2));

            _sessionManager.Flush();

            Assert.AreEqual(true, _sessionManager.GetSession(session.SessionId).IsEmpty());
        }

        [TestMethod]
        public void InGameSessionShouldNotBeRemovedAfterFlush()
        {
            var session = _sessionManager.CreateSession(TestedUser);
            session.State = SessionState.InGame;
            Assert.AreEqual(false, session.IsEmpty());
            _timeProvider.ShiftTime(TimeSpan.FromMinutes(2));

            _sessionManager.Flush();

            Assert.AreEqual(false, _sessionManager.GetSession(session.SessionId).IsEmpty());
        }

        private static void InitializeSessionManager()
        {
            _sessionManager = new SessionManager(
                _timeProvider,
                new FallbackGameCallbackFactory(), 
                TimeSpan.FromMinutes(1));

            _testedService = new AuthorizationService(
                _userRepository,
                _sessionManager,
                new EmptyLogger());
        }

        const string SqlServer = @".\SQLSERVER";
        const string DatabaseName = @"TestAuthentificationDb";
        private static IConnectionFactory _connectionFactory;
        private static UserDatabaseRepository _userRepository;
        private static IAuthenticationSevice _testedService;
        private static ManualTimeProvider _timeProvider;
        private static SessionManager _sessionManager;

        private const string UserPassword = "qwerty";
        private static readonly UserEntry TestedUser = 
            new UserEntry(
                userName: "test",
                password: UserPassword, 
                displayName: "display", 
                rating: 1000);
    }
}
