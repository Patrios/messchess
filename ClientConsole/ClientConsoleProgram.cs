﻿using System;
using System.Threading;
using ClientCore;
using Common;
using Common.Logging;
using Common.SettingsProvider;
using GameMechanics;
using GameMechanics.Board;
using Protocol;

namespace ClientConsole
{
    public static class ClientConsoleProgram
    {
        private static void Main()
        {
            var settingsProvider = new ConfigSettingsProvider();
            var logger = new ConsoleLogger();
            var servicesUri = settingsProvider.ServerUriSetting();

            using (_container = 
                new ClientContainer( 
                    logger,
                    servicesUri, 
                    settingsProvider.AuthorizationServicePortSetting(),
                    settingsProvider.GameServicePortSetting()))
            {
                LoginDialog();
                SubscribeToCallbacks();

                while (_state != ClientState.Exit)
                {
                    Menu();
                }
            }
        }

        private static void SubscribeToCallbacks()
        {
            _container.GameCallback.GameFoundEvent += OnEnemyFounded;
            _container.GameCallback.EnemyMakeMoveEvent += OnEnemyMakeMove;
            _container.GameCallback.EnemyUpgradePawnEvent += OnEnemyMakeMove;
            _container.GameCallback.GameTerminatedEvent += OnGameTerminated;
            _container.GameCallback.GameCanceledEvent += OnGameCanceledEvent;
        }

        private static void OnGameCanceledEvent(bool yourFault)
        {
            Console.WriteLine("Ошибка противника, идет поиск другого соперника.");
            _state = yourFault ? ClientState.MainMenu : ClientState.Search;
        }

        private static void Menu()
        {
            switch (_state)
            {
                case ClientState.MainMenu:
                    MainMenu();
                    break;
                case ClientState.Search:
                    Thread.Sleep(1000);
                    break;
                
                case ClientState.Game:
                    GameMenu();
                    break;
                
                case ClientState.NeedPawnUpgrade:
                    PawnUpgradeMenu();
                    break;

                case ClientState.WaitEnemy:
                    Thread.Sleep(1000);
                    break;
                
                case ClientState.Results:
                    DisplayGameResults();
                    break;
                
                case ClientState.Exit:
                    return;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void MainMenu()
        {
            Console.WriteLine("Для поиска быстрой игры введите 1.");
            Console.WriteLine("Для поиска обычной игры введите 2.");
            Console.WriteLine("Для поиска долгой игры введите 3.");
            Console.WriteLine("Для поиска выхода из игры введите 0.");

            var key = int.Parse(Console.ReadLine());

            var searchedMode = GameMode.SimpleGame;
            switch (key)
            {
                case 0:
                    _state = ClientState.Exit;
                    return;
                case 1:
                    searchedMode = GameMode.ShortGame;
                    break;
                case 2:
                    searchedMode = GameMode.SimpleGame;
                    break;
                case 3:
                    searchedMode = GameMode.LongGame;
                    break;
            }

            _container.GameProvider.FindEnemy(searchedMode);

            Console.WriteLine("Идет поиск противника.");
            _state = ClientState.Search;
        }

        private static void GameMenu()
        {
            Console.WriteLine("Для вывода доски введите 1");
            Console.WriteLine("Для совершения хода введите 2\n");

            var key = int.Parse(Console.ReadLine());
            
            if (key == 1)
            {
                DisplayBoard();
            }
            else
            {
                System.Diagnostics.Debugger.Launch();
                Console.WriteLine("Введите ячейку фигуры.");
                var start = Console.ReadLine();

                Console.WriteLine("Введите конечную ячейке.");
                var finish = Console.ReadLine();

                var moveResult = _container.GameProvider.MakeMove(FlatPosition.Create(start),
                    FlatPosition.Create(finish));

                if (moveResult.GameResult != null)
                {
                    _state = ClientState.Results;
                }
                else if (moveResult.Information.IsIncorrectMove())
                {
                    Console.WriteLine("Невозможный ход.");
                }
                else if (moveResult.Information.PawnUpgrade)
                {
                    _state = ClientState.NeedPawnUpgrade;
                }
                else
                {
                    _state = ClientState.WaitEnemy;
                }
            }
        }

        private static void DisplayBoard()
        {
            Console.WriteLine("Текущее состояние доски:");
            for (var row = 0; row < BoardUtils.BoardRank; row++)
            {
                for (var column = 0; column < BoardUtils.BoardRank; column++)
                {
                    var figure = _container.GameProvider.CurrentBoard.GetFigure(
                        new FlatPosition(row, column).ToBitboard());
                    Console.Write("{0} ", GetFigureCode(figure));
                }

                Console.WriteLine();
            }

            Console.WriteLine("\n");
        }

        private static string GetFigureCode(Figure figure)
        {
            switch (figure)
            {
                case Figure.None:
                    return "NN";
                case Figure.WhiteKing:
                    return "WK";
                case Figure.WhiteQueen:
                    return "WQ";
                case Figure.WhiteRook:
                    return "WR";
                case Figure.WhiteBishop:
                    return "WB";
                case Figure.WhiteKnight:
                    return "WH";
                case Figure.WhitePawn:
                    return "WP";
                case Figure.BlackKing:
                    return "BK";
                case Figure.BlackQueen:
                    return "BQ";
                case Figure.BlackRook:
                    return "BR";
                case Figure.BlackBishop:
                    return "BB";
                case Figure.BlackKnight:
                    return "BH";
                case Figure.BlackPawn:
                    return "BP";
                default:
                    throw new ArgumentOutOfRangeException("figure");
            }
        }

        private static void PawnUpgradeMenu()
        {
            Console.WriteLine("Для превращения в коня введите 1");
            Console.WriteLine("Для превращения в офицера введите 2");
            Console.WriteLine("Для превращения в туру введите 3");
            Console.WriteLine("Для превращения в королеву введите 4\n");

            var clientColor = _container.GameProvider.CurrentGame.Color;
            var key = int.Parse(Console.ReadLine());

            var upgradeFigure = Figure.None;
            switch (key)
            {
                case 1:
                    upgradeFigure = 
                            clientColor == FigureColor.White ? 
                            Figure.WhiteKnight : 
                            Figure.BlackKnight;
                    break;
                case 2:
                    upgradeFigure =
                            clientColor == FigureColor.White ?
                            Figure.WhiteBishop :
                            Figure.BlackBishop;
                    break;
                case 3:
                    upgradeFigure =
                            clientColor == FigureColor.White ?
                            Figure.WhiteRook :
                            Figure.BlackRook;
                    break;
                case 4:
                    upgradeFigure =
                            clientColor == FigureColor.White ?
                            Figure.WhiteQueen :
                            Figure.BlackQueen;
                    break;
            }

            _container.GameProvider.UpgradePawn(upgradeFigure);
            _state = ClientState.WaitEnemy;
        }

        private static void DisplayGameResults()
        {
            Console.WriteLine("Игра завершена.");

            var gameResult = _container.GameProvider.LastGameResult;
            Console.WriteLine(ResultMessage(gameResult.Result));
            Console.WriteLine(
                "Ваш новый рейтинг {0}. Новый рейтинг врага {1}.\n", 
                gameResult.NewRating, 
                gameResult.NewEnemyRating);

            _state = ClientState.MainMenu;
        }

        private static string ResultMessage(GameResult result)
        {
            if (result == GameResult.Draw)
            {
                return "Ничья!";
            }

            if ((result == GameResult.WhiteWin && _container.GameProvider.CurrentGame.Color == FigureColor.White) ||
                (result == GameResult.BlackWin && _container.GameProvider.CurrentGame.Color == FigureColor.Black))
            {
                return "Вы выиграли!";
            }

            return "Вы проиграли!";
        }

        private static void OnEnemyFounded(SearchResult result)
        {
            Console.WriteLine("\n\nНайден противник {0} с рейтингов {1}.\nВы играете цветом {2}\n",
                result.EnemyName, 
                result.EnemyRating,
                result.Color == FigureColor.White ? "Белый" : "Черный");

            _state = 
                result.Color == FigureColor.White ? 
                    ClientState.Game : 
                    ClientState.WaitEnemy;
        }

        private static void OnEnemyMakeMove(MoveResult moveResult)
        {
            if (moveResult.GameResult != null)
            {
                _state = ClientState.Results;
            }
            else
            {
                if (!moveResult.Information.PawnUpgrade)
                {
                    _state = ClientState.Game;
                }
            }
        }

        private static void OnGameTerminated(CompleteResult obj)
        {
            _state = ClientState.Results;
        }

        private static void LoginDialog()
        {
            do
            {
                Console.WriteLine("Введите имя пользователя: ");
                var userName = Console.ReadLine();

                Console.WriteLine("Введите пароль пользователя: ");
                var password = Console.ReadLine();

                _container.LoginProvider.Login(userName, password);
            } while (ProcessLoginResult(_container.LoginProvider.CurrentLogin));
        }

        private static bool ProcessLoginResult(LoginInformation login)
        {
            switch (login.Result)
            {
                case LoginResult.Success:
                    Console.WriteLine("Авторизация прошла успешно.");
                    return false;
                case LoginResult.WrongNameOrPassword:
                    Console.WriteLine("Неверное имя пользователя или пароль.");
                    return true;
                case LoginResult.UnknownServerException:
                    Console.WriteLine("Неизвестная ошибка на сервере.");
                    return true;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static ClientContainer _container;
        private static ClientState _state = ClientState.MainMenu;    
    }
}
