﻿using System;
using System.Globalization;
using Common;
using Common.TimeProvider;
using DataAccessLayer.Users;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.GameSettings;
using ServiceCore.Game.Search;
using TestUtils;

namespace ServiceCoreTests
{
    [TestClass]
    public class GameSearcherTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            _timeProvider = new ManualTimeProvider();
            _sessionManager = new SessionManager(
                _timeProvider,
                new TestFallbackGameCallbackFactory(), 
                TimeSpan.FromHours(1));
            
            _gameManager = new GameManager(
                _timeProvider,
                new RatingCalculatorStub(),
                new GameRepositoryStub(),
                new ChessBoardFactory(new StandardBoardInitialization()));

            _searcher = new GameSearcher(
                _sessionManager, 
                _gameManager, 
                new ChessGameSettingsFactory(),
                MaxDelta);
        }

        [TestMethod]
        public void TwoPersonInSearchShouldFindEachOtherTest()
        {
            var player1 = new UserEntry(1, "player1", displayName: "displayplayer1");
            var player2 = new UserEntry(2, "player2", displayName: "displayplayer2");

            var player1Session = _sessionManager.CreateSession(player1);
            var player2Session = _sessionManager.CreateSession(player2);

            player1Session.State = player2Session.State = SessionState.InSearch;

            SearchResult player1Founded = null;
            SearchResult player2Founded = null;

            SubscribeToGameResult(player1Session, game => player1Founded = game);
            SubscribeToGameResult(player2Session, game => player2Founded = game);

            _searcher.Flush();

            Assert.IsNotNull(player1Founded);
            Assert.IsNotNull(player2Founded);

            Assert.AreEqual(player2.DisplayName, player1Founded.EnemyName);
            Assert.AreEqual(player1.DisplayName, player2Founded.EnemyName);

            Assert.AreEqual(player2Founded.GameId, player1Founded.GameId);

            Assert.AreNotEqual(player1Founded.Color, player2Founded.Color);
        }

        [TestMethod]
        public void TwoPersonWithBigRatingDeltaShouldNotFindEachOtherTest()
        {
            var player1 = CreateUser(1, GameMode.SimpleGame, 1);
            var player2 = CreateUser(2, GameMode.SimpleGame, 1 + MaxDelta);

            var player1Session = _sessionManager.CreateSession(player1);
            var player2Session = _sessionManager.CreateSession(player2);

            player1Session.State = player2Session.State = SessionState.InSearch;
            player1Session.SearchedMode = player2Session.SearchedMode = GameMode.SimpleGame;

            SearchResult player1Founded = null;
            SearchResult player2Founded = null;

            SubscribeToGameResult(player1Session, game => player1Founded = game);
            SubscribeToGameResult(player2Session, game => player2Founded = game);

            _searcher.Flush();

            Assert.IsNull(player1Founded);
            Assert.IsNull(player2Founded);
        }

        [TestMethod]
        public void TwoPersonWithDifferentSearchModesShouldNotFoundEachOther()
        {
            var player1 = CreateUser(1, GameMode.SimpleGame, 10);
            var player2 = CreateUser(2, GameMode.ShortGame, 10);

            var player1Session = _sessionManager.CreateSession(player1);
            var player2Session = _sessionManager.CreateSession(player2);

            player1Session.State = player2Session.State = SessionState.InSearch;
            player1Session.SearchedMode = GameMode.SimpleGame;
            player2Session.SearchedMode = GameMode.ShortGame;

            SearchResult player1Founded = null;
            SearchResult player2Founded = null;

            SubscribeToGameResult(player1Session, game => player1Founded = game);
            SubscribeToGameResult(player2Session, game => player2Founded = game);

            _searcher.Flush();

            Assert.IsNull(player1Founded);
            Assert.IsNull(player2Founded);
        }

        [TestMethod]
        public void FourPersonShouldFindEachOtherByRatingDeltaMinimizationTest()
        {
            var player1 = CreateUser(1, GameMode.SimpleGame, 10);
            var player2 = CreateUser(2, GameMode.SimpleGame, 20);
            var player3 = CreateUser(3, GameMode.SimpleGame, 50);
            var player4 = CreateUser(4, GameMode.SimpleGame, 60);

            var player1Session = _sessionManager.CreateSession(player1);
            var player2Session = _sessionManager.CreateSession(player2);
            var player3Session = _sessionManager.CreateSession(player3);
            var player4Session = _sessionManager.CreateSession(player4);

            player1Session.State = player2Session.State = SessionState.InSearch;
            player3Session.State = player4Session.State = SessionState.InSearch;
            player3Session.SearchedMode = player4Session.SearchedMode = GameMode.SimpleGame;
            player1Session.SearchedMode = player2Session.SearchedMode = GameMode.SimpleGame;

            SearchResult player1Founded = null;
            SearchResult player2Founded = null;
            SearchResult player3Founded = null;
            SearchResult player4Founded = null;

            SubscribeToGameResult(player1Session, game => player1Founded = game);
            SubscribeToGameResult(player2Session, game => player2Founded = game);
            SubscribeToGameResult(player3Session, game => player3Founded = game);
            SubscribeToGameResult(player4Session, game => player4Founded = game);

            _searcher.Flush();

            Assert.AreEqual(player2.DisplayName, player1Founded.EnemyName);
            Assert.AreEqual(player1.DisplayName, player2Founded.EnemyName);
            Assert.AreEqual(player3.DisplayName, player4Founded.EnemyName);
            Assert.AreEqual(player4.DisplayName, player3Founded.EnemyName);
        }

        [TestMethod]
        public void PlayerShouldFoundEnemyOnSecondSearchTest()
        {
            var player1 = CreateUser(1, GameMode.SimpleGame, 10);
            var player2 = CreateUser(2, GameMode.SimpleGame, 20);
            var player3 = CreateUser(3, GameMode.SimpleGame, 50);
            var player4 = CreateUser(4, GameMode.SimpleGame, 60);

            var player1Session = _sessionManager.CreateSession(player1);
            var player2Session = _sessionManager.CreateSession(player2);
            var player3Session = _sessionManager.CreateSession(player3);
            var player4Session = _sessionManager.CreateSession(player4);

            player1Session.State = player2Session.State = SessionState.InSearch;
            player3Session.State = SessionState.InSearch;
            player1Session.SearchedMode = player2Session.SearchedMode = GameMode.SimpleGame;
            player3Session.SearchedMode = player4Session.SearchedMode = GameMode.SimpleGame;

            SearchResult player1Founded = null;
            SearchResult player2Founded = null;
            SearchResult player3Founded = null;

            SubscribeToGameResult(player1Session, game => player1Founded = game);
            SubscribeToGameResult(player2Session, game => player2Founded = game);
            SubscribeToGameResult(player3Session, game => player3Founded = game);

            _searcher.Flush();

            Assert.AreEqual(player2.DisplayName, player1Founded.EnemyName);
            Assert.AreEqual(player1.DisplayName, player2Founded.EnemyName);
            Assert.IsNull(player3Founded);

            SearchResult player4Founded = null;
            SubscribeToGameResult(player4Session, game => player4Founded = game);
            player4Session.State = SessionState.InSearch;

            _searcher.Flush();

            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual(player4.DisplayName, player3Founded.EnemyName);
            Assert.AreEqual(player3.DisplayName, player4Founded.EnemyName);
        }

        private static UserEntry CreateUser(int number, GameMode mode, int rating)
        {
            var user = new UserEntry(
                number, 
                number.ToString(CultureInfo.InvariantCulture), 
                "Display" + number);
            user.Ratings.SetRating(mode, rating);

            return user;
        }

        private static void SubscribeToGameResult(Session player, Action<SearchResult> onGameWasFound)
        {
            // ReSharper disable once PossibleNullReferenceException
            player.ClientChannel.SendMessage(
                gameCallback => gameCallback.GameFoundEvent += onGameWasFound,
                Actions.DoNothing);
        }

        private const int MaxDelta = 200;
        private GameManager _gameManager;
        private ManualTimeProvider _timeProvider;
        private SessionManager _sessionManager;
        private GameSearcher _searcher;
    }
}
