﻿using Common.Crypto;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServiceCoreTests
{
    [TestClass]
    public class CryptoTests
    {
        [TestMethod]
        public void SaltHashShouldBeSameForUniqueSaltPasswordCombination()
        {
            var saltedHashProvider = new PBKDF2HashProvider();
            var salt = saltedHashProvider.GenerateSalt();
            var password = "qwerty";

            var hashResult = saltedHashProvider.Hash(password, salt);
            Assert.AreEqual(saltedHashProvider.Hash(password, salt), hashResult);
        }

        [TestMethod]
        public void GeneratedSaltShouldBeUnique()
        {
            var saltedHashProvider = new PBKDF2HashProvider();

            Assert.AreNotEqual(saltedHashProvider.GenerateSalt(), saltedHashProvider.GenerateSalt());
        }
    }
}
