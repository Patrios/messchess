﻿using System;
using Common;
using Common.TimeProvider;
using DataAccessLayer.GameEntry;
using DataAccessLayer.Users;
using GameMechanics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.GameSettings;
using ServiceCore.Game.MovementManager;
using ServiceCore.Game.RatingCalculator;
using TestUtils;

namespace ServiceCoreTests
{
    [TestClass]
    public class MovementManagerTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            _gameRepository = new GameRepositoryStub();

            _timeProvider = new ManualTimeProvider();
            _timeProvider.StopTime();

            _board = new BoardStub();
            _calculator = new EloRatingCalculator();
            _gameManager = new GameManager(
                _timeProvider, 
                _calculator, 
                _gameRepository, 
                new BoardFactoryStub(_board));
            _sessionManager = new SessionManager(
                _timeProvider,
                new TestFallbackGameCallbackFactory(), 
                TimeSpan.FromHours(1));

            _movementManager = new MovementManager(_gameManager);
        }

        [TestMethod]
        public void NullGameShouldReturnNullResultTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            var result = _movementManager.MakeMove(_whitePlayerSession, Guid.NewGuid(), Bitboard.A1, Bitboard.A2);

            Assert.IsNull(result);
            Assert.IsNull(_whitePlayerReceivedMoveResult);
            Assert.IsNull(_blackPlayerReceivedMoveResult);
        }

        [TestMethod]
        public void BlackPlayerShouldNotGetUnAcceptedWhitePlayerMoveTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.IncorrectMove
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.MakeMove(_whitePlayerSession, game.Id, Bitboard.A1, Bitboard.A2);

            Assert.AreEqual(MoveStatus.IncorrectMove, result.Information.Status);
            Assert.IsNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);
        }

        [TestMethod]
        public void BlackUserShouldReceivedWhitePlayerMoveTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.AcceptedMove,
                Finish = Bitboard.A2
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.MakeMove(_whitePlayerSession, game.Id, Bitboard.A1, Bitboard.A2);

            Assert.AreEqual(MoveStatus.AcceptedMove, result.Information.Status);
            Assert.IsNotNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);

            Assert.IsNull(_blackPlayerReceivedMoveResult.GameResult);
            Assert.AreEqual(Bitboard.A2, _blackPlayerReceivedMoveResult.Information.Finish);
        }

        [TestMethod]
        public void BlackPlayerShouldReceivedWhitePlayerUpgradeMoveTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.AcceptedMove
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.UpgradePawn(_whitePlayerSession, game.Id, Figure.WhiteBishop);

            Assert.AreEqual(MoveStatus.AcceptedMove, result.Information.Status);
            Assert.IsNotNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);
        }

        [TestMethod]
        public void BlackPlayerShouldNotReceivedWhitePlayerIncorrectUpgradeTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.IncorrectMove
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.UpgradePawn(_whitePlayerSession, game.Id, Figure.WhiteBishop);

            Assert.AreEqual(MoveStatus.IncorrectMove, result.Information.Status);
            Assert.IsNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);
        }

        [TestMethod]
        public void WhitePlayerShouldReceivedBlackPlayerMoveTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.AcceptedMove
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            game.Watches.SwitchPlayers();

            var result = _movementManager.MakeMove(_blackPlayerSession, game.Id, Bitboard.A1, Bitboard.A2);

            Assert.AreEqual(MoveStatus.AcceptedMove, result.Information.Status);
            Assert.IsNull(_blackPlayerReceivedMoveResult);
            Assert.IsNotNull(_whitePlayerReceivedMoveResult);
        }

        [TestMethod]
        public void BlackPlayerShouldReceivedGameResultTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.AcceptedMove,
                EnemyStatus = EnemyStatus.Mate
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.MakeMove(_whitePlayerSession, game.Id, Bitboard.A1, Bitboard.A2);

            Assert.AreEqual(MoveStatus.AcceptedMove, result.Information.Status);
            Assert.AreEqual(EnemyStatus.Mate, result.Information.EnemyStatus);
            Assert.IsNotNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);

            Assert.IsNotNull(_blackPlayerReceivedMoveResult.GameResult);
            Assert.IsNotNull(result.GameResult);

            Assert.AreEqual(GameResult.WhiteWin, _blackPlayerReceivedMoveResult.GameResult.Result);
        }

        [TestMethod]
        public void StalemateShouldBeDrawTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            _board.SetReturnedResult(new MoveInformation
            {
                Status = MoveStatus.AcceptedMove,
                EnemyStatus = EnemyStatus.Stalemate
            });

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.LongGame));

            var result = _movementManager.MakeMove(_whitePlayerSession, game.Id, Bitboard.A1, Bitboard.A2);

            Assert.AreEqual(MoveStatus.AcceptedMove, result.Information.Status);
            Assert.AreEqual(EnemyStatus.Stalemate, result.Information.EnemyStatus);
            Assert.IsNotNull(_blackPlayerReceivedMoveResult);
            Assert.IsNull(_whitePlayerReceivedMoveResult);

            Assert.IsNotNull(_blackPlayerReceivedMoveResult.GameResult);
            Assert.IsNotNull(result.GameResult);

            Assert.AreEqual(GameResult.Draw, _blackPlayerReceivedMoveResult.GameResult.Result);
        }

        private void SubscribePlayersCompleteResult()
        {
            SubscribeToEnemyMakeMove(_whitePlayerSession, result => _whitePlayerReceivedMoveResult = result);
            SubscribeToEnemyMakeMove(_blackPlayerSession, result => _blackPlayerReceivedMoveResult = result);
        }

        private static void SubscribeToEnemyMakeMove(Session player, Action<MoveResult> onEnemyMakeMove)
        {
            player.ClientChannel.SendMessage(
                gameCallback =>
                {
                    gameCallback.EnemyMakeMoveEvent += onEnemyMakeMove;
                    gameCallback.EnemyUpgradePawnEvent += onEnemyMakeMove;
                },
                Actions.DoNothing);
        }

        private void InitializeTwoPlayer()
        {
            var whitePlayer = new UserEntry(1, "player1", displayName: "displayplayer1");
            var blackPlayer = new UserEntry(2, "player2", displayName: "displayplayer2");

            _whitePlayerSession = _sessionManager.CreateSession(whitePlayer);
            _blackPlayerSession = _sessionManager.CreateSession(blackPlayer);
        }

        private MoveResult _whitePlayerReceivedMoveResult;
        private MoveResult _blackPlayerReceivedMoveResult;

        private Session _whitePlayerSession;
        private Session _blackPlayerSession;
        private BoardStub _board;
        private IMovementManager _movementManager;
        private static IGameRepository _gameRepository;

        private readonly ChessGameSettingsFactory _settingsFactory = new ChessGameSettingsFactory();
        private IRatingCalculator _calculator;
        private ManualTimeProvider _timeProvider;
        private GameManager _gameManager;
        private ISessionManager _sessionManager;
    }
}
