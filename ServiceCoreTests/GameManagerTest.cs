﻿using System;
using Common;
using Common.TimeProvider;
using DataAccessLayer.GameEntry;
using DataAccessLayer.Users;
using GameMechanics.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Protocol;
using ServiceCore.Authentication.SessionControl;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.GameSettings;
using ServiceCore.Game.RatingCalculator;
using TestUtils;

namespace ServiceCoreTests
{
    [TestClass]
    public class GameManagerTest
    {
        [TestInitialize]
        public void TestInitialize()
        {
            _gameRepository = new GameRepositoryStub();

            _timeProvider = new ManualTimeProvider();
            _timeProvider.StopTime();

            _calculator = new EloRatingCalculator();
            _gameManager = new GameManager(
                _timeProvider, 
                _calculator, 
                _gameRepository, 
                new ChessBoardFactory(new StandardBoardInitialization()));
            _sessionManager = new SessionManager(
                _timeProvider,
                new TestFallbackGameCallbackFactory(), 
                TimeSpan.FromHours(1));

            _settingsFactory = new ChessGameSettingsFactory();
            
            _whitePlayerResult = null;
            _blackPlayerResult = null;
        }

        [TestMethod]
        public void WhitePlayerShouldNotLooseIfHeHasTimeTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();

            _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(GameMode.SimpleGame));

            _gameManager.Flush();

            Assert.IsNull(_whitePlayerResult);
            Assert.IsNull(_blackPlayerResult);
        }

        [TestMethod]
        public void WhitePlayerShouldLooseGameAfterStepTimeoutTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();

            var settings = _settingsFactory.Create(GameMode.SimpleGame);

            _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                settings);

            _timeProvider.ShiftTime(settings.OneMoveTime.Add(TimeSpan.FromSeconds(1)));

            _gameManager.Flush();

            Assert.IsNotNull(_whitePlayerResult);
            Assert.IsNotNull(_blackPlayerResult);

            Assert.AreEqual(GameResult.BlackWin, _whitePlayerResult.Result);
            Assert.AreEqual(GameResult.BlackWin, _blackPlayerResult.Result);

            Assert.IsTrue(_whitePlayerResult.NewRating < UserRatings.MinRatingValue);
            Assert.IsTrue(_whitePlayerResult.NewEnemyRating > UserRatings.MinRatingValue);
        }

        [TestMethod]
        public void WhitePlayerShouldNotLooseIfGameSwitched()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            var settings = _settingsFactory.Create(GameMode.SimpleGame);

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                settings);

            _timeProvider.ShiftTime(TimeSpan.FromSeconds(10));
            game.Watches.SwitchPlayers();
            _timeProvider.ShiftTime(settings.OneMoveTime.Add(-TimeSpan.FromSeconds(9)));
            
            _gameManager.Flush();

            Assert.IsNull(_whitePlayerResult);
            Assert.IsNull(_blackPlayerResult);
        }

        [TestMethod]
        public void WhitePlayerShouldLooseAfterTotalTimeoutTest()
        {
            InitializeTwoPlayer();
            SubscribePlayersCompleteResult();
            var settings = new ChessGameSettings(
                TimeSpan.FromMinutes(1),
                TimeSpan.FromMinutes(1),
                "test");

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                settings);

            _timeProvider.ShiftTime(TimeSpan.FromSeconds(30));
            game.Watches.SwitchPlayers();

            _gameManager.Flush();
            Assert.IsNull(_whitePlayerResult);

            _timeProvider.ShiftTime(TimeSpan.FromSeconds(30));
            game.Watches.SwitchPlayers();

            _gameManager.Flush();
            Assert.IsNull(_whitePlayerResult);

            _timeProvider.ShiftTime(TimeSpan.FromSeconds(31));
            
            _gameManager.Flush();

            Assert.IsNotNull(_whitePlayerResult);
            Assert.IsNotNull(_blackPlayerResult);

            Assert.AreEqual(GameResult.BlackWin, _whitePlayerResult.Result);
            Assert.AreEqual(GameResult.BlackWin, _blackPlayerResult.Result);

            Assert.IsTrue(_whitePlayerResult.NewRating < UserRatings.MinRatingValue);
            Assert.IsTrue(_whitePlayerResult.NewEnemyRating > UserRatings.MinRatingValue);
        }


        [TestMethod]
        public void WinnerRatingShouldIncreaseTest()
        {
            InitializeTwoPlayer();
            var changes = GetChangesWhenWhiteWin(
                whiteRating: 1400, 
                blackRating: 1200);

            Assert.IsTrue(changes.WhitePlayerDelta > 0);
            Assert.IsTrue(changes.BlackPlayerDelta < 0);
        }

        [TestMethod]
        public void WinnerShouldIncreaseMoreIfHisRatingLess()
        {
            InitializeTwoPlayer();
            var changesWithBigRating = GetChangesWhenWhiteWin(
                whiteRating: 1400,
                blackRating: 1200); 
            var changesWithLittleRating = GetChangesWhenWhiteWin(
                 whiteRating: 1100,
                 blackRating: 1200);

            Assert.IsTrue(changesWithBigRating.WhitePlayerDelta < changesWithLittleRating.WhitePlayerDelta);
            Assert.IsTrue(changesWithBigRating.BlackPlayerDelta > changesWithLittleRating.BlackPlayerDelta);
        }

        [TestMethod]
        public void PlayersWithGreatRatingShouldChangeSlowly()
        {
            InitializeTwoPlayer();
            var changesWithLittleRating = GetChangesWhenWhiteWin(
                whiteRating: 1400,
                blackRating: 1200);
            var changesWithGreatRating = GetChangesWhenWhiteWin(
                 whiteRating: 5400,
                 blackRating: 5200);

            Assert.IsTrue(changesWithLittleRating.WhitePlayerDelta > changesWithGreatRating.WhitePlayerDelta);
            Assert.IsTrue(changesWithLittleRating.BlackPlayerDelta < changesWithGreatRating.BlackPlayerDelta);
        }

        private void SubscribePlayersCompleteResult()
        {
            SubscribeToGameTerminated(_whitePlayerSession, result => _whitePlayerResult = result);
            SubscribeToGameTerminated(_blackPlayerSession, result => _blackPlayerResult = result);
        }

        private static void SubscribeToGameTerminated(Session player, Action<CompleteResult> onGameTerminated)
        {
            // ReSharper disable once PossibleNullReferenceException
            player.ClientChannel.SendMessage(
                gameCallback => gameCallback.GameTerminatedEvent += onGameTerminated,
                Actions.DoNothing);
        }

        
        private void InitializeTwoPlayer()
        {
            var whitePlayer = new UserEntry(1, "player1", displayName: "displayplayer1");
            var blackPlayer = new UserEntry(2, "player2", displayName: "displayplayer2");

            _whitePlayerSession = _sessionManager.CreateSession(whitePlayer);
            _blackPlayerSession = _sessionManager.CreateSession(blackPlayer);
        }

        private RatingChanges GetChangesWhenWhiteWin(int whiteRating, int blackRating)
        {
            const GameMode testedMode = GameMode.SimpleGame;

            _whitePlayerSession.User.Ratings.SetRating(testedMode, whiteRating);
            _blackPlayerSession.User.Ratings.SetRating(testedMode, blackRating);
            _whitePlayerSession.SearchedMode = testedMode;

            var game = _gameManager.CreateGame(
                _whitePlayerSession,
                _blackPlayerSession,
                _settingsFactory.Create(testedMode));

            return _calculator.RecalculateRatings(game, GameResult.WhiteWin);
        }

        private static IGameRepository _gameRepository;
        private Session _whitePlayerSession;
        private Session _blackPlayerSession;

        private CompleteResult _whitePlayerResult;
        private CompleteResult _blackPlayerResult;

        private ChessGameSettingsFactory _settingsFactory;
        private IRatingCalculator _calculator;
        private ManualTimeProvider _timeProvider;
        private GameManager _gameManager;
        private ISessionManager _sessionManager;
    }
}
