﻿using GameMechanics;
using GameMechanics.Board;

namespace ServiceCoreTests
{
    public sealed class BoardFactoryStub : IBoardFactory
    {
        public BoardFactoryStub(BoardStub board)
        {
            _board = board;
        }

        public IBoard Create()
        {
            return _board;
        }

        private readonly BoardStub _board;
    }

    public class BoardStub : IBoard
    {
        public void SetReturnedResult(MoveInformation information)
        {
            _information = information;
        }

        public void SetFigure(Figure figure, Bitboard positions)
        {
        }

        public MoveInformation MakeMove(Bitboard start, Bitboard finish, FigureColor color)
        {
            return _information;
        }

        public MoveInformation UpgradePawn(FigureColor color, Figure newFigure)
        {
            return _information;
        }

        private MoveInformation _information;
    }
}
