﻿using Common.Logging;
using Protocol;
using ServiceCore.Game;

namespace ServiceCoreTests
{
    public class TestFallbackGameCallbackFactory : IFallbackGameServiceCallbackFactory
    {
        public IFallbackClientChannel<IGameCallback> Create()
        {
            return new FallbackGameCallback(ChannelState.Active, new EmptyLogger());
        }
    }
}
