﻿namespace DataAccessLayer
{
    public interface IEntryCreator
    {
        void CreateIfMissing();

        string EntryName { get; }
    }
}
