﻿using System;
using Common;
using Protocol;

namespace DataAccessLayer.GameEntry
{
    public sealed class GameEntry
    {
        public GameEntry(
            int whitePlayerId,
            int blackPlayerId,
            GameResult gameResult,
            GameMode mode, 
            int whitePlayerRatingDelta, 
            int blackPlayerRatingDelta, 
            DateTime finishTime, 
            DateTime startTime,
            int id = -1)
        {
            StartTime = startTime;
            FinishTime = finishTime;
            BlackPlayerRatingDelta = blackPlayerRatingDelta;
            WhitePlayerRatingDelta = whitePlayerRatingDelta;
            Mode = mode;
            BlackPlayerId = blackPlayerId;
            WhitePlayerId = whitePlayerId;
            Id = id;
            Result = gameResult;
        }

        public int Id { get; set; }

        public int WhitePlayerId { get; private set; }

        public int BlackPlayerId { get; private set; }

        public GameMode Mode { get; private set; }

        public int WhitePlayerRatingDelta { get; private set; }

        public int BlackPlayerRatingDelta { get; private set; }

        public GameResult Result { get; private set; }

        public DateTime FinishTime { get; private set; }

        public DateTime StartTime { get; private set; }
    }
}
