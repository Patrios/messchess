﻿using System.Collections.Generic;

namespace DataAccessLayer.GameEntry
{
    public interface IGameRepository : IEntryCreator
    {
        void SaveGameResult(GameEntry game);

        IEnumerable<GameEntry> GetGamesWithUser(int userId);
    }
}
