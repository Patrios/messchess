﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Common;
using Common.Database;
using Protocol;

namespace DataAccessLayer.GameEntry
{
    public class GameRepository : IGameRepository
    {
        public GameRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public void SaveGameResult(GameEntry game)
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var command = connection.CreateCommand();
                command.CommandText = GetGameResultInsertCommand();

                FillParameters(command, game);
                game.Id = int.Parse(command.ExecuteScalar().ToString());
            }
        }

        public IEnumerable<GameEntry> GetGamesWithUser(int userId)
        {
            var games = new List<GameEntry>();
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                using (var reader = connection.ExecuteReader(GetSelectGamesByWithUserText(userId)))
                {
                    while (reader.Read())
                    {
                        games.Add(new GameEntry(
                            id: reader.GetInt32(reader.GetOrdinal("Id")),
                            gameResult: (GameResult) reader.GetInt32(reader.GetOrdinal("GameResult")),
                            whitePlayerId: reader.GetInt32(reader.GetOrdinal("WhitePlayerId")),
                            blackPlayerId: reader.GetInt32(reader.GetOrdinal("BlackPlayerId")),
                            mode: (GameMode) reader.GetInt32(reader.GetOrdinal("Mode")),
                            whitePlayerRatingDelta: reader.GetInt32(reader.GetOrdinal("WhitePlayerRatingDelta")),
                            blackPlayerRatingDelta: reader.GetInt32(reader.GetOrdinal("BlackPlayerRatingDelta")),
                            startTime: reader.GetDateTime(reader.GetOrdinal("StartTime")),
                            finishTime: reader.GetDateTime(reader.GetOrdinal("FinishTime"))));
                    }

                }

                return games.ToArray();
            }
        }

        private string GetSelectGamesByWithUserText(int userId)
        {
            return string.Format("SELECT * FROM {0} WHERE WhitePlayerId = '{1}' OR BlackPlayerId = '{1}'", EntryName, userId);
        }

        private void FillParameters(IDbCommand command, GameEntry game)
        {
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Id"), game.Id));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("GameResult"), game.Result));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("WhitePlayerId"), game.WhitePlayerId));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("BlackPlayerId"), game.BlackPlayerId));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Mode"), game.Mode));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("WhitePlayerRatingDelta"), game.WhitePlayerRatingDelta));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("BlackPlayerRatingDelta"), game.BlackPlayerRatingDelta));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("StartTime"), game.StartTime));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("FinishTime"), game.FinishTime));
        }


        // ToDo: Убрать весь этот мусор либо с переходом на Nhibernate либо через мета программирование
        private string GetGameResultInsertCommand()
        {
            return
                string.Format(
                    "INSERT INTO {0} " +
                        "(GameResult, " +
                        "WhitePlayerId, " +
                        "BlackPlayerId, " +
                        "Mode, " +
                        "WhitePlayerRatingDelta, " +
                        "BlackPlayerRatingDelta, " +
                        "FinishTime, " +
                        "StartTime) VALUES ({1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}); SELECT SCOPE_IDENTITY()",
                    EntryName,
                    DatabaseUtils.GetParameterName("GameResult"),
                    DatabaseUtils.GetParameterName("WhitePlayerId"),
                    DatabaseUtils.GetParameterName("BlackPlayerId"),
                    DatabaseUtils.GetParameterName("Mode"),
                    DatabaseUtils.GetParameterName("WhitePlayerRatingDelta"),
                    DatabaseUtils.GetParameterName("BlackPlayerRatingDelta"),
                    DatabaseUtils.GetParameterName("FinishTime"),
                    DatabaseUtils.GetParameterName("StartTime"));
        }

        public void CreateIfMissing()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.CreateTableIfNotExists(EntryName, _entryFields);
            }
        }

        public string EntryName
        {
            get { return "Games"; }
        }
        
        private readonly IDictionary<string, string> _entryFields = new Dictionary<string, string>
        {
            { "Id", "int identity primary key" },
            { "GameResult", "int" },
            { "WhitePlayerId", "int" },
            { "BlackPlayerId", "int" },
            { "Mode", "int" },
            { "WhitePlayerRatingDelta", "int" },
            { "BlackPlayerRatingDelta", "int" },
            { "FinishTime", "DateTime" },
            { "StartTime", "DateTime" }
        };

        private readonly IConnectionFactory _connectionFactory;
    }
}
