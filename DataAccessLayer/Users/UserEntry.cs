﻿using Protocol;

namespace DataAccessLayer.Users
{
    public sealed class UserEntry
    {
        public UserEntry(
            int id = -1,
            string userName = "",
            string password = "",
            string displayName = "",
            string salt = "",
            string email = "")
        {
            Email = email;
            DisplayName = displayName;
            Id = id;
            UserName = userName;
            Password = password;
            Salt = salt;
            Ratings = new UserRatings();
        }

        public int Id { get; set; }

        public string UserName { get; private set; }

        public string Password { get; set; }

        public string Salt { get; set; }

        public string DisplayName { get; private set; }

        public UserRatings Ratings { get; private set; }

        public string Email { get; private set; }

        public bool IsEmptyUser()
        {
            return Id == EmptyEntry.Id;
        }

        public static readonly UserEntry EmptyEntry = new UserEntry(-1, string.Empty);
    }
}
