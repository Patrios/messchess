﻿using Common;

namespace DataAccessLayer.Users
{
    public interface IUserRepository : IEntryCreator
    {
        UserEntry GetUser(string userName, string password);

        UserEntry RegisterUser(UserEntry user);

        UserEntry GetUserByName(string userName);

        string GetUserDisplayById(int id);

        void SaveRatings(UserEntry user);

        int GetRating(GameMode mode, int userId);

        int GetUserIdByDisplayName(string displayName);
    }
}
