﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Common;
using Common.Crypto;
using Common.Database;
using Protocol;

namespace DataAccessLayer.Users
{
    public sealed class UserDatabaseRepository : IUserRepository
    {
        public UserDatabaseRepository(
            IConnectionFactory connectionFactory,
            ISaltedHashProvider saltedHashProvider)
        {
            _connectionFactory = connectionFactory;
            _saltedHashProvider = saltedHashProvider;
        }

        public UserEntry RegisterUser(UserEntry user)
        {
            if (!GetUserByName(user.UserName).IsEmptyUser())
            {
                return UserEntry.EmptyEntry;
            }

            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var command = connection.CreateCommand();
                command.CommandText = GetUserInsertCommandText();

                FillUserParameters(command, user);
                user.Id = int.Parse(command.ExecuteScalar().ToString());
            }

            return user;
        }

        public UserEntry GetUser(string userName, string password)
        {
            var userEntry = GetUserByName(userName);
            if (userEntry.IsEmptyUser() || !IsCorrectPassword(userEntry, password))
            {
                return UserEntry.EmptyEntry;
            }

            return userEntry;
        }

        public UserEntry GetUserByName(string userName)
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var loadedUser = UserEntry.EmptyEntry;
                using (var reader = connection.ExecuteReader(GetSelectUserCommandText(userName)))
                {
                    if (reader.Read())
                    {
                        loadedUser = new UserEntry(
                            id: reader.GetInt32(reader.GetOrdinal("Id")),
                            userName: reader.GetString(reader.GetOrdinal("UserName")),
                            password: reader.GetString(reader.GetOrdinal("Password")),
                            displayName: reader.GetString(reader.GetOrdinal("DisplayName")),
                            salt: reader.GetString(reader.GetOrdinal("Salt")),
                            email: reader.GetString(reader.GetOrdinal("Email")));
                    }
                }

                InitializeUserRatings(connection, loadedUser.Ratings, loadedUser.Id);
                return loadedUser;
            }
        }

        public string GetUserDisplayById(int id)
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var displayName = connection.ExecuteScalar<string>(
                    string.Format("SELECT {0} FROM {1} WHERE {2} = {3}",
                        "DisplayName",
                        EntryName,
                        "Id",
                        id));

                return displayName;
            }
        }

        public void SaveRatings(UserEntry user)
        {
            var oldRatings = new UserRatings(); 
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                InitializeUserRatings(connection, oldRatings, user.Id);
                var oldModes = oldRatings.GetModesWithRatings();
                foreach (var mode in user.Ratings.GetModesWithRatings())
                {
                    InsertOrUpdateRating(
                        connection, 
                        user, 
                        mode,
                        oldModes.Contains(mode) ? GetUpdateRatingsCommand() : GetInsertRatingsCommand());
                }
            }
        }

        public int GetRating(GameMode mode, int userId)
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var id = connection.ExecuteScalar<int?>(
                    string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}'",
                        "Rating",
                        _ratingsTableName,
                        "Mode",
                        (int)mode));
                return id == null ? new UserRatings().GetRating(mode) : id.Value;
            }
        }

        public int GetUserIdByDisplayName(string displayName)
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                var id = connection.ExecuteScalar<int?>(
                    string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}'",
                        "Id",
                        EntryName,
                        "DisplayName",
                        displayName));
                return id == null ? -1 : id.Value;
            }
        }

        public void CreateIfMissing()
        {
            using (var connection = _connectionFactory.CreateOpenedConnection())
            {
                connection.CreateTableIfNotExists(EntryName, _userEntryFields);
                connection.CreateTableIfNotExists(_ratingsTableName, _ratingsEntryFields);
            }
        }

        public string EntryName
        {
            get { return "Users"; }
        }


        private void InsertOrUpdateRating(IDbConnection connection, UserEntry user, GameMode mode, string commandText)
        {
            var command = connection.CreateCommand();
            command.CommandText = commandText;

            FillRatingsParameters(command, user.Id, user.Ratings.GetRating(mode), mode);
            command.ExecuteScalar();
        }

        private void InitializeUserRatings(IDbConnection connection, UserRatings ratings, int userId)
        {
            if (userId == UserEntry.EmptyEntry.Id)
            {
                return;
            }

            using (var reader = connection.ExecuteReader(GetSelectRatingsCommand(userId)))
            {
                while (reader.Read())
                {
                    ratings.SetRating(
                        (GameMode) reader.GetInt32(reader.GetOrdinal("Mode")),
                        reader.GetInt32(reader.GetOrdinal("Rating")));   
                }
            }
        }

        private bool IsCorrectPassword(UserEntry user, string password)
        {
            return _saltedHashProvider.Hash(password, user.Salt) == user.Password;
        }

        #region Create and load fields

        private void FillUserParameters(IDbCommand command, UserEntry user)
        {
            user.Salt = _saltedHashProvider.GenerateSalt();
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("UserName"), user.UserName));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Password"), _saltedHashProvider.Hash(user.Password, user.Salt)));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("DisplayName"), user.DisplayName));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Salt"), user.Salt));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Email"), user.Email));
        }

        private void FillRatingsParameters(IDbCommand command, int userId, int newRating, GameMode mode)
        {
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("UserId"), userId));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Mode"), (int)mode));
            command.Parameters.Add(new SqlParameter(DatabaseUtils.GetParameterName("Rating"), newRating));            
        }

        private string GetInsertRatingsCommand()
        {
            return
                string.Format(
                    "INSERT INTO {0} " +
                    "(UserId, Mode, Rating) VALUES ({1}, {2}, {3})",
                    _ratingsTableName,
                    DatabaseUtils.GetParameterName("UserId"),
                    DatabaseUtils.GetParameterName("Mode"),
                    DatabaseUtils.GetParameterName("Rating"));
        }

        private string GetUpdateRatingsCommand()
        {
            return
                string.Format(
                    "UPDATE {0} SET Rating = {1} WHERE Mode = {2} AND UserId = {3}",
                    _ratingsTableName,
                    DatabaseUtils.GetParameterName("Rating"),
                    DatabaseUtils.GetParameterName("Mode"),
                    DatabaseUtils.GetParameterName("UserId"));
        }

        private string GetSelectRatingsCommand(int userId)
        {
            return string.Format("SELECT * FROM {0} WHERE UserId = '{1}'", _ratingsTableName, userId);
        }

        private string GetSelectUserCommandText(string userName)
        {
            return string.Format("SELECT * FROM {0} WHERE UserName = '{1}'", EntryName, userName);
        }

        private string GetUserInsertCommandText()
        {
            return
                string.Format(
                    "INSERT INTO {0} " +
                    "(UserName, Password, DisplayName, Salt, Email) VALUES ({1}, {2}, {3}, {4}, {5}); SELECT SCOPE_IDENTITY()",
                    EntryName,
                    DatabaseUtils.GetParameterName("UserName"),
                    DatabaseUtils.GetParameterName("Password"),
                    DatabaseUtils.GetParameterName("DisplayName"),
                    DatabaseUtils.GetParameterName("Salt"),
                    DatabaseUtils.GetParameterName("Email"));
        }

        private readonly IDictionary<string, string> _userEntryFields = new Dictionary<string, string>
        {
            { "Id", "int identity primary key" },
            { "UserName", "varchar(512) unique" },
            { "Password", "varchar(512)" },
            { "Salt", "varchar(512)" },
            { "DisplayName", "varchar(128)" },
            { "Email", "varchar(256)" }
        };


        private readonly IDictionary<string, string> _ratingsEntryFields = new Dictionary<string, string>
        {
            { "UserId", "int" },
            { "Mode", "int" },
            { "Rating", "int" }
        };

        private readonly string _ratingsTableName = "Ratings";

        #endregion

        private readonly IConnectionFactory _connectionFactory;
        private readonly ISaltedHashProvider _saltedHashProvider;
    }
}
