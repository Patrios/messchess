﻿using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.GameEntry;

namespace TestUtils
{
    public sealed class GameRepositoryStub : IGameRepository
    {
        public void SaveGameResult(GameEntry game)
        {
            _games.Add(game);
        }

        public IEnumerable<GameEntry> GetGamesWithUser(int userId)
        {
            return _games.Where(
                game => game.BlackPlayerId == userId || game.WhitePlayerId == userId);
        }
        public void CreateIfMissing()
        {
            throw new System.NotImplementedException();
        }

        public string EntryName { get; private set; }

        private readonly List<GameEntry> _games = new List<GameEntry>();
    }
}
