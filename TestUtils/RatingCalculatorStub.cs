﻿using DataAccessLayer.GameEntry;
using Protocol;
using ServiceCore.Game.GameManager;
using ServiceCore.Game.RatingCalculator;

namespace TestUtils
{
    public sealed class RatingCalculatorStub : IRatingCalculator
    {
        public RatingChanges RecalculateRatings(ChessGame game, GameResult result)
        {
            return new RatingChanges(0, 0);
        }
    }
}
