﻿using System;
using Common.Crypto;
using Common.Database;
using Common.SettingsProvider;
using DataAccessLayer.Users;

namespace InfrastructureSystemConsole
{
    class Program
    {
        public static void Main(string[] args)
        {
            var settingsProvider = new ConfigSettingsProvider();
            var userRepository = new UserDatabaseRepository(
                new SqlConnectionFactory(
                    settingsProvider.SqlServerNameSetting(),
                    settingsProvider.DatabaseNameSetting()),
                new PBKDF2HashProvider());

            Console.WriteLine("Введите имя пользователя.");
            var userName = Console.ReadLine();

            Console.WriteLine("Введите пароль пользователя.");
            var password = Console.ReadLine();

            Console.WriteLine("Введите псевдоним пользователя.");
            var displayName = Console.ReadLine();

            var user = userRepository.RegisterUser(
                new UserEntry(
                    userName: userName,
                    displayName: displayName,
                    password: CryptoUtils.GetHashedPassword(password)));

            if (user.IsEmptyUser())
            {
                Console.WriteLine("Ошибка регистрации.");
            }
            else
            {
                Console.WriteLine("Регистрация прошла успешно.");
            }

            Console.ReadLine();
        }
    }
}
